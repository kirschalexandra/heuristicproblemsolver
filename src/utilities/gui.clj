; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns utilities.gui
  (:require [clojure.string :as string])
  (:import javafx.application.Platform
           [javafx.event ActionEvent EventHandler]
           javafx.geometry.Insets
           [javafx.scene.control Alert Alert$AlertType MenuItem]
           [javafx.scene.input KeyCode KeyCodeCombination KeyCombination KeyEvent]
           [javafx.scene.layout Background BackgroundFill CornerRadii]))

;;; --------------------------------------------------------------------- ;;;
;;; Events                                                                ;;;
;;; --------------------------------------------------------------------- ;;;

;; Button Actions
(defmacro set-button-action [button & body]
  `(.setOnAction ~button
                 (proxy [EventHandler] []
                   (handle [^ActionEvent _#]
                     ~@body))))

;; other Actions
(defmacro set-action [action element & body]
  `(~action ~element
     (proxy [EventHandler] []
       (handle [^ActionEvent _#]
         ~@body))))


;; Keyboard shortcuts
(defn make-keycode [{key :key modifier :modifier}]
  (let [keycode-list {:ctrl  (KeyCombination/CONTROL_DOWN)
                      :shift (KeyCombination/SHIFT_DOWN)
                      :alt   (KeyCombination/ALT_DOWN)}]
    (KeyCodeCombination.
      (KeyCode/getKeyCode key)
      (into-array [(modifier keycode-list)]))))


(defmacro add-keyboard-shortcut [scene shortcut & body]
  `(.addEventHandler ~scene (KeyEvent/KEY_RELEASED)
                     (proxy [EventHandler] []
                       (handle [^KeyEvent event#]
                         (when (.match (make-keycode ~shortcut) event#)
                           ~@body)))))

(defmacro run-in-fx-thread [& body]
  `(Platform/runLater
     (proxy [Runnable] []
       (run [] ~@body))))


;;; --------------------------------------------------------------------- ;;;
;;; Elements                                                              ;;;
;;; --------------------------------------------------------------------- ;;;

(defn add-to-container [container & elements]
  (doseq [elem elements]
    (.add (.getChildren container) elem)))

(defn add-style [object & styles]
  (doseq [stylename styles]
    (.add (.getStyleClass object) stylename)))

(defn clear-container [container]
  ;(.clear (.getChildren container)))
  (let [root-content (.getChildren container)]
    (doseq [elem root-content]
      (.remove (.getChildren container) elem))))

(defn format-keyword [kw]
  (string/capitalize (name kw)))

(defn create-menu-item
  ([parent title function] (create-menu-item parent title nil function))
  ([parent title shortcut function]
   (let [item (MenuItem. title)]
     (.add (.getItems parent) item)
     (set-button-action item (function))
     (when shortcut
       (.setAccelerator item (make-keycode shortcut)))
     item)))

(defn set-background-color [elem color]
  (.setBackground elem (Background. (into-array [(BackgroundFill. color (CornerRadii. 1) (Insets. 0.0 0.0 0.0 0.0))]))))
;; Examples for Color:
;; Color/BLUE
;; (Color/web "#1E90FF" 0.5)

(defn show-alert-message
  ([message] (show-alert-message message "Information" ""))
  ([message title header-text]
   (doto (Alert. Alert$AlertType/INFORMATION)
     (.setTitle "Information Dialog")
     (.setHeaderText header-text)
     (.setContentText message)
     (.showAndWait))))

(defn adjust-stage-size [stage]
  (when-not (.isMaximized stage)
    (.sizeToScene stage)))

(defn clear-canvas [canvas]
  (doto (.getGraphicsContext2D canvas)
    (.save)
    (.setTransform 1 0 0 1 0 0)
    (.clearRect 0 0 (.getWidth canvas) (.getHeight canvas))
    (.restore)))


