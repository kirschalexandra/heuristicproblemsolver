; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns utilities.misc)

;;; --------------------------------------------------------------------- ;;;
;;; Control flow                                                          ;;;
;;; --------------------------------------------------------------------- ;;;

(defmacro preturn
  "Print x and return x."
  [x]
  `(do (println ~x)
       ~x))


;;; --------------------------------------------------------------------- ;;;
;;; Lists                                                                 ;;;
;;; --------------------------------------------------------------------- ;;;
(defn without
  [coll x]
  (filter #(not= x %) coll))

(defn transpose
  [m]
  (apply map list m))

(defn in?
  "Returns true if x is present in coll, else false.
   Order of parameters might be counterintuitive but makes it
   better applicable for the use with filter. "
  [coll x] (some #{x} coll))

(defn comp-order-like-seq                                   ; TODO: robuster machen, momentan wird angenommen, dass a1 und a2 in der Sequenz sind
  ([given-seq a1 a2]
   (< (.indexOf given-seq a1)
      (.indexOf given-seq a2)))
  ([given-seq keyfun a1 a2]
   (< (first (keep-indexed #(when (= (keyfun %2) (keyfun a1)) %1) given-seq))
      (first (keep-indexed #(when (= (keyfun %2) (keyfun a2)) %1) given-seq)))))

;;; --------------------------------------------------------------------- ;;;
;;; Hash-Maps                                                             ;;;
;;; --------------------------------------------------------------------- ;;;
(defn kv-map [f m]
  "Takes a hash-map m, applies function f to every value of m and returns a hash-map with the altered values"
  (reduce (fn [altered-map [k v]] (assoc altered-map k (f v))) {} m))

(defn map-to-kv [f s]
  "Takes a sequence and a function, outputs a hash-map where the key is the function argument and the value is the function result"
  (into {}
        (map (fn [arg] [arg (f arg)])
             s)))

;;; --------------------------------------------------------------------- ;;;
;;; Data types                                                            ;;;
;;; --------------------------------------------------------------------- ;;;
(defn bool2int
  "Converts true to 1 and false to 0"
  [bool]
  (if bool 1 0))

(defn as-vector [x]
  (cond
    (vector? x) x
    (sequential? x) (vec x)
    :else (vector x)))

;;; --------------------------------------------------------------------- ;;;
;;; Functions                                                             ;;;
;;; --------------------------------------------------------------------- ;;;
; count arguments of function, works also for anonymous functions
(defn arg-count [f]
  {:pre [(instance? clojure.lang.AFunction f)]}
  (-> f class .getDeclaredMethods first .getParameterTypes alength))