; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns utilities.math
  (:require [clojure.math.numeric-tower :as math]))


;;; --------------------------------------------------------------------- ;;;
;;; Statistics                                                            ;;;
;;; --------------------------------------------------------------------- ;;;

(defn average [numbers]
  (when (seq numbers) (/ (apply + numbers) (count numbers))))

(defn stdev [numbers]
  (when (seq numbers)
    (let [mean (average numbers)]
      (math/sqrt (average (map #(math/expt (- % mean) 2) numbers))))))

(defn quantile [coll quart]
  (let [sorted (sort coll)
        length (count sorted)
        position (* quart length)]
    (if (= (mod position 1.0) 0.0)
      (average [(nth sorted (dec position)) (nth sorted position)])
      (nth sorted (Math/floor position)))))

(defn median [coll]
  (quantile coll 0.5))

(defn lower-quartile [coll]
  (quantile coll 0.25))

(defn upper-quartile [coll]
  (quantile coll 0.75))

(defn calculate-standard-statistics [values]
  (when (not-empty values)
    {:min            (apply min values)
     :max            (apply max values)                     ; Maximalgeschwindigkeit 1.42 macht Sinn, wenn er in x- und y-Richtung 1m/s fährt, ergibt die Geschwindigkeit in die Diagonale 1.414
     :average        (average values)
     :stdev          (stdev values)
     :median         (median values)
     :lower-quartile (lower-quartile values)
     :upper-quartile (upper-quartile values)}))

;;; --------------------------------------------------------------------- ;;;
;;; Geometry                                                              ;;;
;;; --------------------------------------------------------------------- ;;;

(defn dot-product
  [v1 v2]
  (reduce + (map #(reduce * 1 %) (map vector v1 v2))))

(defn vector-length
  [v]
  (let [res (Math/sqrt (reduce + (map #(Math/pow % 2) v)))]
    res))

;;; --------------------------------------------------------------------- ;;;
;;; Misc                                                                  ;;;
;;; --------------------------------------------------------------------- ;;;
(defn randbetween
  "returns a random number in range [lower, limit["
  [lower limit]
  (let [random-range (- limit lower)]
    (cond
      (pos? random-range) (+ (rand random-range) lower)
      (neg? random-range) (- lower (rand (- random-range)))
      :else limit)))

(defn round2
  "Round a double to the given precision (number of significant digits)"
  ([d] (round2 2 d))
  ([precision d]
   (let [factor (Math/pow 10 precision)]
     (/ (Math/round (* d factor)) factor))))

(defn normalize-number-list [numbers]
  (when (not-empty numbers)
    (let [minval (float (apply min numbers))
          maxval (float (apply max numbers))
          maxmindiff (- maxval minval)]
      (if (= minval maxval)
        (take (count numbers)
              (repeat (cond (and (<= minval 1) (>= minval 0)) minval
                            (> minval 1) 1.0
                            (< minval 0) 0.0)))
        (map #(- (* (/ 1 maxmindiff) %) (/ minval maxmindiff))
             numbers)))))

(defn z-normalize [numbers]
  (let [mean (average numbers)
        stdev (stdev numbers)]
    (map (if (== 0 stdev) #(- % mean) #(/ (- % mean) stdev)) numbers)))

(defn sum-product [values weights]
  (apply + (map * values weights)))