(ns remind.GUI.menu
  [:require
   [utilities.gui :as gut]
   [remind.io :as io]]
  (:import (javafx.scene.control MenuBar Menu MenuItem)
           (javafx.application Platform)
           (javafx.stage FileChooser)
           (javafx.scene.input KeyCode KeyCodeCombination KeyCombination KeyCombination$Modifier)))


(defn adjust-menu-for-remind [menubar scene]
  (let [menufile (first (filter #(= (.getText %) "File") (.getMenus menubar)))
        file-chooser (FileChooser. )
        stage (.getWindow scene)]
    (print "Menu")
    (gut/create-menu-item menufile
                          "Read Log File"
                          {:key "O" :modifier :ctrl}
                          (fn [] (do  (io/load-log-data (.showOpenDialog file-chooser stage))
                                     (gut/adjust-stage-size (.getWindow scene)))))))

