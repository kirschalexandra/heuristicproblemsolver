(ns remind.GUI.display
  (:import (javafx.scene.layout StackPane VBox HBox GridPane BorderPane)
           (javafx.geometry Insets)
           (javafx.scene.control Label CheckBox TabPane Tab)
           (javafx.scene.shape Line)
           (javafx.scene.chart NumberAxis LineChart BarChart))
  (:require [utilities.gui :as gut]
            [remind.state :as state]))


(defn make-status-fields []
  (let [wrapper (GridPane.)
        risk-checker (CheckBox.)
        risk-label (Label. "HD pls")
        activity-label (Label. "Detected activity")
        activity-text (Label. " - None -")
        line-label (Label. "Entry") line-value (Label. "-1")
        time-label (Label. "Timestamp")  time-value (Label. "HH:MM:SS")
        sensor-label (Label. "Sensor") sensor-value (Label. "-1")
        step-label (Label. "Step") step-value (Label. "-1")
        alcohol-conc-label(Label. "Alcohol-Conc") alcohol-conc-value (Label. "-1")
        last-hd-label (Label. "Last-Hd") last-hd-value (Label. "-1")]

    (.setDisable risk-checker true)
    (.setSelected risk-checker true)
    (add-watch state/rowindex :gui (fn [k a o n] (.setText line-value (str n))))
    (add-watch state/timestamp :gui (fn [k a o n] (.setText time-value (str n))))
    (add-watch state/raw-sensor-value :gui (fn [k a o n] (.setText sensor-value (str n))))
    (add-watch state/number-of-steps :gui (fn [k a o n] (.setText step-value (str n))))
    (add-watch state/alcohol-concentration :gui (fn [k a o n] (.setText alcohol-conc-value (str n))))
    (add-watch state/time-since-last-hd :gui (fn [k a o n] (.setText last-hd-value (str n))))
    (.add wrapper line-label 0 0) (.add wrapper line-value 1 0)
    (.add wrapper time-label 0 1) (.add wrapper time-value 1 1)
    (.add wrapper sensor-label 0 2) (.add wrapper sensor-value 1 2)
    (.add wrapper step-label 0 3) (.add wrapper step-value 1 3)
    (.add wrapper alcohol-conc-label 0 4) (.add wrapper alcohol-conc-value 1 4)
    (.add wrapper last-hd-label 0 5) (.add wrapper last-hd-value 1 5)
    (.setHgap wrapper 50)
    wrapper))


(defn add-series-data [pos series dataindex]
  (let [entries (.size (.getData series))
        diff (- (inc pos) entries)
        toinsert (map #(read-string (get % dataindex)) (take diff (drop entries @state/datalog)))
        xvals (range entries (inc pos))]
    (.addAll (.getData series) (map #(javafx.scene.chart.XYChart$Data. %1 %2) xvals toinsert ))))

(defn add-series-data2 [pos series datasource]
  (let [entries (.size (.getData series))
        diff (- (inc pos) entries)
        toinsert (take diff (drop entries datasource))
        xvals (range entries (inc pos))]
    (.addAll (.getData series (map #(javafx.scene.chart.XYChart$Data. %1 %2) xvals toinsert)))))

(defn remove-series-data [pos series]
  (let [entries (.size (.getData series))]
    (.remove (.getData series) pos entries)))


(defn on-data-change
  ([k a o n series dataindex] (on-data-change k a o n series dataindex identity))
  ([k a o n series dataindex add-preprocess]
   (let [v @state/raw-sensor-value]
     (if (and (>= n o) (some? v))
       (add-series-data (add-preprocess n) series dataindex))
     (if (and (< n o) (some? v))
       (remove-series-data n series)))))

(defn make-line-chart [series]
  (let [wrapper (StackPane.)
        xaxis (NumberAxis.)
        yaxis (NumberAxis.)
        chart (LineChart. xaxis yaxis)]
    (gut/add-to-container wrapper chart)
    (.addAll (.getData chart) series)
    (.setAnimated chart false)
    (.setCreateSymbols chart false)
    wrapper))

(defn make-hd-chart []
  (let  [series (javafx.scene.chart.XYChart$Series.)
         hddetects (javafx.scene.chart.XYChart$Series.)
         recomms (javafx.scene.chart.XYChart$Series.)]
    (add-watch state/rowindex :alcchart (fn [k a o n] (on-data-change k a o n series 3)))
    (add-watch state/rowindex :hdchart (fn [k a o n] (let [overhead  (filter #(> (.getXValue %) n) (.getData hddetects))]
                                                      (.removeAll (.getData hddetects) overhead))))
    (add-watch state/rowindex :recomchart (fn [k a o n] (let [overhead  (filter #(> (.getXValue %) n) (.getData recomms))]
                                                         (.removeAll (.getData recomms) overhead))))
    (add-watch state/detectedHDs :hdchart (fn [k a o n]
                                            (when (not-empty n)
                                              (.addAll (.getData hddetects)
                                                       [(javafx.scene.chart.XYChart$Data. @state/rowindex 0)
                                                        (javafx.scene.chart.XYChart$Data. @state/rowindex 5000)
                                                        (javafx.scene.chart.XYChart$Data. @state/rowindex 0)]))))
    (add-watch state/recomms :recomchart (fn [k a o n]
                                           (when (not-empty n)
                                             (.addAll (.getData recomms)
                                                      [(javafx.scene.chart.XYChart$Data. @state/rowindex 0)
                                                       (javafx.scene.chart.XYChart$Data. @state/rowindex 5000)
                                                       (javafx.scene.chart.XYChart$Data. @state/rowindex 0)]))))
    (.setName series "Raw Sensor Data")
    (.setName hddetects "Detected HDs")
    (.setName recomms "Recommendation")
    (make-line-chart [series hddetects recomms])))


(defn make-ble-chart []
  (let [zone1 (javafx.scene.chart.XYChart$Series.)
        zone2 (javafx.scene.chart.XYChart$Series.)
        zone3 (javafx.scene.chart.XYChart$Series.)]
    (add-watch state/rowindex :zone1 (fn [k a o n] (on-data-change k a o n zone1 17)))
    (add-watch state/rowindex :zone2 (fn [k a o n] (on-data-change k a o n zone2 18)))
    (add-watch state/rowindex :zone3 (fn [k a o n] (on-data-change k a o n zone3 19)))
    (.setName zone1 "Zone1")
    (.setName zone2 "Zone2")
    (.setName zone3 "Zone3")
    (make-line-chart [zone1 zone2 zone3])))


(defn make-tab [name element]
  (let [tab (Tab. name)
        tab-content (BorderPane.)]
    (.setCenter tab-content element)
    (.setContent tab tab-content)
    tab))


(defn make-display []
  (let [tab-pane (TabPane.)
        hd-wrapper (VBox.)]
    (gut/add-to-container hd-wrapper (make-hd-chart) (make-status-fields))
    (.add (.getTabs tab-pane) (make-tab "HD-Sensor" hd-wrapper))
    (.add (.getTabs tab-pane) (make-tab "BLE Sensor" (make-ble-chart)))
    tab-pane))

