(ns remind.GUI.core
  (:require [clojure.java.io :as io]
            [hps.GUI.structure :as structure]
            remind.GUI.controls
            remind.GUI.display
            remind.GUI.menu
            [hps.decision-modules :as dm]
            [utilities.gui :as gut]
            [remind.experts :as experts])
  (:import [javafx.event Event EventHandler]
           [javafx.scene.control Tab TabPane]
           [javafx.scene.layout BorderPane HBox Priority]))

(def dm-colors {:Timer ["ADF3FF" "ADF3FF"]
                :HDRecognition ["24502b" "d4a3d2"]
                :Activity ["ffb800" "ffb800"]
                :Zone  ["A9BA54" "A9BA54"]})

(defn load-dm-configuration [dm-name]
  (get (dm/list-decision-modules) (keyword dm-name)))


(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dm/get-dm-from-name dm-keyword 'remind.experts))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
    (run! (fn [[pane experts]]
            (hps.GUI.make-decision/initialize-experts-in-display pane experts)
            (.setSelected (some #(when (= (.getText %) "All") %) (.getChildren (.getContent pane))) true))
          [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])))


(defn make-decision-module-tabs [tab-pane decision-modules]
  (doseq [dm-keyword decision-modules]
    (let [tab (Tab. (name dm-keyword))
          tab-content-pane (BorderPane.)
          tabcontentpane-hps-displays (structure/make-decision-module-basic-display)
          [fg-color bg-color] (get dm-colors dm-keyword)
          display-parameters (dissoc hps.make-decision/produce-and-evaluate-default-params :evaluator-aggregation-fun)]
      (initialize-expert-display tabcontentpane-hps-displays dm-keyword)
      ;; (hps.GUI.make-decision/update-setup-display
      ;;  {:experts {:producers prods :evaluators evalus :adaptors nil}
      ;;   :parameters display-parameters}
      ;;  (structure/get-element-from-decision-module-display tabcontentpane-hps-displays "hps-configbox"))
      (.setId tabcontentpane-hps-displays (str "dm-pane-tab-" (name dm-keyword)))
      (.setDividerPosition tabcontentpane-hps-displays 0 0.4)
      (.add (.getStyleClass tab-content-pane) "morph-tabpane-with-goal")
      (.setStyle tab (str "-fx-background-color: #" bg-color ";"))
      (.setStyle tab-content-pane (str "-fx-background-color: #" bg-color ";"))
      (.setCenter tab-content-pane tabcontentpane-hps-displays)
      (.setContent tab tab-content-pane)
      (.add (.getTabs tab-pane) tab)
      (.setOnSelectionChanged tab
                              (proxy [EventHandler] []
                                (handle [^Event event]
                                  (when (.isSelected tab)
                                    (load-dm-configuration(.getText tab)))))))))

(defn display-decision-process [scene]
  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion  (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        decision-modules-tab-pane (TabPane.)
        info-and-control-box (remind.GUI.controls/make-control-elements scene)]

    (.setId decision-modules-tab-pane "dm-tabpane")
    (gut/add-to-container controlRegion info-and-control-box)
    (gut/add-to-container decisionModules decision-modules-tab-pane)

    (HBox/setHgrow decision-modules-tab-pane Priority/ALWAYS)
    (.setDividerPosition hps-split-pane 0 0.6)

    (make-decision-module-tabs decision-modules-tab-pane
                               (dm/list-decision-modules 'remind.experts))

    (gut/add-to-container stateRegion (remind.GUI.display/make-display)))

  (doto (.getWindow scene)
    (gut/adjust-stage-size)
    (.centerOnScreen)))


(defn adjust-gui-for-remind [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/remind.css")))
  (remind.GUI.menu/adjust-menu-for-remind (.lookup scene "#hps-menubar-left") scene)
  (display-decision-process scene))



;;always add "REM" mode
(hps.GUI.structure/register-hps-mode "REM" adjust-gui-for-remind false)
