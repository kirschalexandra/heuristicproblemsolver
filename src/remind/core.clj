(ns remind.core
  (:require   [remind.GUI.core]
              [hps.core]
              [hps.GUI.main :as main]
              [hps.GUI.structure :as structure]
              [clojure.string :as string]
              [clojure.pprint :as pprint])
  (:use [clojure.repl :only (doc find-doc apropos)]
        [clojure.pprint] ; super für Ausgaben: pp druckt letzten Rückgabewert in hübsch
        [hps.types])
  (:import (java.lang Runtime System)
           (javafx.application Application))
  (:gen-class))



(defn remind-start []
  (structure/register-hps-mode "REM" remind.GUI.core/adjust-gui-for-remind true)
  (main/start-hps-gui))
