(ns remind.config)

(def timed-interval 220)
(def sensor-thresh 2500)
(def sensor-window 30)

(def ble-min -100)
(def ble-thresh -10)
