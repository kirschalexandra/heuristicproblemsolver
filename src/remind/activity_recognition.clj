(ns remind.activity-recognition)

(def activities '(:noRisk
                  :enteredPatientZone
                  :exitedPatientZone
                  :enteredRiskArea
                  :disposedGloves
                  :performedAsepticTask
                  :bodyFluidExposureRisk))
