(ns remind.experts
  (:require [hps.make-decision :as md]
            [hps.types :refer :all]
            [hps.heuristics :refer :all]
            [hps.decision-modules :refer :all]
            [remind.config :as cfg]
            [remind.state :as state :refer :all]))

(def-heuristic dummy-heuristic
  :producer (def-expert-fun [] true)
  :evaluator (def-expert-fun [alt] (rand)))

(def-heuristic timed-reminder
  :producer (def-expert-fun [] true)
  :evaluator (def-expert-fun [alt]
               (let [current @state/rowindex
                     last-hd (or (last @state/detectedHDs) 0)
                     last-rem (or (last @state/recomms) 0)
                     hd-factor (/ (- current last-hd) cfg/timed-interval)
                     rem-factor (/ (- current last-rem) cfg/timed-interval)]
                 (when (and  (> hd-factor 1) (> rem-factor 1))
                   (state/addRecom @state/rowindex))
                 hd-factor)))

(defn get-zone-atom [name]
  (case name
    "zone1" @state/ble-1
    "zone2" @state/ble-2
    "zone3" @state/ble-3))

(def threshold-evaluator
  (def-expert-fun [alt]
    (let [res  (/ (- cfg/ble-min
                     (get-zone-atom (:value alt)))
                  cfg/ble-thresh)]  res)))


(def alcohol-Sensor
  (def-expert-fun [alt]
    (if (and (> @state/raw-sensor-value cfg/sensor-thresh)
             (> (- @state/rowindex (or (last @state/detectedHDs) 1)) cfg/sensor-window ))
      (state/addHD @state/rowindex))
    (/ @state/raw-sensor-value cfg/sensor-thresh)))

(def-heuristic hd-detection
  :producer (def-expert-fun [] true)
  :evaluator alcohol-Sensor)

(def-heuristic zone1
  :producer (def-expert-fun [] "zone1")
  :evaluator threshold-evaluator)
(def-heuristic zone2
  :producer (def-expert-fun [] "zone2")
  :evaluator threshold-evaluator)
(def-heuristic zone3
  :producer (def-expert-fun [] "zone3")
  :evaluator threshold-evaluator)

(declare-dm :Timer
            :available-experts {:producers [:timed-reminder]
                                :evaluators [:timed-reminder]}
            :run-frequency-fun (run-frequency-fun-by-time 100))

(declare-dm :HDRecognition
            :available-experts {:producers [:hd-detection]
                                :evaluators [:hd-detection]}
            :run-frequency-fun (run-frequency-fun-by-time 100))
(declare-dm :Zone
            :available-experts {:producers [:zone1 :zone2 :zone3]
                                :evaluators [:zone1 :zone2 :zone3]}
            :run-frequency-fun (run-frequency-fun-by-time 100))


(def dm-lookup {:HDRecognition 0
                :Timer 1
                :Zone 3})


(defn run-dms []
  (loop [names (keys dm-lookup)
         result {}]
    (if (nil? (first names))
      result
      (recur (rest names)
             (assoc result (keyword (first names))
                    (step-decision-module (get-dm-from-name (first names) 'remind.experts)))))))


(defn run-dms-stepwise []
  (let [goal @state/rowindex]
    (state/reset-data @state/datalog)
    (loop [n 0
           decisions []]
      (if (<= n goal)
        (do
          (state/set-line-index n)
          (recur (inc n)
                 (run-dms)))
        (do
          decisions)))))
