(ns remind.io
  [:require
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [remind.state :as state]])

(defn read-log-data [filename]
  (if filename
    (with-open [in-file (io/reader filename)]
      (doall
       (csv/read-csv in-file :separator \,)))))

(defn load-log-data [filename]
  (let [data (read-log-data filename)]
    (when data
      (state/reset-data data))))

