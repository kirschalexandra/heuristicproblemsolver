(ns remind.state)

;;General info
(def datalog (atom nil))
(def rowindex (atom -1))

;;BLE Data
(def ble-1 (atom []))
(def ble-1-history (atom []))
(def ble-2 (atom []))
(def ble-2-history (atom []))
(def ble-3 (atom []))
(def ble-3-history (atom []))

;; HD-Detection
(def recomms (atom []))
(def detectedHDs (atom []))

;; Other fields
(def timestamp (atom nil))
(def raw-sensor-value (atom nil))
(def number-of-steps (atom nil))
(def alcohol-concentration (atom nil))
(def time-since-last-hd (atom nil))

(defn addRecom [time]
  (swap! recomms conj time))

(defn addHD [time]
  (swap! detectedHDs conj time))

(defn reset-data [data]
  (do
    (reset! datalog data)
    (reset! rowindex 0)
    (reset! recomms [])
    (reset! detectedHDs [])
    (reset! timestamp nil)
    (reset! raw-sensor-value nil)
    (reset! number-of-steps nil)
    (reset! alcohol-concentration nil)
    (reset! time-since-last-hd nil)
    (reset! ble-1 [])
    (reset! ble-2 [])
    (reset! ble-3 [])))

(defn lowpass-moving-average [raw-value history]
  (if (empty? @history)
    (do
      (swap! history (partial  cons raw-value))
      raw-value)
    (let [window (take 5 @history)
          lp (+ (first window)
                (* 0.5 (- raw-value (first window))))]
      (swap! history (partial  cons lp))
      (/ (apply + (take 5 @history)) (count (take 5 @history))))))


(defn set-line-index [idx]
  (let [line (nth @datalog idx)]
    (if line
      (do
        (reset! rowindex idx)
        (reset! timestamp  (get line 2))
        (reset! raw-sensor-value (read-string (get line 3)))
        (reset! number-of-steps (read-string (get line 4)))
        (reset! alcohol-concentration (read-string (get line 7)))
        (reset! time-since-last-hd (read-string (get line 12)))
        (reset! ble-1 (lowpass-moving-average (read-string (get line 17)) ble-1-history))
        (reset! ble-2 (lowpass-moving-average (read-string (get line 18)) ble-2-history))
        (reset! ble-3 (lowpass-moving-average (read-string (get line 19)) ble-3-history))))))

(defn next-data-line []
  (if (< (inc @rowindex) (count @datalog))
    (do
      (swap! rowindex inc)
      (set-line-index @rowindex))))

(defn prev-data-line []
  (if (>= (dec @rowindex) 0)
    (do
      (swap! rowindex dec)
      (set-line-index @rowindex))))
