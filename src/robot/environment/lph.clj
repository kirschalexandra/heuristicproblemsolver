; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.environment)

(def lph-furniture
  {:bedroom {:bed           {:pose {:x -3.19 :y -2.97 :rotation   0} :size {:width 2.08 :depth 0.97}}
             :armchair      {:pose {:x -2.37 :y -3.79 :rotation  90} :size {:width 0.73 :depth 0.81}}
             :bedside-table {:pose {:x -3.20 :y -1.88 :rotation   0} :size {:width 0.55 :depth 0.55}}
             :wardrobe      {:pose {:x  0.06 :y -1.16 :rotation -90} :size {:width 2.75 :depth 0.61}}}
   :livingroom {:sofa         {:pose {:x -3.19 :y -1.0  :rotation   0} :size {:width 1.61 :depth 0.88}}
                :armchair     {:pose {:x -2.38 :y  0.91 :rotation   0} :size {:width 0.8  :depth 0.7 }}
                :coffee-table {:pose {:x -2.97 :y  0.64 :rotation -90} :size {:width 0.55 :depth 0.90}}
                :tv-table     {:pose {:x  0.12 :y  1.29 :rotation   0} :size {:width 0.55 :depth 0.90}}
                :plant        {:pose {:x -2.51 :y  1.63 :rotation  90} :size {:width 0.6  :depth 0.74}}}
   :kitchen {:kitchenette {:pose {:x -3.34 :y  5.69 :rotation   0} :size {:width 4.38 :depth 0.60}}
             :table       {:pose {:x -2.80 :y  2.90 :rotation   0} :size {:width 1.03 :depth 1.02}}
             :chair-0     {:pose {:x -2.71 :y  3.69 :rotation   0} :size {:width 0.70 :depth 0.67}}
             :chair-1     {:pose {:x -1.93 :y  3.74 :rotation -90} :size {:width 0.70 :depth 0.67}}
             :chair-2     {:pose {:x -2.66 :y  3.10 :rotation  90} :size {:width 0.70 :depth 0.67}}
             :chair-3     {:pose {:x -1.96 :y  3.03 :rotation 180} :size {:width 0.70 :depth 0.67}}
             :fridge      {:pose {:x  1.04 :y  3.43 :rotation 180} :size {:width 0.76 :depth 0.86}}}
   :bathroom {:toilet     {:pose {:x  1.75 :y  5.43 :rotation   0} :size {:width 0.54 :depth 0.87}}
              :shower     {:pose {:x  3.16 :y  4.76 :rotation   0} :size {:width 1.48 :depth 1.48}}
              :wash-basin {:pose {:x  3.79 :y  4.47 :rotation -90} :size {:width 0.80 :depth 0.80}}
              :bathtub    {:pose {:x  3.14 :y  3.42 :rotation 180} :size {:width 1.90 :depth 0.90}}}})


(def lph #^{:name :lph}
  {:rooms {:kitchen      {:pose {:x -3.667 :y  2.264 :rotation 0} :size {:width 4.936 :depth 4.35}}
           :bathroom     {:pose {:x  1.057 :y -2.264 :rotation 0} :size {:width 3.759 :depth 4.35}}
           :livingroom   {:pose {:x -3.667 :y -1.116 :rotation 0} :size {:width 4.529 :depth 3.697}}
           :bedroom      {:pose {:x -3.667 :y -4.268 :rotation 0} :size {:width 4.529 :depth 3.337}}}
   :room-connections [[:bedroom    :livingroom  {:x -0.867 :y -1.055}]
                      [:livingroom :kitchen     {:x -1.267 :y  2.41}]
                      [:kitchen    :bathroom    {:x  1.163 :y  4.414}]]
   :outer-walls [{:pose {:x -3.242  :y -4.268 :rotation 90}  :size {:width 6.532 :depth 0.345} :adjacent-rooms [:bedroom :livingroom] :label "north-western wall"}
                 {:pose {:x -3.242  :y -4.268 :rotation  0}  :size {:width 3.972 :depth 0.345} :adjacent-rooms [:bedroom]             :label "western wall bedroom"}
                 {:pose {:x  0.942 :y -4.268 :rotation 90}  :size {:width 3.274 :depth 0.212} :adjacent-rooms [:bedroom]             :label "southern wall bedroom"}
                 {:pose {:x  0.942 :y  1.164 :rotation 90}  :size {:width 1.4   :depth 0.212} :adjacent-rooms [:livingroom]          :label "southern wall livingroom"}
                 {:pose {:x -3.322 :y  2.264 :rotation 90}  :size {:width 4.35  :depth 0.317} :adjacent-rooms [:kitchen]             :label "north-eastern wall"}
                 {:pose {:x -3.322 :y  6.297 :rotation  0}  :size {:width 8.0   :depth 0.317} :adjacent-rooms [:kitchen :bathroom]   :label "eastern wall"}
                 ; :width von "eastern wall" ist eigentlich 7.954, aber dadurch dass ich Versatz zwischen Flying Space und Wohnzimmer nicht korrekt habe gäbe es damit eine kleine Lücke in der Wand
                 {:pose {:x  0.942 :y  2.264 :rotation  0}  :size {:width 3.725 :depth 0.292} :adjacent-rooms [:bathroom]            :label "western wall bathroom"}
                 {:pose {:x  4.879 :y  2.264 :rotation 90}  :size {:width 4.35  :depth 0.212} :adjacent-rooms [:bathroom]            :label "southern wall bathroom"}]
   :inner-walls [{:pose {:x -3.27  :y -1.116  :rotation  0} :size {:width 1.75 :depth 0.122} :adjacent-rooms [:bedroom :livingroom]  :label "inner wall bedroom-livingroom 1"}
                 {:pose {:x  0.0   :y -1.116  :rotation  0} :size {:width 0.73 :depth 0.122} :adjacent-rooms [:bedroom :livingroom]  :label "inner wall bedroom-livingroom 2"}
                 {:pose {:x -3.35  :y  2.264  :rotation  0} :size {:width 0.6  :depth 0.292} :adjacent-rooms [:livingroom :kitchen]  :label "inner wall livingroom-kitchen 1"}
                 {:pose {:x  0.33  :y  2.264  :rotation  0} :size {:width 0.4  :depth 0.292} :adjacent-rooms [:livingroom :kitchen]  :label "inner wall livingroom-kitchen 2"}
                 {:pose {:x  1.269 :y  2.556  :rotation 90} :size {:width 0.75 :depth 0.212} :adjacent-rooms [:kitchen :bathroom]    :label "inner wall kitchen-bathroom 1"}
                 {:pose {:x  1.269 :y  5.497  :rotation 90} :size {:width 0.8  :depth 0.212} :adjacent-rooms [:kitchen :bathroom]    :label "inner wall kitchen-bathroom 2"}
                 ]
   :furniture lph-furniture
})
