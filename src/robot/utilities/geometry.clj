; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.utilities)

(def radian-max (* 2 Math/PI))
(def degree-max 360.0)


;; *********************************************************************************
;; ***** angle conversions                                                     *****
;; *********************************************************************************

;; Normalization

(defn angle-to-angle [angle angle-max]
  (let [d (min (- angle (* (quot angle angle-max) angle-max)) angle)]
    (if (< d 0.0)
    	(+ d angle-max)
      d)))

(defn angle-to-left-right-angle [angle angle-max]
  (let [d (angle-to-angle angle angle-max)]
    (if (<= d (* 0.5 angle-max))
    	d
    	(- d angle-max))))

(defn radian-to-radian [rads]
  "returns an angle in radian measure between 0 and 2pi"
  (angle-to-angle rads radian-max))

(defn radian-to-left-right-radian [rads]
  "returns an angle in radian measure between -pi and pi"
  (angle-to-left-right-angle rads radian-max))

;; Conversion radian <-> degrees

(defn convert-degree-to-radian [degrees]
  (* degrees (/ 180.0) Math/PI))

(defn convert-radian-to-degree [rads]
  (* rads 180.0 (/ Math/PI)))

(defn degree-to-radian [degrees]
  "returns an angle in radian measure between 0 and 2pi"
  (convert-degree-to-radian (angle-to-angle degrees degree-max)))

(defn radian-to-degree [rads]
  "returns an angle in degrees between 0 and 360"
  (convert-radian-to-degree (angle-to-angle rads radian-max)))



;; *********************************************************************************
;; ***** angle operations                                                      *****
;; *********************************************************************************

(defn difference-angle [phi-0 phi-1 angle-max]
  (angle-to-left-right-angle
    (- (angle-to-angle phi-0 angle-max) (angle-to-angle phi-1 angle-max))
    angle-max))

(defn difference-radian [phi-0 phi-1]
  "returns angle difference (phi-0 - phi-1) between -pi and pi"
  (difference-angle phi-0 phi-1 radian-max))

(defn difference-degree [phi-0 phi-1]
  "returns angle difference (phi-0 - phi-1) between -180 and 180"
  (difference-angle phi-0 phi-1 degree-max))


(defn delta-radian [phi-0 phi-1]
  "returns absolute angle difference between 0 and pi"
  (math/abs (difference-radian phi-0 phi-1)))

(defn delta-degree [phi-0 phi-1]
  "returns absolute angle difference between 0 and 180"
  (math/abs (difference-degree phi-0 phi-1)))



;; *********************************************************************************
;; ***** point operations                                                      *****
;; *********************************************************************************

; euklid distance sqr
(defn euklid-distance-sqr [vec-1 vec-2]
  "calculates the square of the euklidean distance between to n-dimensional points"
  (reduce + (map (comp #(math/expt % 2) -) vec-1 vec-2)))


; euklid distance
(defn euklid-distance [vec-1 vec-2]
  "calculates the euklidean distance between to n-dimensional points"
  (math/sqrt (euklid-distance-sqr vec-1 vec-2)))

; spezifische Funktionen auf hashmaps die Punkte repräsentieren
(defn point-distance [{x1 :x y1 :y} {x2 :x y2 :y}]
  (euklid-distance [x1 y1] [x2 y2]))

; radian between points
(defn radian-between-points [start-point goal-point]
  "calculates angle between two 2D-points between -pi and pi"
  (let [delta-x (- (:x goal-point) (:x start-point))
        delta-y (- (:y goal-point) (:y start-point))]
    (radian-to-left-right-radian (Math/atan2 delta-y delta-x))))


;; *********************************************************************************
;; ***** areas                                                                 *****
;; *********************************************************************************

(defn point-in-rectangle [{x :x y :y} [min-x max-x min-y max-y]]
  "checks whether a point (first argument) lies withing the rectangle (second argument) given by its corner points; assumption: retangle is aligned with cooridnate system axes"
  (and (>= x min-x) (<= x max-x) (>= y min-y) (<= y max-y)))

(defn point-dist-to-rectangle [point rectangle]
  (if (point-in-rectangle point rectangle)
    0
    (let [{x :x y :y} point
          [min-x max-x min-y max-y] rectangle]
      (cond
        ; Fall 1: Punkt liegt im y-Bereich
        (and (>= y min-y) (<= y max-y))
          (if (<= x min-x)
            (- min-x x)  ; Fall 1a: Punkt liegt links von Rechteck
            (- x max-x)) ; Fall 1b: Punkt liegt rechts von Rechteck
        ; Fall 2: Punkt liegt im x-Bereich
        (and (>= x min-x) (<= x max-x))
          (if (<= y min-y)
            (- min-y y)  ; Fall 2a: Punkt liegt unterhalb von Rechteck
            (- y max-y)) ; Fall 2b: Punkt liegt oberhalb von Rechteck
        ; Fall 3: Punkt liegt an keiner Seite -> Distanz zur nächsten Ecke
        (and (<= x min-x) (<= y min-y)) (point-distance point {:x min-x :y min-y}) ; linke untere Ecke
        (and (<= x min-x) (>= y max-y)) (point-distance point {:x min-x :y max-y}) ; linke obere Ecke
        (and (>= x max-x) (<= y min-y)) (point-distance point {:x max-x :y min-y}) ; rechte untere Ecke
        (and (>= x max-x) (>= y max-y)) (point-distance point {:x max-x :y max-y}) ; rechte obere Ecke
      ))))
