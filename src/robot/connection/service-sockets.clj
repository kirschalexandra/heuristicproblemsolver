; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.connection)

;; basic socket connection for services
(defn start-service-socket
  ([] (start-service-socket 4000))
  ([port]
    (let [service-socket (Socket. "localhost" port)]

      ; define function for sending service requests
      (let [service-writer (PrintWriter. (.getOutputStream service-socket) true)]

        (defn send-service-request
          ([component-name service-name arguments] (send-service-request (gensym) component-name service-name arguments))
          ([request-id component-name service-name arguments]
            (let [cmdstring (cl-format nil "~s ~a ~a ~a"
                              request-id component-name service-name arguments)]
              (println cmdstring)
              (.println service-writer cmdstring))
            request-id)))
      ; define functions for service reply
      (let [reply-watchlist (atom {})
            service-reader (BufferedReader. (InputStreamReader. (.getInputStream service-socket)))
            watch-for-replies? (atom true)]

        (defn stop-watching-for-service-replies []
          (reset! watch-for-replies? false))

        (defn register-reply [request-id promise]
          (swap! reply-watchlist assoc request-id promise))

        (defn watch-service-replies []
          (while @watch-for-replies?
            (let [[id-string reply] (str/split (.readLine service-reader) #" " 2)
                  id (symbol id-string)]
              (deliver (id @reply-watchlist) reply)
              (swap! reply-watchlist dissoc id))))))))


;; auxiliary function for service requests
(defn invoke-service-with-reply [component-name service-name arguments]
  (let [request-id (gensym)
        reply (promise)]
    (register-reply request-id reply)
    (send-service-request request-id component-name service-name (if (empty? arguments) "[]" (cl-format nil "[\"~{~a~^, ~}\"]" arguments)))
    (let [[status reply-data] (str/split @reply #" " 2)]
      (if (= status "SUCCESS")
        [:success reply-data]
        [:failure reply-data]))))

;; auxiliary function to get port numbers
(defn get-port-for-stream [stream-name]
  (let [reply (invoke-service-with-reply "simulation" "get_stream_port" [stream-name])]
    (when (= (first reply) :success)
      (read-string (second reply)))))
