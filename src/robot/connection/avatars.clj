; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.connection)

;; Humans werden als Perzepte bei start-robot mitberücksichtigt
;; Avatars sind Menschen in der Simulation, die von Clojure aus per Skript gesteuert werden
;; Avatars werden ähnlich wie Roboter definiert, nur das Perzept für die Position sollten nicht definiert werden, weil das sowieso bekannt ist


;; Avatar definitions
;; könnte man noch irgendwie "verstecken", aber bei den wenigen Avataren spendiere ich globale Variablen

(def ella {:name "ella"
           :active? (atom false)
           :frequency 15
           ;:percepts {:joint-state {:var state/ella-joint-state :stream-name "ella.skeleton.joint_states"}} ;pose hier nicht definieren, das wird schon bei den "passiven" Menschen oben eingeschaltet
           :commands {:ellamotion {:type :stream :stream-name "ella.ellamotion"
                                   :command-default {:x 0.0 :y 0.0 :w 0.0}
                                   :max-values {:x 1.0 :y 1.0 :w 1.0}}}
           :control-fun (fn [] (send-command :ellamotion {:x 0.5 :y 0.0 :w 0.0}))
           :stop-fun    (fn [] (send-command :ellamotion {:x 0.0 :y 0.0 :w 0.0}))})


(declare stop-avatar)

(def hans {:name "hans"
           :active? (atom false)
           :frequency 15
           ;:percepts {:joint-state {:var state/ella-joint-state :stream-name "ella.skeleton.joint_states"}} ;pose hier nicht definieren, das wird schon bei den "passiven" Menschen oben eingeschaltet
           :commands {:hansmotion {:type :stream :stream-name "hans.hansmotion"
                                   :command-default {:x 0.0 :y 0.0 :w 0.0}
                                   :max-values {:x 1.0 :y 1.0 :w 1.0}}}
           :control-fun (fn [] (if (> (:y @(:hans @state/people)) -1.5)
                                (send-command :hansmotion {:x 0.8 :y 0.0 :w 0.0})
                                (stop-avatar hans)))
                                ;(send-command :hansmotion {:x 0.0 :y 0.0 :w 0.0})))
           :stop-fun    (fn [] (send-command :hansmotion {:x 0.0 :y 0.0 :w 0.0}))})


;; Define and start avatars
(defn run-avatar [{:keys [name active? frequency control-fun stop-fun]}]
  (future
    (try
      (do
        (loop []
          (control-fun)
          (Thread/sleep frequency)
          (when @active?
            (recur)))
        (stop-fun))
      (catch Exception e
        (do (println (type e) " in avatar " name)
            (.printStackTrace e))))))

; initialize-avatars: in core verwendet, um beim Starten Avatare auch zu initialisieren, kann man aber auch irgendwo im Programm aufrufen
(defn initialize-avatars
  ([] (initialize-avatars [ella]))
  ([avatar-definitions] ; hashmap übergeben mit Funktionen, was die Avatare tun sollen
   (doseq [avatar avatar-definitions]
     (start-percepts (:percepts avatar))
     (start-commands (:commands avatar)))))


(defn start-avatar [avatar]
  (reset! (:active? avatar) true)
  (run-avatar avatar))

(defn stop-avatar [avatar]
  (reset! (:active? avatar) false))


