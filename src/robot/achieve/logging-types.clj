; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.achieve)

;;; --------------------------------------------------------------------- ;;;
;;; Typen vereinfachen und zu hash-map machen                             ;;;
;;; --------------------------------------------------------------------- ;;;

;----------------------------------------
; simplified (shorter) types
;; Funktionen, die die oben definierten Typen so vereinfachen, dass man sie in eine Datei schreiben und einlesen kann (d.h. insbesondere alle Funktionen entfernen)
;; aber auch Länge der Bezeichungen kürzen, damit Log-Datei nicht so groß wird

(defmulti simplify-type type)

(defmethod simplify-type :default [arg]
  arg)


(defmethod simplify-type hps.types.DecisionAlternative [{cmd :value prod :produced-by alt :altered-by evl :evaluations agg :aggregated-evaluation}]
  {:v   cmd
   :p   prod
   :a   alt
   :agg agg
   :e   (map (fn [{:keys [value evaluator reason]}]
               (let [basic-eval {:v value :e evaluator}]
                 (if reason
                   (assoc basic-eval :r reason)
                   basic-eval)))
             evl)})

(defmethod simplify-type hps.types.DecisionResult [{d :decision ret :retained-alternatives rej :rejected-alternatives}]
  {:d (simplify-type d) :ret (map simplify-type ret) :rej (map simplify-type rej)})

(defmethod simplify-type hps.types.DecisionSetup [{{evaluators :evaluators producers :producers adaptors :adaptors} :experts alt :alternatives param :parameters}]
  {:e {:ev (map simplify-type evaluators) :pr (map simplify-type producers) :ad (map simplify-type adaptors)}
   :a (map simplify-type alt)
   :p (dissoc param :accept-best-alternative-fun :run-frequency-fun)})

(defmethod simplify-type hps.types.Expert [{:keys [name]}]
  name)


(defmethod simplify-type hps.types.Goaltype [{name :name sv :statevars aexp :abstract-experts param :parameters}]
  {:n name :sv (map meta sv) :ae (map simplify-type aexp) :p param})

(defmethod simplify-type hps.types.Goal [goal]
  (into {} (map #(vector % (get goal %)) (:parameters (:type goal)))))




;;; --------------------------------------------------------------------- ;;;
;;; Hash-maps zu Typen expandieren                                        ;;;
;;; --------------------------------------------------------------------- ;;;

(defmulti expand-type (fn [event-or-typelabel _] event-or-typelabel))

(defmethod expand-type :default [_ arg]
  ; (println "default: " arg)
  arg)

(defmethod expand-type :alternative [_ {cmd :v prod :p alt :a evl :e agg :agg :as alternative}]
  ; (println "expand alternative: " alternative)
  (map->DecisionAlternative {:value       cmd :produced-by prod :altered-by alt :aggregated-evaluation agg
                             :evaluations (map (fn [{:keys [v e r]}] {:value v :evaluator e :reason r})
                                               evl)}))


(defmethod expand-type :decision [_ {d :d ret :ret rej :rej}]
  (map->DecisionResult {:decision                 (if (nil? d) d (expand-type :alternative d))
                        :retained-alternatives    (map #(expand-type :alternative %) ret)
                        :rejected-alternatives    (map #(expand-type :alternative %) rej)}))

(defmethod expand-type :setup [_ {{evaluators :ev producers :pr adaptors :ad} :e alt :a param :p :as data}]
  (let [res
        (map->DecisionSetup {:experts      {:evaluators (map #(expand-type % :evaluator) evaluators)
                                            :producers  (map #(expand-type % :producer) producers)
                                            :adaptors   (map #(expand-type % :adaptor) adaptors)}
                             :alternatives (map #(expand-type :alternative %) alt)
                             :parameters   param})]
    res))


(defmethod expand-type :expert [_ name type]
  (map->Expert {:name name :type type}))


(defmethod expand-type :goaltype [_ {name :n sv :sv aexp :ae param :p}]
  (map->Goaltype {:name name :statevars sv :abstract-experts (map #(expand-type :abstract-expert %) aexp) :parameters param}))
; Statevars evaluieren?

(defmethod expand-type [:goal-achieved :goal-set :goal-failed] [_ goal]
  (map->Goal {:type    (expand-type :goaltype (:tp goal))
              :timeout (:to goal)}))
