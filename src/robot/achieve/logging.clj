; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.achieve)

;; pro dm/source werden die letzte Zeiten gespeichert
;; das log Intervall ist für alle gleich und wird mit dem top-level Ziel gesetzt

(let [last-logs (atom {})                                   ; (into {} (map #(vector % 0) [:decision :command :setup :goal-achieved :goal-set :goal-failed])))
      min-log-interval (atom 200)]                          ; in GUI konfigurierbar

  (defn reset-log-times []
    (reset! last-logs {}))

  (defn get-last-log-time [event-type source]
    (or (get (event-type @last-logs) source) 0))

  (defn store-last-log-time [event-type source time]
    (when-not (some #(= event-type %) [:goal-achieved :goal-set :goal-failed]) ; goal-Ereignisse werden immer aufgezeichnet
      (swap! last-logs assoc-in [event-type source] time)))

  (defn set-log-interval [interval]
    (reset! min-log-interval interval))


  (defn log-trace-data
    ([traceatom event-type data] (log-trace-data traceatom event-type nil data))
    ([traceatom event-type source data]
     (let [now (.getTime (java.util.Date.))]
       (when (> (- now (get-last-log-time event-type source)) @min-log-interval)
         (let [simplified-data
               (case event-type
                 (:decision :setup :goal-achieved :goal-set :goal-failed) (simplify-type data)
                 :command (:value data)
                 :state data
                 "unspecified trace event")]
           (swap! traceatom (fn [currenttrace] (conj currenttrace [now event-type source simplified-data])))
           (store-last-log-time event-type source now))))))
  )


(defn package-trace
  ([trace] (package-trace trace nil))
  ([trace decision-modules]
   (cons {:humans           (vec (keys @robot.state/people))
          :environment      (:name (meta robot.connection/environment*))
          :decision-modules decision-modules}
         (reverse trace))))

;;; --------------------------------------------------------------------- ;;;
;;; logging für mehrere Decision Modules                                  ;;;
;;; --------------------------------------------------------------------- ;;;

(defn log-decision-module-start [trace dm-spec dm-name watch-tag]
  (add-watch (:internal-decision dm-spec) watch-tag
             (fn [_ _ _ newval]
               (log-trace-data trace :decision dm-name newval)))
  (add-watch (:executed-decision dm-spec) watch-tag
             (fn [_ _ _ newval]
               (log-trace-data trace :command dm-name newval)))
  (add-watch (:decision-setup dm-spec) watch-tag
             (fn [_ _ oldval newval]
               ;(when-not (= oldval newval)
               ;(println "setup changed: " (:name dm-spec))
               (log-trace-data trace :setup dm-name newval))) ;)
  (add-watch (:goal dm-spec) watch-tag
             (fn [_ _ oldval newval]
               (when-not (= oldval newval)
                 (when (and oldval (:threshold (:type oldval)))
                   (log-trace-data trace (if (hps.goals/goal-achieved? oldval) :goal-achieved :goal-failed) dm-name oldval))
                 (when newval
                   (log-trace-data trace :goal-set dm-name newval))))))



(defn log-decision-module-stop [trace dm-spec dm-name watch-tag]
  (remove-watch (:internal-decision dm-spec) watch-tag)
  (remove-watch (:executed-decision dm-spec) watch-tag)
  (remove-watch (:decision-setup dm-spec) watch-tag)
  (remove-watch (:goal dm-spec) watch-tag))

(defn log-state-start [trace statevars]
  (let [watch-tag :trace]
    (doseq [sv statevars]
      (add-watch sv watch-tag
                 (fn [_ _ oldval newval]
                   ; bei Zustand bewusst alle aufzeichnen, damit Frequenzberechnung stimmt
                   (log-trace-data trace :state (merge {:name (:name (meta sv))} newval)))))
    (doseq [[personname personpose] @robot.state/people]
      ; einmal am Anfang aufzeichnen, damit Position drin ist, auch wenn sich Mensch nicht bewegt
      (log-trace-data trace :state personname (assoc @personpose :name personname))
      (add-watch personpose watch-tag
                 (fn [_ _ oldval newval]
                   (when-not (= (dissoc oldval :timestamp) (dissoc newval :timestamp))
                     (log-trace-data trace :state personname (assoc newval :name personname))))))))

(defn log-state-stop [trace statevars]
  (let [watch-tag :trace]
    (doseq [sv statevars]
      (remove-watch sv watch-tag))
    (doseq [[_ personpose] @robot.state/people]
      (remove-watch personpose watch-tag))))

