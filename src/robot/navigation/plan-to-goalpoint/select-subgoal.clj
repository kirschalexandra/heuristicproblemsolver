; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; noch keine spezifische Heuristik, sondern Sammlung erster Ideen


;;; ------------------------------------------------------------------- ;;;
;;; Hilfsfunktionen --------------------------------------------------- ;;;
;;; ------------------------------------------------------------------- ;;;
(defn get-coordinate-for-topomap-point [pointname]
  (assoc (select-keys (pointname (:topomap conn/environment*)) [:x :y])
    :name pointname))

(defn get-static-obstacles []
  (concat (mapcat vals (vals (:furniture conn/environment*))) (:inner-walls conn/environment*)))

(defn get-static-obstacles-with-labels []
  (concat (map (fn [[k v]] (assoc v :label (name k))) (reduce merge (vals (:furniture conn/environment*))))
          (:inner-walls conn/environment*)))


;;; ------------------------------------------------------------------- ;;;
;;; Producers --------------------------------------------------------- ;;;
;;; ------------------------------------------------------------------- ;;;

(def-heuristic first-planpoint
               :producer (def-expert-fun [:goalvar plangoal] (first (:path plangoal))))

(def-heuristic reachable-mappoint
               :producer (def-expert-fun [:goalvar plangoal]
                                         (let [result
                                               (reduce (fn [reachable-points pointname]
                                                         ; (println "reachable points: " reachable-points)
                                                         ; (println "pointname: " pointname)
                                                         (let [point (get-coordinate-for-topomap-point pointname)]
                                                           ; (println "point coords: " point)
                                                           ; (println "connectable: " (env/points-connectable? @state/robot-pose point (get-static-obstacles)))
                                                           (if (env/points-connectable? @state/robot-pose point (get-static-obstacles))
                                                             (conj reachable-points point)
                                                             reachable-points)))
                                                       ; reachable-points)
                                                       []
                                                       (:path plangoal))]
                                           (seq result))))

(def-heuristic all-plan-points
               :producer (def-expert-fun [:goalvar plangoal] (map get-coordinate-for-topomap-point (:path plangoal))))

(def-heuristic goalpoint
               :producer (def-expert-fun [:goalvar plangoal] (select-keys @(:parent-goal plangoal) [:x :y :yaw])))

(def visited-points-work-off-plan-producer (atom #{}))
(def current-subgoal-work-off-plan-producer (atom nil))


(def-heuristic work-off-plan
               :producer
               (def-expert-fun [:goalvar plangoal
                                :optional [params {:subgoal-reached-dist 0.3}]
                                :reset-fun (fn []
                                             (reset! visited-points-work-off-plan-producer #{})
                                             (reset! current-subgoal-work-off-plan-producer nil))]
                               (if @current-subgoal-work-off-plan-producer
                                 (when (and @current-subgoal-work-off-plan-producer
                                            (not= (deref current-subgoal-work-off-plan-producer) :goal)
                                            (< (util/point-distance @state/robot-pose
                                                                    (get-coordinate-for-topomap-point @current-subgoal-work-off-plan-producer))
                                               (:subgoal-reached-dist params)))
                                   (let [next-point-in-plan (loop [current-point @current-subgoal-work-off-plan-producer
                                                                   rest-plan (:path plangoal)]
                                                              (when (seq rest-plan) (if (and (= current-point (first rest-plan)) (not (contains? (deref visited-points-work-off-plan-producer) (second rest-plan)))) (second rest-plan) (recur current-point (rest rest-plan)))))]
                                     (swap! visited-points-work-off-plan-producer conj @current-subgoal-work-off-plan-producer)
                                     (reset! current-subgoal-work-off-plan-producer next-point-in-plan)))
                                 (reset! current-subgoal-work-off-plan-producer (first (:path plangoal))))
                               (when (nil? @current-subgoal-work-off-plan-producer) ; ist nötig, weil sonst der aktuelle Punkt (nil) identisch mit dem ersten des leeren Restplans ist
                                 (reset! current-subgoal-work-off-plan-producer :goal))
                               (if (= @current-subgoal-work-off-plan-producer :goal)
                                 (select-keys @(:parent-goal plangoal) [:x :y :yaw])
                                 (get-coordinate-for-topomap-point @current-subgoal-work-off-plan-producer))))


;;; ------------------------------------------------------------------- ;;;
;;; Evaluators -------------------------------------------------------- ;;;
;;; ------------------------------------------------------------------- ;;;

(def-heuristic prefer-point-near-goal
               :evaluator
               (def-expert-fun [alts :goalvar plangoal]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [main-goal @(:parent-goal plangoal)
                                         eval-value (util/point-distance (:value proposed-alternative) @(:parent-goal plangoal))
                                         scaled-eval-value (util/compare-to-ideal
                                                             {:dist (util/point-distance (:value proposed-alternative) @(:parent-goal plangoal))} ; proposed-map
                                                             {:dist 0.0} ; ideal-map
                                                             {:dist 0.0} ; min-values-map
                                                             {:dist (apply max (map (comp #(util/point-distance % @(:parent-goal plangoal)) ; max-values-map
                                                                                          get-coordinate-for-topomap-point)
                                                                                    (:path plangoal)))})]

                                     [(if main-goal
                                        scaled-eval-value
                                        0.0)                ; man kann streiten, ob in diesem Fall 0 oder 1 zurückgegeben wird, auf jeden Fall werden alle Alternativen den gleichen Wert bekommen
                                      {:main-goal main-goal}]))
                                 alts)))


(def-heuristic prefer-late-point-in-plan
               :evaluator
               (def-expert-fun [alts :goalvar plangoal]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [proposed-point-name (:name (:value proposed-alternative))]
                                     (if proposed-point-name
                                       (loop [[current-plan-point & restplan] (:path plangoal)
                                              position 1]
                                         (cond (nil? current-plan-point)
                                               0            ; Punkt ist gar nicht im Plan, sollte eigentlich nicht vorkommen
                                               (= current-plan-point proposed-point-name)
                                               (/ position (count (:path plangoal)))
                                               :else
                                               (recur restplan (inc position))))
                                       1.0)))               ; Punkt ist nicht im Plan; eigentlich müsste man sowas wie "ich halte mich raus" signalisieren, aber da momentan der einzige vorgeschlagene Punkt, der nicht im Plan ist, der Zielpunkt ist, bewerte ich ihn hoch
                                 alts)))

(def-heuristic keep-orientation
               :evaluator
               (def-expert-fun [alts :goalvar plangoal]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (util/compare-to-ideal
                                     {:orientation-diff (util/delta-radian (:yaw @state/robot-pose) (util/radian-between-points @state/robot-pose proposed-alternative))}
                                     {:orientation-diff 0.0}
                                     {:orientation-diff 0.0}
                                     {:orientation-diff (apply max (map (comp #(util/delta-radian (:yaw @state/robot-pose) %)
                                                                              #(util/radian-between-points @state/robot-pose %)
                                                                              get-coordinate-for-topomap-point)
                                                                        (:path plangoal)))}))
                                 alts)))


(def-heuristic prevent-unreachable-points
               :evaluator
               (def-expert-fun [alts :goalvar plangoal]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [current-pose @state/robot-pose]
                                     (if (env/points-connectable? current-pose (:value proposed-alternative) (get-static-obstacles))
                                       1.0                  ; man könnte hier auch eine interessantere Funktion anwenden, z.B. wie weit man Hindernisse umfährt
                                       [nil {:robot-pose (select-keys current-pose [:x :y])
                                             :obstacles  (env/get-obstacles-between-points current-pose (:value proposed-alternative) (get-static-obstacles-with-labels))}])))
                                 alts)))


; weitere Evaluatoren möglich mit Orientierung, z.B. Bewegungsrichtung beibehalten
