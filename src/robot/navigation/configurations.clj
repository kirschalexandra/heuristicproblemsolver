; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

(def dm-configurations
  (atom
    {:test
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts
                    {:producers  [:p-control
                                  [:random :quantity 3]
                                  [:turn :quantity 1]
                                  :repeat-command
                                  :motion-primitives]
                     :evaluators [[:safety-laser :weight 0.19]
                                  [:safety-tables :weight 0.21]
                                  [:p-control :weight 0.5]
                                  [:dwa-align :weight 0.25]
                                  [:look-at-goalpoint :weight 1.00]
                                  [:look-at-path :weight 0.32]
                                  [:velocity :weight 0.47]
                                  [:goal-distance :weight 0.77]
                                  [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:producers  [[:random :quantity 1]]
                              :evaluators [[:p-control :weight 0.5]
                                           [:dwa-align :weight 0.57]
                                           [:look-at-goalpoint :weight 0.40]
                                           [:look-at-path :weight 0.84]
                                           [:velocity :weight 1.00]
                                           [:goal-distance :weight 0.81]]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-sub-turn
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [:look-at-goalpoint]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-sub-towards-goal
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [:towards-goalpose]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-sub-towards-goal-20
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [:towards-goalpose]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-sub-turn-towards
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [[:look-at-goalpoint :weight 1.0]
                                           [:towards-goalpose :weight 1.0]]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-sub
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [[:dwa-align :weight 0.57]
                                           [:look-at-goalpoint :weight 0.40]
                                           [:look-at-path :weight 0.84]
                                           [:velocity :weight 1.00]
                                           [:goal-distance :weight 0.81]]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :hps-base
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           [:prefer-point-near-goal :weight 0.5]
                                           [:prefer-late-point-in-plan :weight 0.8]]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [:accept-all]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :classical
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [[:a-star :weight 1.0]]}}
      :subgoals    {:experts {:producers  [:work-off-plan]
                              :evaluators [:accept-all]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 31]
                                           :turn
                                           :repeat-command
                                           :motion-primitives]
                              :evaluators [[:safety-laser :weight 0.19]
                                           [:safety-tables :weight 0.21]
                                           [:dwa-align :weight 0.25]
                                           [:look-at-goalpoint :weight 1.00]
                                           [:look-at-path :weight 0.32]
                                           [:velocity :weight 0.47]
                                           [:goal-distance :weight 0.77]
                                           [:towards-goalpoint :weight 0.92]]}}
      :nav-complex {:experts {:evaluators [:accept-all]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :dwa-realistic
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           :prefer-point-near-goal]}}
      :nav-simple  {:experts {:producers  [[:random :quantity 50]] ; sollte eigentlich dwa-discretisation sein
                              :evaluators [[:safety-humans :weight 0.0] ; safety evaluators bleiben diesmal, aber alle mit 0 gewichtet werden um andere Entscheidungen nicht zu beeinflussen
                                           [:safety-tables :weight 0.0]
                                           [:safety-laser :weight 0.0]
                                           [:dwa-align :weight 0.8]
                                           [:dwa-velocity :weight 0.1]
                                           [:dwa-goal-region :weight 0.1]]}}
      :nav-complex {:experts {:evaluators [:accept-all]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :p-control-naive
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:work-off-plan]
                              :evaluators [:accept-all]}}
      :nav-simple  {:experts {:producers  [:p-control]
                              :evaluators [[:p-control :weight 0.5]]}}
      :nav-complex {:experts {:evaluators [:accept-all]}}}
     ;; ------------------------------------------------------------------------------------------ ;;
     :minimal
     {:plan        {:experts {:producers  [:a-star]
                              :evaluators [:a-star]}}
      :subgoals    {:experts {:producers  [:all-plan-points
                                           :goalpoint]
                              :evaluators [:prevent-unreachable-points
                                           :prefer-point-near-goal]}}
      :nav-simple  {:experts {:producers  [:p-control]
                              :evaluators [[:p-control :weight 0.5]]}}
      :nav-complex {:experts {:evaluators [:accept-all]}}}
     }))
