; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: get closer to your goal

; genutzter Teil des inneren Zustands:
; statevar robot-pose
; Navigationsziel nav-goaltype

; Funktionen
(defn state-goal-distance [current-pose goal-pose]
  (let [{x0 :x y0 :y yaw0 :yaw} current-pose
        {xg :x yg :y yawg :yaw} goal-pose]
    ; berechnet euklidsche Distanz, nur dass Winkeldifferenz richtig skaliert wird
    (math/sqrt (reduce + (map #(math/expt % 2) [(- xg x0) (- yg y0) (util/difference-radian yawg yaw0)])))))

(defn state-goal-point-distance [current-pose goal-pose]
  (let [{x0 :x y0 :y} current-pose
        {xg :x yg :y} goal-pose]
    ; berechnet euklidsche Distanz, nur dass Winkeldifferenz richtig skaliert wird
    (math/sqrt (reduce + (map #(math/expt % 2) [(- xg x0) (- yg y0)])))))

(defn vector-towards-goal [current-pose goal-pose]
  (let [{x0 :x y0 :y yaw0 :yaw} current-pose
        {xg :x yg :y yawg :yaw} goal-pose
        x-diff (- xg x0)
        y-diff (- yg y0)
        sin-yaw0 (Math/sin yaw0)
        cos-yaw0 (Math/cos yaw0)]
    [(+ (* y-diff sin-yaw0) (* x-diff cos-yaw0))
     (- (* y-diff cos-yaw0) (* x-diff sin-yaw0))
     (util/difference-radian yawg yaw0)]))


; Experten
(defn calculate-p-command
  ([navgoal] (calculate-p-command navgoal 1.2))
  ([navgoal pp]
    ; (pp 1.2) wie in Original-p-controller funktioniert gar nicht, wahrscheinlich weil control loop viel langsamer ist
    ; mit voller Geschwindigkeit 1.0 funktioniert (pp 0.15)
   (into {}
         (map (fn [distval cmdvar]
                (let [limit (cmdvar (:max-values (:motion (:commands robot.connection/robot*))))]
                  [cmdvar (util/limit-to-range (* pp distval) (- limit) limit)]))
              (vector-towards-goal @state/robot-pose navgoal)
              [:x :y :w]))))


(def-heuristic p-control
               :producer (def-expert-fun [:goalvar navgoal :optional [params {:p-value 1.2}]]
                                         [:motion (calculate-p-command navgoal (:p-value params))])
               :evaluator (def-expert-fun [alts :goalvar navgoal :optional [params {:p-value 1.2}]]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (util/compare-to-ideal
                                                (second (:value proposed-alternative)) ; erstes Argument müsste :motion sein, wird hier einfach angenommen, dass es passt
                                                (calculate-p-command navgoal (:p-value params))
                                                {:x 0 :y 0 :w 0}
                                                (:max-values (:motion (:commands robot.connection/robot*)))))
                                            alts)))

(def-heuristic towards-goalpose
               :producer (def-expert-fun [:goalvar navgoal :optional [params {:p-values (range 0.6 1.8 0.2)}]]
                                         (map #(vector :motion (calculate-p-command navgoal %))
                                              (:p-values params)))
               :evaluator (def-expert-fun [alts :goalvar navgoal]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [current-dist (state-goal-distance @state/robot-pose navgoal)
                                                    predicted-dist (state-goal-distance (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000) navgoal)
                                                    delta-dist (- current-dist predicted-dist)]
                                                (util/compare-to-ideal
                                                  {:delta-dist-rel (if (< current-dist 0.1) ; division durch null bzw. kleine Zahl vermeiden, in diesem Fall absoluten Abstand verwenden
                                                                     delta-dist
                                                                     (/ delta-dist current-dist))}
                                                  {:delta-dist-rel 1.0} ; eigentlich kann man das nicht wirklich sagen, man müsste hier eher Alternativen sortieren
                                                  {:delta-dist-rel -1.0} ; min und max Abstand könnte man besser als Parameter machen
                                                  {:delta-dist-rel 1.0})))
                                            alts)))

(def-heuristic towards-goalpoint
               :producer (def-expert-fun [:goalvar navgoal :optional [params {:p-values (range 0.6 1.8 0.2)}]]
                                         (map #(vector :motion (calculate-p-command (assoc navgoal :yaw (util/radian-between-points @state/robot-pose navgoal)) %))
                                              (:p-values params)))
               :evaluator (def-expert-fun [alts :goalvar navgoal]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [current-dist (state-goal-point-distance @state/robot-pose navgoal)
                                                    predicted-dist (state-goal-point-distance (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000) navgoal)
                                                    delta-dist (- current-dist predicted-dist)]
                                                ;(println "current-dist: " current-dist ", predicted dist: " predicted-dist ", delta-dist: " delta-dist)
                                                (util/compare-to-ideal
                                                  {:delta-dist-rel (if (< current-dist 0.1) ; division durch null bzw. kleine Zahl vermeiden, in diesem Fall absoluten Abstand verwenden
                                                                     delta-dist
                                                                     (/ delta-dist current-dist))}
                                                  {:delta-dist-rel 1.0} ; eigentlich kann man das nicht wirklich sagen, man müsste hier eher Alternativen sortieren
                                                  {:delta-dist-rel -1.0} ; min und max Abstand könnte man besser als Parameter machen
                                                  {:delta-dist-rel 1.0})))
                                            alts)))

(def-heuristic goal-distance
               :evaluator (def-expert-fun [alts :goalvar navgoal]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [current-dist (state-goal-point-distance @state/robot-pose navgoal)
                                                    predicted-dist (state-goal-point-distance (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000) navgoal)
                                                    delta-dist (- current-dist predicted-dist)]
                                                (util/compare-to-ideal
                                                  {:delta-dist-rel predicted-dist}
                                                  {:delta-dist-rel 0.0}
                                                  {:delta-dist-rel 0.0}
                                                  {:delta-dist-rel current-dist})))
                                            alts)))


