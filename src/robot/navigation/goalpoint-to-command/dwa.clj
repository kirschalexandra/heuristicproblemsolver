; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: keine eigenen Heuristiken, sondern Strategie aus DWA

; genutzter Teil des inneren Zustands:
; statevar robot-pose

(def-heuristic dwa-align
               :evaluator
               (def-expert-fun [alts :goalvar navgoal :optional [params {:worst-possible-value Math/PI}]] ; "move in direction of goal"
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [predicted-pose (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000)
                                         motion-direction-angle (util/radian-between-points @state/robot-pose predicted-pose)
                                         goal-heading (util/radian-between-points @state/robot-pose navgoal)]
                                     (util/compare-to-ideal
                                       {:delta-radian (util/delta-radian motion-direction-angle goal-heading)}
                                       {:delta-radian 0.0}
                                       {:delta-radian 0.0}
                                       {:delta-radian (:worst-possible-value params)})))
                                 alts)))

(def-heuristic dwa-velocity
               :evaluator
               (def-expert-fun [alts :goalvar navgoal :optional [params {:max-vel (math/sqrt 2.0) :goal-region-radius 0.5}]]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [proposed-command (second (:value proposed-alternative))
                                         relative-total-velocity (/ (math/sqrt (+ (math/expt (:x proposed-command) 2) (math/expt (:y proposed-command) 2)))
                                                                    (:max-vel params))]
                                     (if (< (util/point-distance @state/robot-pose navgoal) (:goal-region-radius params))
                                       (- 1 relative-total-velocity)
                                       relative-total-velocity)))
                                 alts)))


(def-heuristic dwa-goal-region
               :evaluator
               (def-expert-fun [alts :goalvar navgoal :optional [params {:goal-region-radius 0.5}]]
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (if (< (util/point-distance (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000)
                                                               navgoal)
                                          (:goal-region-radius params))
                                     1
                                     0))
                                 alts)))

