; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: do what worked before

; genutzter Teil des inneren Zustands:
; statevars robot-pose (mit History) bzw. Motion Primitives ohne alles

; Menschen werden nicht vom Laser erkannt!

(def-heuristic repeat-command
  :producer (def-expert-fun []
              (when (first (:history (meta (:motion @conn/command-variables))))
                [:motion (first (:history (meta (:motion @conn/command-variables))))])))

; Motion Promitives: Da kann man sich streiten, ob sie zu "constancy" passen, aber auf Dauer wäre es wünschenswert, wenn die Motion Primitives gelernt werden würden


(def-heuristic motion-primitives
  :producer (def-expert-fun []
              (let [{xmax :x ymax :y wmax :w} (:max-values (:motion (:commands robot.connection/robot*)))]
                (map (fn [[x y w]] [:motion {:x x :y y :w w}])
                     (list
                       [(mut/randbetween 0 xmax) 0.0 0.0]          ; move forward
                       [(mut/randbetween (- xmax) 0) 0.0 0.0]      ; move backward
                       [0.0 (mut/randbetween 0 ymax) 0.0]          ; move left
                       [0.0 (mut/randbetween (- ymax) 0) 0.0]      ; move right
                       [0.0 0.0 (mut/randbetween 0 wmax)]          ; turn left
                       [0.0 0.0 (mut/randbetween (- wmax) 0)]))))) ; turn right

(def-heuristic turn ; müsste man eigentlich per Parameter aus motion-primitives bekommen können
  :producer (def-expert-fun [:optional [params {:quantity 10}]]
              (let [{wmax :w} (:max-values (:motion (:commands robot.connection/robot*)))]
                (mapcat (fn [w] [[:motion {:x 0 :y 0 :w w}] [:motion {:x 0 :y 0 :w (- w)}]])
                        (take (:quantity params) (iterate (fn [_] (mut/randbetween 0 wmax))
                                                 (mut/randbetween 0 1))))))
  :adaptor (def-expert-fun [{command :value}]
            (println "turn adaptor")
            (let [{x-old :x y-old :y w-old :w} (second command)
                  {wmax :w} (:max-values (:motion (:commands conn/robot*)))]
              [:motion {:x x-old :y y-old :w (mut/randbetween (- wmax) wmax)}])))


