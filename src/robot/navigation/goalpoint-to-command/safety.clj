; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: don't hit anyone or anything

; genutzter Teil des inneren Zustands:
; statevars robot-pose, laser-data, people
; environment furniture

; Menschen werden nicht vom Laser erkannt!

;;; ------------------------------------------------------------------- ;;;
;;; Auxiliary Functions ----------------------------------------------- ;;;
;;; ------------------------------------------------------------------- ;;;

(defn sample-robot-outline
  ([pose] (sample-robot-outline pose conn/robot*))
  ([pose robot]
   (let [{:keys [x y yaw]} pose
         robotHalfDepth (:half-depth (:constants robot))
         robotHalfWidth (:half-width (:constants robot))
         robotEdgeRadius 0.1
         robot-diagonal (math/sqrt (+ (math/expt robotHalfDepth 2) (math/expt robotHalfWidth 2)))
         ; (- (math/sqrt (+ (math/expt robotHalfDepth 2) (math/expt robotHalfWidth 2)))
         ;    (- (math/sqrt (* 2 (math/expt robotEdgeRadius 2))) robotEdgeRadius))
         corner-angle-right (- yaw (/ Math/PI 4))
         corner-angle-left (+ yaw (/ Math/PI 4))
         frontal-point-x (* robotHalfDepth (Math/cos yaw))
         frontal-point-y (* robotHalfDepth (Math/sin yaw))
         sideward-point-x (* robotHalfWidth (Math/sin yaw))
         sideward-point-y (* robotHalfWidth (Math/cos yaw))
         right-corner-x (* robot-diagonal (Math/sin corner-angle-right))
         right-corner-y (* robot-diagonal (Math/cos corner-angle-right))
         left-corner-x (* robot-diagonal (Math/sin corner-angle-left))
         left-corner-y (* robot-diagonal (Math/cos corner-angle-left))]
     [pose                                                  ; Mittelpunkt
      {:x (+ x frontal-point-x) :y (+ y frontal-point-y)}   ; vorn
      {:x (- x frontal-point-x) :y (- y frontal-point-y)}   ; hinten
      {:x (+ x sideward-point-x) :y (- y sideward-point-y)} ; Seiten
      {:x (- x sideward-point-x) :y (+ y sideward-point-y)} ;
      {:x (+ x right-corner-x) :y (- y right-corner-y)}     ; Ecken
      {:x (- x right-corner-x) :y (+ y right-corner-y)}
      {:x (+ x left-corner-x) :y (- y left-corner-y)}
      {:x (- x left-corner-x) :y (+ y left-corner-y)}])))


;;; ------------------------------------------------------------------- ;;;
;;; Evaluators -------------------------------------------------------- ;;;
;;; ------------------------------------------------------------------- ;;;

(def-heuristic safety-humans
               :evaluator
               (def-expert-fun [alts :goalvar navgoal
                                :optional [params {:human-robot-safety-distance 0.655 ; halbe Roboterbreite plus Radius Mensch
                                                   :max-human-robot-distance    3.0}]] ; 3m sind "public zone"
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (if (empty? @state/people)
                                     1.0
                                     (let [predicted-poses (map #(hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time %) '(250 500 750 1000 1250 1500))
                                           distances-to-humans (into {} (map (fn [[id pose]] {id (apply min (map #(util/point-distance @pose %) predicted-poses))})
                                                                             @state/people))
                                           min-distance-to-humans (apply min (vals distances-to-humans))]
                                       [(when-not (<= min-distance-to-humans (:human-robot-safety-distance params)) (util/compare-to-ideal {:safetydist min-distance-to-humans} {:safetydist (:max-human-robot-distance params)} {:safetydist (:human-robot-safety-distance params)} {:safetydist (:max-human-robot-distance params)}))
                                        {:predicted-distances distances-to-humans}])))
                                 alts)))

(def-heuristic human-approach
               :evaluator
               (def-expert-fun [alts :goalvar navgoal :optional [params {:human-robot-safety-distance 0.5}]] ; bewusst knapper als bei safety-humans-evaluator, weil der eigentlich schon die Kollisionsfälle rauswerfen sollte
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (if (empty? @state/people)
                                     1.0
                                     (let [current-pose @state/robot-pose
                                           predicted-pose (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000)
                                           delta-distances-to-humans (into {} (map (fn [[id pose]] {id (- (util/point-distance @pose current-pose) ; <alter Abstand> - <neuer Abstand>
                                                                                                          (util/point-distance @pose predicted-pose))})
                                                                                   @state/people))
                                           max-approach-to-humans (apply max-key val delta-distances-to-humans)
                                           maximum-possible-approach (util/point-distance @((key max-approach-to-humans) @state/people) current-pose)] ; schlimmster Wert ist der aktuelle Abstand, wäre äquivalent zu Kollision
                                       [(when-not (< (- maximum-possible-approach (val max-approach-to-humans)) (:human-robot-safety-distance params)) (util/compare-to-ideal {:approach (val max-approach-to-humans)} {:approach 0.0} {:approach 0.0} {:approach maximum-possible-approach}))
                                        {:predicted-approaches delta-distances-to-humans}])))
                                 alts)))


(defn get-laser-reading [robot-pose predicted-movement-angle laser-ranges
                         {min-scan-angle :min-scan-angle max-scan-angle :max-scan-angle}]
  (let [predicted-angle (util/difference-radian (:yaw robot-pose) predicted-movement-angle)
        maxindex (dec (count laser-ranges))
        laser-segment (+ (/ (* maxindex predicted-angle) (- min-scan-angle max-scan-angle))
                         (/ maxindex 2))]
    (if (or (neg? laser-segment) (> laser-segment maxindex))
      :out-of-bounds
      (min (nth laser-ranges (math/floor laser-segment))
           (nth laser-ranges (math/ceil laser-segment))))))


; safety-laser: ist nur sehr grob, da er nur auf den vorhergesagten Punkt testet
; habe versucht ihn ähnlich wie Table Evaluator mit verschiedenen Sample-Punkten zu verwenden, aber dann hat man z.B. das Problem, dass bei geringen Geschwindigkeiten
; einige Roboterpunkte hinter dem Laser liegen und er das als out-of-bounds betrachtet, wodurch die Gesamtevaluation seltsam wird
; berücksichtigt auch nicht die Breite des Roboters
; und: Wände, Gegenstände und Menschen werden eh abgefangen, eigentlich ist das nur noch für Notfälle
(def-heuristic safety-laser
               :evaluator
               (def-expert-fun [alts :goalvar navgoal
                                :optional [params {:min-scan-angle  (- (/ Math/PI 2))
                                                   :max-scan-angle  (/ Math/PI 2)
                                                   :max-laser-range 3.0}]]
                               ; max-laser-range könnte man auch ordentlicher aus Morse auslesen:
                               ; (get (get (clojure.data.json/read-str (second (invoke-service-with-reply "robot.laserscanner" "get_properties" [])))
                               ;           "properties")
                               ;      "laser_range")
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [[predicted-pose predicted-translation predicted-angle] (hps.state/predict+ state/robot-pose :command (:value proposed-alternative) :delta-time 1000)
                                         laser-reading (get-laser-reading @state/robot-pose predicted-angle (:range_list @state/laser-data) params)]
                                     [(cond
                                        (= laser-reading :out-of-bounds) 0.0
                                        (< laser-reading predicted-translation) nil ; sicherheitsgefährdend, evtl. Sicherheitsabstand einbauen
                                        :else (util/compare-to-ideal
                                                {:safetydist (- laser-reading predicted-translation)}
                                                {:safetydist (:max-laser-range params)}
                                                {:safetydist 0.0}
                                                {:safetydist (:max-laser-range params)}))
                                      {:laser-reading laser-reading}]))
                                 alts)))


(defn prefilter-furniture [predicted-pose objects]
  (letfn [(half-diagonal [{w :width d :depth}]
            (math/sqrt (+ (math/expt w 2) (math/expt d 2))))]
    (filter (fn [[_ obj-data]]
              (< (util/point-distance predicted-pose (:pose obj-data))
                 (+ 0.49 (half-diagonal (:size obj-data))))) ; Roboterdiagonale plus größte Ausdehnung Möbelstück in der Schräge
            objects)))


(defn detect-robot-table-collisions [robot-poses relevant-objects]
  (let [robot-surface-points (mapcat sample-robot-outline robot-poses)
        min-distance-per-object (into {} (map (fn [[obj-id obj-data]]
                                                {obj-id
                                                 (apply min
                                                        (map #(util/point-dist-to-rectangle % (env/get-global-coordinate-limits obj-data))
                                                             robot-surface-points))})
                                              relevant-objects))
        min-distance-to-all-objects (apply min (vals min-distance-per-object))]
    [min-distance-per-object min-distance-to-all-objects]))

(defn get-relevant-objects [pose]
  (let [current-rooms (keys (filter (fn [[k v]] (util/point-in-rectangle pose (env/get-global-coordinate-limits v))) (:rooms conn/environment*)))
        ; current-rooms: sollte immer nur ein Raumname rauskommen, aber falls ich es mal erweitere mit mehreren Roboterpunktion kann es auch als Liste nicht schaden
        relevant-furniture (prefilter-furniture pose (mapcat #(get (:furniture conn/environment*) %) current-rooms))
        ; relevant-furniture: zum Ausfiltern genügt größte Vorhersage
        relevant-walls (map (fn [{:keys [label] :as wall-spec}] [label (select-keys wall-spec [:pose :size])])
                            (mapcat (fn [wall-fun]
                                      (mapcat (fn [room]
                                                (filter (fn [wall] (some #(= room %) (:adjacent-rooms wall)))
                                                        (wall-fun conn/environment*)))
                                              current-rooms))
                                    [:inner-walls :outer-walls]))]
    (concat relevant-furniture relevant-walls)))

(def-heuristic safety-tables
               :evaluator
               (def-expert-fun [alts :goalvar navgoal
                                :optional [params {:robot-table-safety-distance 0.005 ; an Einrichtungsgegenständen kann der Roboter nah vorbeifahren
                                                   :max-robot-table-distance    0.7}]] ; ca. einfache Roboterbreite
                               (utmisc/map-to-kv
                                 (fn [proposed-alternative]
                                   (let [relevant-objects (get-relevant-objects @state/robot-pose)
                                         ; ich nehme erstmal nur die Objekte in der Nähe des Ausgangspunktes, man könnte unten zusätzlich die Objekte der weiteren Vorhersage hinzufügen,
                                         ; aber bei den kurzen Zeitspannen sollte es hoffentlich keinen Unterschied machen
                                         [min-dist-per-object min-dist-to-all-objects] (detect-robot-table-collisions [@state/robot-pose] relevant-objects)]
                                     (cond (empty? relevant-objects) 1.0 ; keine gefährlichen Objekte in der Nähe
                                           (<= min-dist-to-all-objects 0.0001) ; wenn Roboter eh schon in Objekt hängt, sollte er versuchen wieder rauszukommen
                                           (let [predicted-poses (map #(hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time %) '(250 500 750)) ; in diesem Fall nur in der näheren Umgebung schauen
                                                 [min-distance-per-object min-distance-to-all-objects] (detect-robot-table-collisions predicted-poses relevant-objects)]
                                             [(util/compare-to-ideal
                                                {:safetydist min-distance-to-all-objects}
                                                {:safetydist (:max-robot-table-distance params)}
                                                {:safetydist (:robot-table-safety-distance params)}
                                                {:safetydist (:max-robot-table-distance params)})
                                              {:relevant-object-distances-now       min-dist-per-object
                                               :relevant-object-distances-predicted min-distance-per-object
                                               :pose-predictions                    predicted-poses}])
                                           :else (let [predicted-poses (map #(hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time %) '(250 500 750 1000 1250 1500))
                                                       ; mehrere Poses vorhersagen: Wenn man nur die knappere nimmt, ist die Vorwarnung zu kurz
                                                       ; wenn man nur die weitere nimmt, kann es sein, dass der Endpunkt sicher ist, aber Zwischenpunkte zu Kollisionen führen würden

                                                       [min-distance-per-object min-distance-to-all-objects] (detect-robot-table-collisions predicted-poses relevant-objects)]
                                                   [(if (<= min-distance-to-all-objects (:robot-table-safety-distance params))
                                                      nil
                                                      (util/compare-to-ideal
                                                        {:safetydist min-distance-to-all-objects}
                                                        {:safetydist (:max-robot-table-distance params)}
                                                        {:safetydist (:robot-table-safety-distance params)}
                                                        {:safetydist (:max-robot-table-distance params)}))
                                                    {:relevant-object-distances min-distance-per-object
                                                     :pose-predictions          predicted-poses}]))))
                                 alts)))


;; -------------------------------------------------------------- ;;
;; -- ADAPTORS -------------------------------------------------- ;;
;; -------------------------------------------------------------- ;;

; Kommando verlangsamen
(def-heuristic slow-down
               :adaptor (def-expert-fun [{command :value evaluations :evaluations} :goalvar navgoal]
                                        (when (some (fn [[{expert :name} e]]
                                                      (and (nil? e)
                                                           (some #(= expert %) '(safety-tables-evaluator safety-laser-evaluator safety-humans-evaluator))))
                                                    evaluations)
                                          (let [{x-old :x y-old :y w-old :w} (second command)]
                                            [:motion {:x (/ x-old 2) :y (/ y-old 2) :w w-old}]))))


