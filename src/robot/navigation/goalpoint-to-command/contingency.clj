; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: randomly try something that is at least relevant to the goal

; genutzter Teil des inneren Zustands: nichts
; wenn das Programm eine Verbindung herstellen könnte zwischen den statevars des Zieltyps (also Position) und Kommandos, die diese verändern (also Geschwindigkeit),
; könnte man diesen Producer sogar problemunabhängig machen


; accept-all-evaluator: dummy für nav-complex DM im top-down Modus -> könnte man evtl. in einer Heuristik mit random-producer zusammenfassen
(def-heuristic accept-all
               :evaluator (def-expert-fun [alts] (utmisc/map-to-kv (fn [_] 1.0) alts)))

(def-heuristic random
               :producer (def-expert-fun [:goalvar goal :optional [params {:quantity 10}]]
                                         (let [{xmax :x ymax :y wmax :w} (:max-values (:motion (:commands robot.connection/robot*)))]
                                           (repeatedly
                                             (:quantity params)
                                             #(vector :motion
                                                      (hash-map :x (mut/randbetween (- xmax) xmax)
                                                                :y (mut/randbetween (- ymax) ymax)
                                                                :w (mut/randbetween (- wmax) wmax)))))))
