; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.core)

(declare start-robot)

(defn screenshot-session
  ([dir x-vals] (screenshot-session dir x-vals :straight))
  ([dir x-vals orientation]
   ;(.exec (Runtime/getRuntime) (str "rm " screenshot-tmp-dir "*"))
   (doseq [[x ii] (map list x-vals (range (count x-vals)))]
     (println "iteration: " ii ", x: " x)
     (start-robot :start-hps-gui false
                  :start-sim true
                  :morse-script-file "pr2-geradeaus.py"
                  :robot-start-pose {:x x :y 0.0 :yaw (case orientation :straight 0.0 :right (/ Math/PI 2) :left (/ Math/PI -2) :back Math/PI) })
     (Thread/sleep 6500)
     (.exec (Runtime/getRuntime) (format "gnome-screenshot -f /work/kirsch/animationsprojekt/final-nur-bilder/%s/%0,3d.png" dir (int ii)))
     (println "screenshot done")
     (Thread/sleep 2000)
     (.exec (Runtime/getRuntime) "pkill blender")
     (println "killed blender"))))
;;;; cd /work/kirsch/animationsprojekt/tmp
;;;; for i in *.png; do convert -crop 1421x609+192+405 "${i}" ../fertig-nur-bilder/vel-...-below/"${i}"; done

;;; Bilder im eigenen Verzeichnis zuschneiden:
;;; for i in *.png; do convert -crop 1421x609+192+405 "${i}" "${i}"; done

;; xvals:for i in *.png; do convert -crop 1421x609+192+405 "${i}" "${i}"; done
;; constant: (take 10 (iterate #(+ % (/ 5 9)) 0.0))
;; inc: (map #(* (* % %) (/ 5.0 81)) (range 10))
;; dec: (map #(* (/ 10.0 9) (- % (/ (* % %) 18))) (range 10))
;; sinusoidal: (map #(- (* (/ 5 9) %) (/ (* 0.4 (Math/cos (+ (/ (* 3 Math/PI %) 9) (/ Math/PI 2)))) (/ (* 3 Math/PI) 9))) (range 10))
