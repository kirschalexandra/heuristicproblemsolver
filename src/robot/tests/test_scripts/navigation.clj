; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.core)

(declare start-robot)

(def configurations-to-test [:simple :classical :hps-base :hps-sub]) ;[:hps-sub :hps-sub-towards-goal :hps-sub-towards-goal-20 :hps-base]) ;[:hps-sub :hps-sub-turn :hps-sub-towards-goal :hps-sub-turn-towards]); [:simple :classical :hps-base :hps-sub :hps-sub-turn]) ;[:hps-sub-turn]) ;::simple]); :classical :hps-base :hps-sub]) ; [:c1 :c2 :c3 :c4] :classical :hps-base :hps-sub])

;; --------------------------------------------------------------------- ;;
;; -- Automatische Testläufe ------------------------------------------- ;;
;; --------------------------------------------------------------------- ;;

(def training-data [{:x -4.92 :y  1.68 :yaw  0.27 :name "bathroom"}
                    {:x  3.35 :y -0.79 :yaw  0.90 :name "livingroom1"}
                    {:x  4.95 :y -1.68 :yaw  0.65 :name "livingroom2"}
                    {:x -5.25 :y -1.19 :yaw -0.53 :name "corridor"}
                    {:x  1.51 :y -3.28 :yaw -0.42 :name "bedroom1"}
                    {:x  0.22 :y -4.02 :yaw  1.20 :name "bedroom2"}
                    {:x -1.24 :y  0.61 :yaw  0.96 :name "kitchen1"}
                    {:x  1.32 :y -0.55 :yaw -1.62 :name "kitchen2"}])

(def test-data [{:x -4.89 :y  1.89 :yaw -0.89 :name "bathroom"}
                {:x  5.73 :y -0.89 :yaw  2.89 :name "livingroom1"}
                {:x  3.53 :y -1.21 :yaw -0.64 :name "livingroom2"}
                {:x -2.36 :y -1.59 :yaw -1.43 :name "corridor"}
                {:x  0.52 :y -4.92 :yaw  1.92 :name "bedroom1"}
                {:x  2.90 :y -5.52 :yaw -1.76 :name "bedroom2"}
                {:x -0.85 :y  2.50 :yaw  0.23 :name "kitchen1"}
                {:x  0.40 :y  1.07 :yaw  1.57 :name "kitchen2"}])


(defn test-navigation []
  (let [goal-points test-data
        configurations configurations-to-test ;:hpstest :p-control-naive]; ]
        repetitions 10]
    (doseq [config configurations]
      (println "test configuration " config)
      (doseq [goal goal-points]
        (println "goal point: " goal)
        ;(let [start-orientation (first {:bathroom -0.89})]
        (doseq [start-orientation {:zero 0.0 :back Math/PI :right (/ Math/PI 2) :left (/ Math/PI -2)}]
          (println "start orientation: " start-orientation)
          (dotimes [rep repetitions]
            (println "trial: " rep)
            (start-robot :start-robot true :start-control-gui false :start-hps-gui false
                         :start-sim true
                         :robot-start-pose {:x 1.3 :y -2.2 :yaw (val start-orientation)} ;{:x -4.89 :y 1.89 :yaw (val start-orientation)}
                         :morse-script-file "pr2-apartment.py")
            (println "robot started")
            (define-goals-for-config (config @goal-configurations))
            (declare-complex-nav-dm)
            (println "config set")
            (let [[trace-header & trace :as whole-trace] (achieve :complex-nav-dm (assoc goal :timeout 30000))
            ;(achieve :simple-nav-dm (assoc goal :timeout 30000))
            ;(robot.GUI.hps.decision-process/read-trace-from-file "/work/kirsch/robot-log/trace.hps")
                  trace-statistics (robot.GUI.hps.decision-process/calculate-trace-statistics trace trace-header)
                  dm-statistics (robot.GUI.hps.decision-process/calculate-bb-statistics trace trace-header)
                  tracefile (format "/work/kirsch/robot-log/automated-tests/%s-%s-%s-%0,2d.hps"
                                    (name config) (:name goal) (name (key start-orientation)) rep)
                  statsfile (format "/work/kirsch/robot-log/automated-tests-statistics/%s.hpsstat"
                                    (name config))]
              ;(println "trace statistics: " statistics)
              (println "got trace, write to " tracefile " and " statsfile)
              (spit statsfile (str {:config config :goal goal :startorientation (key start-orientation) :repetition rep :trace-statistics trace-statistics :dm-statistics dm-statistics} "\n") :append true))
              ;(spit tracefile whole-trace)) ; !!! hier weitermachen: nicht unbedingt ganze traces speichern bzw. gleich auswerten; man könnte auch die sowieso gespeicherte Trace trace.hps kopieren
           (println "file written, stop robot and simulator")
           (robot.connection/stop-robot true)))))))


; Statistik-files wieder einlesen:
;(map read-string (clojure.string/split (slurp "/home/kirsch/clj-test.clj") #"\n"))


;; --------------------------------------------------------------------- ;;
;; -- Optimierung von Parametern --------------------------------------- ;;
;; --------------------------------------------------------------------- ;;

(defn read-and-remove-test-results [configs]
  (loop [[this-config & other-configs] configs
         data []]
    (if this-config
      (let [statsfile (format "/work/kirsch/robot-log/automated-tests-statistics/%s.hpsstat"
                        (name this-config))
            stats (map read-string (clojure.string/split (slurp statsfile) #"\n"))]
        (.exec (Runtime/getRuntime) (str "rm " statsfile))
        (recur other-configs
               (concat
                 data
                 (map (fn [{:keys [config goal trace-statistics]}]
                        {:config config ; this-config geht genauso, das hier war zum Testen praktischer
                         :goal (:name goal)
                         :time (:duration (:time trace-statistics))
                         :failure (if (= (:result (:result trace-statistics)) :failure) 1 0)
                         :collisions (:number-of-collisions (:collisions trace-statistics))
                         :backward (:sideward-backward (:movement trace-statistics))})
                         ;:deceleration (:deceleration-eta (:acceleration trace-statistics))}) ; die Zeile ist noch ungetestet: verwendet neues Maß aus Paper mit Frank und Meike
                      stats))))
      data)))

(defn get-test-results []
  (let [configs configurations-to-test
        all-results (read-and-remove-test-results configs)
        sort-and-aggregate (fn [objective goal-data] (map #(map :config %) (vals (group-by objective (sort-by objective < goal-data)))))
        aggregated-results (into {} (mapcat (fn [[goal goal-data]]
                                              {goal
                                               {:time-ordering (sort-and-aggregate :time goal-data)
                                                :failure (sort-and-aggregate :failure goal-data)
                                                :collision-ordering (sort-and-aggregate :collisions goal-data)
                                                :backwards-ordering (sort-and-aggregate :backward goal-data)
                                                :average-time (mut/average (map :time goal-data))}})
                                            (group-by :goal all-results)))
        time-deviation-per-config (into {} (mapcat (fn [[config config-data]]
                                                    {config
                                                      (mut/stdev (map (fn [{:keys [time goal]}]
                                                                        (let [goal-avg (:average-time (get aggregated-results goal))]
                                                                          (/ (- time goal-avg) goal-avg)))
                                                                      config-data))})
                                                  (group-by :config all-results)))]
    (conj (concat (map :time-ordering (vals aggregated-results))
                  (map :failure (vals aggregated-results))
                  (map :collision-ordering (vals aggregated-results)))
          (map #(map first %) (vals (group-by second (sort-by val < time-deviation-per-config)))))))
; !!! auch Erfolg reinrechnen?


(defn aggregate-votings [votings]
  (letfn [(get-borda-score [config voting]
          ; Borda rule: zählen wie viele Elemente hinter einem Kandidaten waren
            (loop [[preference & restvotes] voting]
              (if (some #(= % config) preference)
                (count restvotes)
                (recur restvotes))))]
    (let [configs-with-score (into {} (map (fn [config]
                                             {config (apply + (map #(get-borda-score config %) votings))})
                                           (flatten (first votings))))
          final-voting (sort-by val > configs-with-score)]
      (spit "/work/kirsch/robot-log/configurations/configs.clj" (str {:votings votings :final-voting final-voting} "\n\n") :append true)
      final-voting)))

(def available-producers [random-producer towards-goalpoint-producer turn-producer repeat-command-producer motion-primitives-producer])

(defn mutate-config [config]
  (println "mutate config")
  (let [new-evaluators (reduce (fn [other-configs expert-conf]
                                 (let [[expert weight] (if (instance? hps.types.AbstractExpert expert-conf)
                                                         [expert-conf (:weight expert-conf)]
                                                         [(first expert-conf) (second expert-conf)])]
                                   (when (= (:type expert) :evaluator)
                                     (conj other-configs
                                           [expert (+ weight (mut/randbetween -0.2 0.2))]))))
                               []
                               (:nav-complex config))
        config-with-new-evaluators  (concat
                                      (remove (fn [exp]
                                                (let [evl (if (coll? exp) (first exp) exp)]
                                                  (some #(= evl (first %)) new-evaluators)))
                                              (:nav-complex config))
                                      new-evaluators)]
    (assoc config :nav-complex config-with-new-evaluators)))

        ;swap-producer (nth available-producers (rand-int (count available-producers))) ; ein Producer wird ein- oder ausgeschaltet
        ;config-with-new-producers (if (some #(= (if (instance? hps.types.AbstractExpert %) % (first %)) swap-producer) config-with-new-evaluators)
                                    ;(remove #(= (if (instance? hps.types.AbstractExpert %) % (first %)) swap-producer) config-with-new-evaluators)
                                    ;(conj config-with-new-evaluators
                                          ;swap-producer))
        ;old-quantity (some #(if (= (first %) random-producer) (:quantity (second %))) (:nav-simple config))] ; es ist immer ein random-producer drin
    ;(println "old quanitity:" old-quantity)
    ;(assoc config
           ;:nav-simple
           ;(conj (remove #(= (first %) random-producer) config-with-new-producers)
                 ;[random-producer {:quantity (max 0 (+ old-quantity (int (mut/randbetween -5 5))))}]))))



(defn create-new-configs
  ([quality-ordering] (create-new-configs quality-ordering 1)) ;z.B. ([:c3 5] [:c1 4] [:c2 1])
  ([quality-ordering number-of-configs-to-copy]
   (println "create-new-configs")
   (let [config-ordering (map first quality-ordering) ; z.B. (:c3 :c1 :c2)
         old-configurations (into {} (map #(hash-map % (get @goal-configurations %)) config-ordering))
         best-config (get old-configurations (first config-ordering))]
     (println "create-new-configs immer noch fein")
     (swap! goal-configurations merge
            (into {}
              (map vector
                configurations-to-test
                (concat
                  (map #(get old-configurations %) (take number-of-configs-to-copy config-ordering))
                  (map #(mutate-config (get old-configurations %)) (take (- (count config-ordering) number-of-configs-to-copy) config-ordering)))))))))


(defn pprint-configs []
  (apply str
     (str (java.util.Date.))
     "\n"
          (map (fn [conf]
                (with-out-str (pprint
                  (cons (key conf)
                        (map #(if (instance? hps.types.AbstractExpert %) [(:name %) (:weight %)] [(:name (first %)) (second %)])
                             (:nav-complex (val conf)))))))
               (select-keys @goal-configurations configurations-to-test))))

(defn save-configs []
  (spit "/work/kirsch/robot-log/configurations/configs.clj"
    (str (pprint-configs) "\n")
    :append true))

(defn optimize-navigation-parameters
  ([] (optimize-navigation-parameters 30))
  ([max-iterations]
   (.exec (Runtime/getRuntime) "rm /work/kirsch/robot-log/configurations/configs.clj")
   (loop [iteration 0]
     (println "iteration: " iteration)
     (save-configs)
     (if (>= iteration max-iterations)
       (print (pprint-configs))
       (do
         (test-navigation)
         (let [votings (get-test-results)
               full-ordering (aggregate-votings votings)]
           (println "ordering: " full-ordering)
           (create-new-configs full-ordering)
           (recur (inc iteration))))))))



;; --------------------------------------------------------------------- ;;
;; -- Statistiken für LaTeX aufbereiten -------------------------------- ;;
;; --------------------------------------------------------------------- ;;

(defn read-test-results []
  (loop [[this-config & other-configs] configurations-to-test
         data []]
    (if this-config
      (let [statsfile (format "/work/kirsch/robot-log/automated-tests-statistics/%s.hpsstat"
                        (name this-config))
            stats (map read-string (clojure.string/split (slurp statsfile) #"\n"))]
        (recur other-configs
               (concat
                 data
                 (map (fn [{:keys [config goal startorientation trace-statistics]}]
                        {:config config ; this-config geht genauso, das hier war zum Testen praktischer
                         :goal (:name goal)
                         :start startorientation
                         :time               (:duration (:time trace-statistics))
                         :success            (:result (:result trace-statistics))
                         :dist-to-goal       (:dist-to-goal (:result trace-statistics))
                         :acceleration-trans (:average (:acceleration trace-statistics))
                         :acceleration-rot   (:average (:rot-acceleration trace-statistics))
                         :jerk-trans         (:average (:jerk trace-statistics))
                         :jerk-rot           (:average (:rot-jerk trace-statistics))
                         :collisions         (/ (:number-of-collisions (:collisions trace-statistics))
                                                (:number-of-states (:collisions trace-statistics))) ;sollte eigentlich nie 0 sein
                         :backward           (/ (:sideward-backward (:movement trace-statistics))
                                                (:number-of-states (:movement trace-statistics)))}) ; sollte auch nicht 0 sein
                      stats))))
      data)))

(defn calculate-relative-times-for-task [data]
  (let [times-per-config (into {}
                               (map (fn [[config single-instance]]
                                     {config
                                      (filter identity
                                              (map #(when (= %1 :success) %2)
                                                   (map :success single-instance)
                                                   (map :time single-instance)))})
                                    (group-by :config data)))
        overall-average (mut/average (apply concat (vals times-per-config)))
        times-per-config-scaled (into {}
                                      (map (fn [[config unscaled-times]]
                                             {config
                                              (map #(/ (- % overall-average) overall-average) unscaled-times)}) ; der Durchschnitt kann normalerweise nicht 0 sein
                                           times-per-config))]
    times-per-config-scaled))

(defn calculate-relative-accelerations [data parameter]
  (let [data-per-config (into {}
                               (map (fn [[config single-instance]]
                                     {config
                                      (map parameter single-instance)})
                                    (group-by :config data)))
        overall-average (mut/average (apply concat (vals data-per-config)))
        data-per-config-scaled (into {}
                                      (map (fn [[config unscaled-data]]
                                             {config
                                              (map #(/ (- % overall-average) overall-average) unscaled-data)}) ; der Durchschnitt kann normalerweise nicht 0 sein
                                           data-per-config))]
    data-per-config-scaled))

(defn calculate-absolute-accelerations [data parameter]
  (let [data-per-config (into {}
                               (map (fn [[config single-instance]]
                                     {config
                                      (map parameter single-instance)})
                                    (group-by :config data)))
        data-per-config-scaled (into {}
                                      (map (fn [[config unscaled-data]]
                                             {config
                                              unscaled-data})
                                           data-per-config))]
    data-per-config-scaled))

(defn calculate-failures-for-task [data]
  (into {}
        (map (fn [[config single-instance]]
               {config
                (list (/ (apply + (map #(if (= (:success %) :failure) 1.0 0.0) single-instance)) ; Anzahl Fehler
                         (count single-instance)))})  ; Gesamtzahl Läufe (sollte nie 0 sein)
             (group-by :config data))))

(defn calculate-rate [data parameter]
  (into {}
    (map (fn [[config single-instance]]
           {config
            (map #(* 1.0 (parameter %)) single-instance)})
         (group-by :config data))))


(defn regroup-data-per-config [data]
  (into {}
        (apply map (fn [& pairs]
                     [(ffirst pairs)
                      (mut/calculate-standard-statistics (mapcat second pairs))])
                   (map second data))))

(defn statistics-to-boxplot [[config {:keys [min max lower-quartile upper-quartile median]}]]
  (format "%% %s\n\\addplot+[boxplot prepared={lower whisker=%.4f,lower quartile=%.4f,median=%.4f,upper quartile=%.4f,upper whisker=%.4f}]coordinates {};\n"
    (name config)
    min lower-quartile median upper-quartile max))

(defn make-tex-figure [data caption]
  (format "\\begin{boxplotfigure}{%s}\n%s\\end{boxplotfigure}\n\n"
    caption
    (clojure.string/join (map statistics-to-boxplot data))))

(defn tests-to-csv []
  (let [data (read-test-results)]
    (loop [[[goal-spec goal-data] & rest-goal-data] (vec (group-by (juxt :goal :start) data))
           time-data []
           failure-data []
           collisions-data []
           backward-data []
           acc-trans-data []
           acc-rot-data []
           jerk-trans-data []
           jerk-rot-data []]
      (if goal-data
        (recur rest-goal-data
               (conj time-data       [goal-spec (calculate-relative-times-for-task goal-data)])
               (conj failure-data    [goal-spec (calculate-failures-for-task goal-data)])
               (conj collisions-data [goal-spec (calculate-rate goal-data :collisions)])
               (conj backward-data   [goal-spec (calculate-rate goal-data :backward)])
               (conj acc-trans-data  [goal-spec (calculate-relative-accelerations goal-data :acceleration-trans)])
               (conj acc-rot-data    [goal-spec (calculate-relative-accelerations goal-data :acceleration-rot)])
               (conj jerk-trans-data [goal-spec (calculate-absolute-accelerations goal-data :jerk-trans)])
               (conj jerk-rot-data   [goal-spec (calculate-absolute-accelerations goal-data :jerk-rot)]))
               ;(conj jerk-trans-data [goal-spec (calculate-relative-accelerations goal-data :jerk-trans)])
               ;(conj jerk-rot-data   [goal-spec (calculate-relative-accelerations goal-data :jerk-rot)]))
        ;time-data))))

        (do
          ;(println failure-data)
          (doseq [[[room ori] {:keys [simple classical hps-base hps-sub hps-sub-turn hps-sub-towards-goal hps-sub-towards-goal-20 hps-sub-turn-towards]}] (sort-by #(first (first %)) failure-data)]
            (println (apply format "%s & %s & %.1f & %.1f & %.1f & %.1f \\\\" room (name ori) (map first [hps-sub hps-sub-towards-goal hps-sub-towards-goal-20 hps-base]))))
            ;[simple classical hps-base hps-sub hps-sub-turn]))))
          (spit "/home/kirsch/arbeit/hcaidocs/src/conferences/17-IROS-Kirsch/evaluation-figures.tex"
                (clojure.string/join (map (fn [[processed-data caption]] (make-tex-figure (regroup-data-per-config processed-data) caption)) [[time-data "Navigation time of successful runs, relative to average time per task."] [failure-data "Failure rate (statistics over failures per task). 0 means a point was always reached, 1 means that a task could not be achieved in any trial"] [collisions-data "Collisions (in percent of observed states)."] [backward-data "Rate of moving side- or backward."] [acc-trans-data "Translational acceleration (relative to average per task)."] [acc-rot-data "Rotational acceleration (relative to average per task)."] [jerk-trans-data "Absolute values of translational jerk."] [jerk-rot-data "Absolute values of rotational jerk."]]))))))))


