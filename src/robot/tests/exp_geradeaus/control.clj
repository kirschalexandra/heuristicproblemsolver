; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.tests.exp-geradeaus.control
  (:require [robot.connection :refer :all]
            [robot.state :refer :all])
  (:import java.lang.Runtime))

                                        ; move-old: prüft, ob Roboter am Ziel ist bzw. stoppt ihn nach bestimmter Zeit -> Anzahl der geschickten Kommandos variiert leicht (zwischen 607 und 612)
(defn move-old [control-fun]
  (let [start-time (.getTime (java.util.Date.))
        command-future (future (loop [t 0]
                                 (send-command :motion (control-fun t)); @robot-pose))
                                 (Thread/sleep (/ 10000 600))
                                 (recur (inc t))))
        goal-reached (promise)]
    (add-watch robot-pose :goal-reached
               (fn [_ _ _ newval]
                 (when (> (:x newval) 5.0)
                   (deliver goal-reached true))))
    (deref goal-reached 10000 nil) ; er sollte immer exakt 10 s brauchen
    (println "moving time (s): " (/ (- (.getTime (java.util.Date.)) start-time) 1000.0))
    (remove-watch robot-pose :goal-reached)
    (future-cancel command-future))
  (send-command :motion))

; (use '[clojure.java.shell :only [sh]])

; move: einfachere Variante bei der Anzahl Kommandos konstant ist und damit die gewünschte Funktion besser umgesetzt wird

(def screenshot-tmp-dir "/work/kirsch/animationsprojekt/tmp/")
(def screenshot-timesteps (/ 600 9)); 60) ;40 60 75

(defn move [control-fun]
  ; (sh "gnome-screenshot")
  ; (.exec (Runtime/getRuntime) "recordmydesktop --width 1400 --height 673 -x 250 -y 190 --no-cursor -o desktop-recording.ogv")
  (Thread/sleep 2000) ; Zeit um Bildschirm für Aufnahme vorzubereiten
  (let [start-time (.getTime (java.util.Date.))]
    (loop [t 0]
      (when (<= t 600)
        (when (zero? (mod t screenshot-timesteps)); 40))
          (.exec (Runtime/getRuntime) (format "gnome-screenshot -f %s%0,3d.png" screenshot-tmp-dir t)))
        (send-command :motion (control-fun t)); @robot-pose))
        (Thread/sleep (/ 10000 600))
        (recur (inc t))))
    (send-command :motion)
    ; (.exec (Runtime/getRuntime) "pkill recordmydesktop")
    (/ (- (.getTime (java.util.Date.)) start-time) 1000.0))) ; Rückgabewert: Fahrtzeit


;;;;;; Nachbearbeitung Screenshots (funktioniert nur wenn höchstens ein Screenshot pro Sekunde gemacht wird):
;;;; cd /work/kirsch/animationsprojekt/tmp
;;;; for i in *.png; do convert -crop 1400x673+250+190 "${i}" ../60-constant/"${i}"; done
;;;; nicht mehr nötig, aber wenn man lieber mit Zeitschritten benennen will (geht nur bei weniger als einer Aufnahme pro Sekunde)
;;;; for f in *.png; do mv -n "$f" "$(date -r "$f" +"%Y%m%d_%H%M%S").png"; done
;;;
;;;; neue Koordinaten für neue Kamerapositionen:
;;;; cd /work/kirsch/animationsprojekt/tmp
;;;; for i in *.png; do convert -crop 1421x609+192+253 "${i}" ../final/vel-...-below/"${i}"; done
;;;
;;;; falls individuelles Zuschneiden gewollt:
;;;; head:
;;;; for i in *.png; do convert -crop 1372x588+217+292 "${i}" ../final/vel-...-head/"${i}"; done
;;;; above:
;;;; for i in *.png; do convert -crop 1248x534+280+337 "${i}" ../final/vel-...-above/"${i}"; done


; allerneueste Schneidekoordinaten für alle Varianten:
; for i in *.png; do convert -crop 1421x609+192+405 "${i}" ../final/vel-...-below/"${i}"; done


(defn move-constant []
  (move (fn [_] {:x 0.5 :y 0 :w 0})))

(defn move-linear-inc []
  (move (fn [t] {:x (/ t 600) :y 0 :w 0})))

(defn move-linear-dec []
  (move (fn [t] {:x (- 1 (/ t 600)) :y 0 :w 0})))

(defn move-trapezoidal-1 []
  (let [vc (/ 2 3)]
    (move (fn [t] {:x (cond (<= t 100) (* (/ t 100) vc)
                            (<= t 400) vc
                            :else (* (- 1 (/ (- t 400) 200)) vc))}))))

(defn move-trapezoidal-2 []
  (move (fn [t] {:x (if (<= t 300) (/ t 300) (- 1 (/ (- t 300) 300)))})))

(defn move-trapezoidal-3 []
  (move (fn [t] {:x (if (<= t 300) (- 1 (/ t 300)) (/ (- t 300) 300))})))

(defn move-sinusoidal
  ([] (move-sinusoidal 0.3 1 (/ Math/PI -2))) ; langsam - schnell - langsam
  ([a b c]
   (move (fn [t] (let [alpha (/ (* 2 Math/PI t) 600)]
                   {:x (+ 0.5 (* a (Math/sin (+ (* b alpha) c)))) :y 0 :w 0})))))


; in pr2-geradeaus:
; robot.rotate(x=0.0, y=0.0, z=math.pi/2)
(defn move-sideways-right []
  (move (fn [_] {:x 0 :y -0.5 :w 0})))

; in pr2-geradeaus:
; robot.rotate(x=0.0, y=0.0, z=-math.pi/2)
(defn move-sideways-left []
  (move (fn [_] {:x 0 :y 0.5 :w 0})))

(defn move-backwards []
  (move (fn [_] {:x -0.5 :y 0 :w 0})))

(defn move-sideways-angle [phi]
  (let [v 0.5]
    (move (fn [_] {:x (* v (Math/cos phi)) :y (* -1 v (Math/sin phi)) :w 0}))))

(defn move-turning [rotvel]
  (let [v 0.5]
    (move (fn [t] (let [phi (:yaw @robot-pose)]
                    {:x (* v (Math/cos phi)) :y (* -1 v (Math/sin phi)) :w rotvel})))))
