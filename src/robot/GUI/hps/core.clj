; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.hps.core
  (:gen-class
   :extends javafx.application.Application)
  (:require [clojure.java.io :as io]
            hps.GUI.main
            [hps.GUI.structure :as structure]
            [robot.GUI.hps.decision-process :as dp]
            robot.GUI.hps.entry
            [robot.GUI.hps.export :as export]
            [utilities.gui :as gut])
  (:import javafx.scene.control.Menu))

(defn adjust-menu-for-morph [menubar scene]
  (let [menufile (first (filter #(= (.getText %) "File") (.getMenus menubar)))
        menucreate (Menu. "Create Traces")
        menuexport (Menu. "Export")]

    (doseq [menu [menucreate menuexport]]
      (.add (.getMenus menubar) menu))

    (gut/create-menu-item menufile "Load Trace" {:key "O" :modifier :ctrl} (partial dp/load-trace scene false))
    (gut/create-menu-item menufile "Load Trace from ..." {:key "O" :modifier :alt} (partial dp/load-trace scene true))

    (gut/create-menu-item menucreate "Set Goal" {:key "G" :modifier :alt} (partial dp/set-goal scene))

    (gut/create-menu-item menuexport "Environment -> LaTeX" {:key "E" :modifier :alt} (partial export/gui-environment-to-latex scene))

    menubar))

(defn adjust-gui-for-morph [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/morph.css")))
  (adjust-menu-for-morph (.lookup scene "#hps-menubar-left") scene)
  (robot.GUI.hps.entry/make-entry-screen scene))


; add Navigation to HPS modes
(structure/register-hps-mode "Navigation" adjust-gui-for-morph false)

(defn start-robot.GUI []
  (structure/register-hps-mode "Navigation" adjust-gui-for-morph true)
  (hps.GUI.main/start-hps-gui))

