; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.export)


(defn draw-objects-latex [objects-list drawing-params]
  (clojure.string/join (map (fn [[x y w h]] (format "\\path[%s] (%.2f, %.2f) rectangle ++(%.2f, %.2f);\n" drawing-params (double x) (double y) (double w) (double h))) (vals objects-list))))

(defn embed-in-latex-context
  ([tikz-cmds] (embed-in-latex-context "" tikz-cmds))
  ([tikz-definitions tikz-cmds]
   (str "\\documentclass{article}\n"
        "\\usepackage{tikz}\n\n"
        "\\begin{document}\n\n"
        tikz-definitions
        "\\begin{tikzpicture}\n"
        tikz-cmds
        "\\end{tikzpicture}\n\n"
        "\\end{document}")))

(defn environment-to-latex
  ([environment-name] (environment-to-latex environment-name "/home/kirsch/morph.tex"))
  ([environment-name file]
   ;(println "environment-name: " environment-name)
   (let [{walls :environment furniture :furniture room :clipping-region} (draw/get-environment-definition environment-name)]
     (spit file
           (embed-in-latex-context
             (str "\\definecolor{obstaclesStrokeColor}{HTML}{888888}\n"
                  "\\colorlet{wallFillColor}{obstaclesStrokeColor}\n"
                  "\\definecolor{furnitureFillColor}{HTML}{bbbbbb}\n"
                  "\n")
             (str (if room
                    (let [[room-min-x room-min-y room-width room-height] (env/get-drawing-coordinates room)]
                      (format "\\clip (%.2f, %.2f) rectangle ++(%.2f, %.2f);\n"
                        (double room-min-x) (double room-min-y) (double room-width) (double room-height)))
                    "")
                  (draw-objects-latex walls "fill=wallFillColor")
                  "\n"
                  (draw-objects-latex furniture "fill=furnitureFillColor,draw=obstaclesStrokeColor,line width=0.02pt")
                  "\n"))))))


(defn gui-environment-to-latex [root-display]
  (let [file-chooser (FileChooser.)]
    (.setTitle file-chooser "Export environment to LaTeX")
    (let [file (.showOpenDialog file-chooser (.getWindow (.getScene root-display)))
          environment-name (.getValue (.lookup (.getScene root-display) "#environment-choices"))] ; ist etwas gefährlich, setzt voraus, dass eine Welt geladen ist!
      (environment-to-latex environment-name)
  (println "Exported environment to LaTeX file " (.getPath file)))))
