; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.hps.dm-specific
  (:require [robot.GUI.hps.decision-process :as dp]
            [robot.GUI.hps.drawing :as draw]
            [clojure.pprint :refer :all]))

(swap! dp/dm-colors assoc :complex-nav-dm ["29425b" "d4deea"]  ; (standard-)blau
       :plan-bdm       ["771c1c" "c8a4a4"]  ; rot
       :subgoals-bdm   ["006500" "c0dcc0"]  ; grün
       :nav-bdm        ["d7c338" "fbf2b6"]  ; gelb
       :simple-nav-dm  ["29425b" "d4deea"]) ; (standard-)blau

(mapv #(swap! dp/dm-hierarchy derive % :nav-dm) [:nav-bdm :complex-nav-dm :simple-nav-dm])
(mapv #(swap! dp/dm-hierarchy derive % :navgoal-dm) [:nav-dm :plan-bdm])


(defmethod dp/format-command :nav-dm [_ alternative-data]
  (let [command (second alternative-data)]
    (when command
      (cl-format nil "x: ~5,2f, y: ~5,2f, w: ~5,2f" (:x command) (:y command) (:w command)))))

(defmethod dp/draw-alternative :nav-dm [displayed-dm mode data state gc]
  (let [colors (get @dp/dm-colors displayed-dm)]
    (case mode
      :selected (draw/draw-selected-command gc data state colors)
      :considered (draw/draw-decision-alternatives gc data state colors))))


(defmethod dp/draw-alternative :plan-bdm [displayed-dm mode data _ gc]
  (let [environment (.getValue (.lookup (.getScene (.getCanvas gc)) "#environment-choices"))
        env-topopoints (:points (:topological-map (draw/get-environment-definition environment)))
        colors (get @dp/dm-colors displayed-dm)
        to-topomap (case displayed-dm
                     :plan-bdm (fn [pointslist]
                                 {:points (select-keys env-topopoints pointslist)
                                  :connections (map (fn [p1 p2] [p1 p2]) pointslist (rest pointslist))})
                     :subgoals-bdm (fn [point] {:points (select-keys env-topopoints [point])}))]
      (case mode
        :selected (draw/draw-topological-map gc (to-topomap data) colors 0.1 nil)
        :considered
        (do
          (when-not (nil? (:decision data))
            (draw/draw-topological-map gc (to-topomap (:value (:decision data))) (reverse colors) 0.05 nil))
          (doseq [alt (:retained-alternatives data)]
            (draw/draw-topological-map gc (to-topomap (:value alt)) (reverse colors) 0.02 nil))
          (doseq [alt (:rejected-alternatives data)]
            (draw/draw-topological-map gc (to-topomap (:value alt)) (reverse colors) 0.02 (into-array Double/TYPE [0.02 0.05])))))))


(defmethod dp/draw-alternative :subgoals-bdm [displayed-dm mode data _ gc]
  (let [colors (get @dp/dm-colors displayed-dm)]
    (case mode
      :selected (draw/draw-point gc data colors)
      :considered
      (do
        (when-not (nil? (:decision data))
          (draw/draw-point gc (:value (:decision data)) (reverse colors) 0.2))
        (doseq [alt (:retained-alternatives data)]
          (draw/draw-point gc (:value alt) (reverse colors) 0.1))
        (doseq [alt (:rejected-alternatives data)]
          (draw/draw-point gc (:value alt) (reverse colors) 0.07))))))


(defmethod dp/draw-goal :navgoal-dm [displayed-dm goal gc]
  (when (and goal (:x goal))
    (draw/draw-goal gc goal (get @dp/dm-colors displayed-dm))))

(defmethod dp/draw-goal :subgoals-bdm [displayed-dm goal gc]
  (let [environment (.getValue (.lookup (.getScene (.getCanvas gc)) "#environment-choices"))
        env-topopoints (:points (:topological-map (draw/get-environment-definition environment)))
        to-topomap (fn [pointslist]
                     {:points (select-keys env-topopoints pointslist)
                      :connections (map (fn [p1 p2] [p1 p2]) pointslist (rest pointslist))})]
    (draw/draw-topological-map gc (to-topomap (:path goal)) (get @dp/dm-colors displayed-dm) 0.01 (into-array Double/TYPE [0.02 0.05])))) ; Dashpattern vielleicht nochmal ändern
