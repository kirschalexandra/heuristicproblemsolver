; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.hps.decision-process
  (:require [hps.GUI.make-decision :as hpsgui]
            [utilities.gui :as gut]
            [robot.GUI.hps.drawing :as draw]
            [robot.utilities :as rut]
            [utilities.math :as mut]
            [robot.achieve :as logging]
            [robot.navigation :as nav]
            [robot.connection :as conn]
            [robot.environment :as env]
            [hps.decision-modules :as dms]
            [clojure.java.io :as io]
            [clojure.math.numeric-tower :as math]
            [hps.GUI.structure :as structure])
  (:use [clojure.pprint])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.scene.image Image ImageView)
           (javafx.geometry Orientation)
           (javafx.scene.paint Color)
           (javafx.beans.property ReadOnlyStringWrapper ReadOnlyObjectWrapper)
           (javafx.util Callback)
           (javafx.event Event EventHandler)
           (javafx.stage FileChooser FileChooser$ExtensionFilter))
  (:load "decision_process/trace" "decision_process/control" "decision_process/displays" "decision_process/set-goal"))

(defn clear-morph-screen [scene]
  (let [[decisionModules stateRegion] (vec (.getItems (.lookup scene "#hps-centerSplitPane")))
        controlRegion (.lookup scene "#hps-controlRegion")]
    (.clear (.getChildren controlRegion))
    (.clear (.getChildren decisionModules))
    (.clear (.getChildren stateRegion))))

(defn load-trace
  ([scene] (load-trace scene false))
  ([scene ask-for-file?]
   (let [file (if ask-for-file?
                (let [file-chooser (FileChooser.)
                      morphlogdir (io/file (System/getenv "HPS_ROBOT_LOG_DIR"))]
                  (.setTitle file-chooser "Choose HPS file")
                  (when morphlogdir
                    (.setInitialDirectory file-chooser (io/file morphlogdir "automated-tests/")))
                  (.showOpenDialog file-chooser (.getWindow scene)))
                (System/getenv "HPS_ROBOT_LOG_FILE"))]
     (when file
       (clear-morph-screen scene)
       (display-decision-process scene (read-trace-from-file file))))))


