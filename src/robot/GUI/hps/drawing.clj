; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.hps.drawing
  (:require [utilities.gui :as gut]
            [robot.utilities :as rut]
            [robot.state]
            [hps.state]
            [robot.environment :as env]
            [clojure.pprint :refer :all])
  (:import (javafx.scene.canvas Canvas GraphicsContext)
           (javafx.scene.paint Color)
           (javafx.scene.control Label)
           (javafx.scene.layout Pane)
           (javafx.event EventHandler)
           (javafx.scene.input MouseEvent))
  (:load "drawing/constants" "drawing/environment-data" "drawing/environment" "drawing/robot" "drawing/people"))


(defn get-coordinate-limits [wall-spec]
  (let [limits-per-wall (map (fn [[_ [x y w h]]] [x (+ x w) y (+ y h)]) wall-spec)
        x-vals (concat (map #(nth % 0) limits-per-wall) (map #(nth % 1) limits-per-wall))
        y-vals (concat (map #(nth % 2) limits-per-wall) (map #(nth % 3) limits-per-wall))]
    [(apply min x-vals)
     (apply max x-vals)
     (apply min y-vals)
     (apply max y-vals)]))

(defn define-canvas [environment humans draw-topomap?]
  (let [{walls :environment room :clipping-region} (get-environment-definition environment)
        [min-x max-x min-y max-y] (if room (env/get-global-coordinate-limits room) (get-coordinate-limits walls))
        [width height] [(- max-x min-x) (- max-y min-y)]
        max-width-or-height 500
        [canvas-width canvas-height] (if (> width height)
                                       [max-width-or-height (int (/ (* height max-width-or-height) width))]
                                       [(int (/ (* width max-width-or-height) height)) max-width-or-height])
        [scale-x scale-y] [(/ canvas-width width) (- (/ canvas-height height))]
        [translate-x translate-y] [(- min-x) (- max-y)]]
    (letfn [(create-layer [layer-name]
              (let [canvas (Canvas. canvas-width canvas-height)
                    gc (.getGraphicsContext2D canvas)]
                (.setId canvas layer-name)
                (doto gc
                  ; set coordinate system
                  (.scale scale-x scale-y)
                  (.translate translate-x translate-y))
                canvas))]
      (let [canvas-pane (Pane.)
            position-label (Label.)
            background-canvas (create-layer "background canvas")]

        (.setOnMouseMoved canvas-pane
                          (proxy [EventHandler] []
                            (handle [^MouseEvent event]
                              (.setText position-label
                                        (cl-format nil "Cursor position: x: ~5,2f, y: ~5,2f"
                                          (+ (/ (.getX event) scale-x) min-x)
                                          (+ (/ (.getY event) scale-y) max-y))))))

        (defn clear-graphics-context [gc]
          (.clearRect gc min-x min-y width height))

        (defn fill-with-background-color [gc]
          (doto gc
            (.setFill Color/WHITE)
            (.fillRect min-x min-y width height)))

        (draw-environment background-canvas environment draw-topomap?)

        (apply gut/add-to-container canvas-pane
          background-canvas (create-layer "goal canvas") (create-layer "alternatives canvas") (create-layer "selected alternatives canvas") (create-layer "state canvas")
          (map #(create-layer (format "%s canvas" %)) humans))
        [canvas-pane position-label]))))


