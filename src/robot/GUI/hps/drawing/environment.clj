; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.drawing)

(defn get-environment-definition [environment-string]
  ((keyword environment-string) (:environments (meta (find-ns 'robot.GUI.hps.drawing)))))


;; environment and furniture
(defn draw-objects [gc objects-list]
  (doseq [[_ [x y w h]] objects-list]
    (.rect gc x y w h)))

(defn draw-walls [gc walls]
  (doto gc
    (.beginPath)
    (draw-objects walls)
    (.closePath)
    (.setFill obstacles-stroke-color)
    ; (.setStroke Color/BLACK)
    ; (.setLineWidth 0.02)
    ; (.stroke)))
    (.fill)))

(defn draw-furniture [gc furniture]
  (doto gc
    (.beginPath)
    (draw-objects furniture)
    (.closePath)
    (.setFill (Color/web "bbbbbb"))
    (.setStroke obstacles-stroke-color)
    (.setLineWidth 0.02)
    (.fill)
    (.stroke)))

(defn draw-point
  ([gc point color] (draw-point gc point color 0.1))
  ([gc point [fg-color _] radius]
   (doto gc
      (.setFill (Color/web fg-color))
      (.beginPath)
      (.arc (:x point) (:y point) radius radius 0 360)
      (.closePath)
      (.fill))))

(defn draw-topological-map
  ([gc topomap] (draw-topological-map gc topomap ["555555" "555555"] 0.03 nil))
  ([gc {points :points connections :connections} [fg-color _ :as color] linewidth dashes]
   (doseq [[_ pp] points]
     (draw-point gc pp color))
   (doto gc
     (.setStroke (Color/web fg-color))
     (.setLineWidth linewidth))
   (when dashes
     (.setLineDashes gc dashes))
   (doseq [[p1-name p2-name] connections]
     (let [p1 (p1-name points)
           p2 (p2-name points)]
       (.strokeLine gc (:x p1) (:y p1) (:x p2) (:y p2))))))


(defn draw-environment [background-canvas environment draw-topomap?]
  (let [{walls :environment furniture :furniture topomap :topological-map room :clipping-region} (get-environment-definition environment)
        gc (.getGraphicsContext2D background-canvas)]
    (when room
      (let [[room-min-x room-min-y room-width room-height] (env/get-drawing-coordinates room)]
        (doto gc
          (.beginPath)
          (.rect room-min-x room-min-y room-width room-height)
          (.closePath)
          (.clip))))
    (clear-graphics-context gc)
    (fill-with-background-color gc)
    (draw-walls gc walls)
    (draw-furniture gc furniture)
    (when (and draw-topomap? topomap)
      (draw-topological-map gc topomap))))


; (defn highlight-topological-map-point [gc point [fg-color bg-color]]
;   (doto gc
;     ; äußerer Kreis
;     (.setFill (Color/web fg-color))
;     (.beginPath)
;     (.arc (:x pp) (:y pp) 0.15 0.15 0 360)
;     (.closePath)
;     (.fill)))
;     ; innerer Kreis
;     (.setFill (Color/web bg-color))
;     (.beginPath)
;     (.arc (:x pp) (:y pp) 0.1 0.1 0 360)
;     (.closePath)
;     (.fill)))
