; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.drawing)

(defn register-environment [name env furniture & {:keys [topomap clipping-region]}]
  (alter-meta!
    (find-ns 'robot.GUI.hps.drawing)
    #(assoc % :environments (assoc (:environments %) name {:environment env :furniture furniture :topological-map topomap :clipping-region clipping-region}))))

;; Historische Notiz:
;; Am Anfang gab es zwei Umgebungsspezifikationen: eine für den Roboter und eine für die GUI
;; Jetzt sind alle Daten in robot.environment
;; Bei Zusammenlegung habe ich nur hier geändert und alle Verwendungen der Infos in drawing beibehalten. Deswegen die etwas seltsame Umstrukturierung der Daten hier.
;; Räume können als Teile von größeren Umgebungen definiert werden (Ausschnitt wird durch :clipping-region definiert)

(defn get-walls-for-environment [environment]
  (into {} (map #(vector (:label %) (env/get-drawing-coordinates %)))
                (concat (:outer-walls environment) (:inner-walls environment))))

(defn get-furniture-for-room [environment room]
  (into {} (map (fn [[k v]] [(name k) (env/get-drawing-coordinates v)])
                (room (:furniture environment)))))

; Indoors
(register-environment :indoors
  (get-walls-for-environment env/boxes)
  (get-furniture-for-room env/boxes :platform )
  :topomap {:points env/boxes-topomap-points :connections env/boxes-topomap-connections})
; Apartment and single rooms
(register-environment :apartment
  (get-walls-for-environment env/apartment-without-chair)
  (mapcat #(get-furniture-for-room env/apartment-without-chair %) [:kitchen :bedroom :livingroom])
  :topomap {:points env/apartment-topomap-points :connections env/apartment-topomap-connections})

(let [kitchen-topomap-points (conj (env/topomap-points (:kitchen (:furniture env/apartment-without-chair)))
                                   [:door (nth (first (:room-connections env/apartment-without-chair)) 2)])]
  (register-environment :kitchen
     (get-walls-for-environment env/apartment-without-chair)
     (get-furniture-for-room env/apartment-without-chair :kitchen)
     :clipping-region (:kitchen (:rooms env/apartment-without-chair))
     :topomap {:points kitchen-topomap-points
               :connections (env/generate-topomap-connections kitchen-topomap-points
                              (concat (vals env/kitchen-furniture) (:inner-walls env/apartment-without-chair)))}))
                               ; die Wände könnte man noch nach Küchenwänden ausfiltern, aber lohnt sich nicht für die einmalige Berechnung

(register-environment :livingroom
  (get-walls-for-environment env/apartment-without-chair)
  (get-furniture-for-room env/apartment-without-chair :livingroom)
  :clipping-region (:livingroom (:rooms env/apartment-without-chair)))
(register-environment :bedroom
  (get-walls-for-environment env/apartment-without-chair)
  (get-furniture-for-room env/apartment-without-chair :bedroom)
  :clipping-region (:bedroom (:rooms env/apartment-without-chair)))
(register-environment :corridor
  (get-walls-for-environment env/apartment-without-chair)
  nil
  :clipping-region (:corridor (:rooms env/apartment-without-chair)))
(register-environment :bathroom
  (get-walls-for-environment env/apartment-without-chair)
  nil
  :clipping-region (:bathroom (:rooms env/apartment-without-chair)))
(register-environment :lph
  (get-walls-for-environment env/lph)
  (mapcat #(get-furniture-for-room env/lph %) [:kitchen :bedroom :livingroom :bathroom]))


