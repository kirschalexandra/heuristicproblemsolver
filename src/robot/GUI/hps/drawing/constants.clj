; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.drawing)

(declare clear-graphics-context)
(declare fill-with-background-color)

(def obstacles-stroke-color (Color/web "888888"))
(def hps-darkblue (Color/web "29425b"))
(def hps-lightblue (Color/web "d4deea"))
(def robot-stroke-color (Color/web "000000")); hps-darkblue)
(def hps-red (Color/web "771c1c"))
(def hps-yellow (Color/web "d7c338"))

