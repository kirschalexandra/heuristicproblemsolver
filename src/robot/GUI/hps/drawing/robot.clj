; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.drawing)


;; robot and commands
(defmacro draw-robot [gc robot-pose & styling-commands]
  (let [[robotHalfWidth robotHalfDepth] [0.355 0.33]
        robotEdgeRadius 0.1
        [topcorners-y bottomcorners-y] [(- robotHalfWidth robotEdgeRadius) (- robotEdgeRadius robotHalfWidth)]
        [rightcorners-x leftcorners-x] [(- robotHalfDepth robotEdgeRadius) (- robotEdgeRadius robotHalfDepth)]]
    `(doto ~gc
      (.save)
      (.beginPath)
      ; position robot
      (.translate (:x ~robot-pose) (:y ~robot-pose))
      (.rotate (rut/radian-to-degree (:yaw ~robot-pose)))
      ; robot orientation
      (.moveTo ~(- robotHalfDepth 0.2) 0)
      (.lineTo ~robotHalfDepth 0)
      ; robot outline
      (.lineTo ~robotHalfDepth ~bottomcorners-y)
      (.arcTo  ~robotHalfDepth ~(- robotHalfWidth)
               ~rightcorners-x ~(- robotHalfWidth)
               ~robotEdgeRadius)
      (.lineTo ~leftcorners-x ~(- robotHalfWidth))
      (.arcTo  ~(- robotHalfDepth) ~(- robotHalfWidth)
               ~(- robotHalfDepth) ~bottomcorners-y
               ~robotEdgeRadius)
      (.lineTo ~(- robotHalfDepth) ~topcorners-y)
      (.arcTo  ~(- robotHalfDepth) ~robotHalfWidth
               ~leftcorners-x ~robotHalfWidth
               ~robotEdgeRadius)
      (.lineTo ~rightcorners-x ~robotHalfWidth)
      (.arcTo  ~robotHalfDepth ~robotHalfWidth
               ~robotHalfDepth ~topcorners-y
               ~robotEdgeRadius)
      (.lineTo ~robotHalfDepth 0)
      ; close and style
      (.closePath)
      ~@styling-commands
      (.restore))))


(defn draw-robot-state [gc robot-pose]
  (draw-robot gc robot-pose
    (.setLineWidth 0.05)
    (.setStroke robot-stroke-color)
    (.setFill (Color/web "aaaaaa"))
    (.fill)
    (.stroke)))

(defn draw-goal [gc robot-pose [stroke-color fill-color]]
  (draw-robot gc robot-pose
    (.setLineWidth 0.05)
    (.setFill (Color/web fill-color))
    (.fill)
    (.setStroke (Color/web "ffffff"))
    (.stroke)))

(let [prediction-horizon 1000]

  (defn draw-decision-alternatives [gc decision-map state [stroke-color fill-color]]
    (when-not (nil? state)
      ; (when-not (nil? (:decision decision-map))
      (when (and (:decision decision-map) (:value (:decision decision-map)))
        (draw-robot gc (hps.state/predict robot.state/robot-pose :command (:value (:decision decision-map)) :initial-state state :delta-time prediction-horizon)
          (.setLineWidth 0.02)
          (.setStroke (Color/web stroke-color))
          (.setFill (Color/web fill-color)) ;(Color/web "d4deea" 0.5))
          (.fill)
          (.stroke)))
      (doseq [alt (:retained-alternatives decision-map)]
        (when (and alt (:value alt))
          (draw-robot gc (hps.state/predict robot.state/robot-pose :command (:value alt) :initial-state state :delta-time prediction-horizon)
            (.setLineWidth 0.02)
            (.setStroke (Color/web stroke-color))
            (.stroke))))
      (doseq [alt (:rejected-alternatives decision-map)]
        (when (and alt (:value alt))
          (draw-robot gc (hps.state/predict robot.state/robot-pose :command (:value alt) :initial-state state :delta-time prediction-horizon)
            (.setLineWidth 0.02)
            (.setLineDashes (into-array Double/TYPE [0.02 0.05]))
            (.setStroke (Color/web stroke-color))
            (.stroke))))))

  (defn draw-chosen-command [gc cmd state]
    (when-not (or (empty? cmd) (empty? (second cmd)) (empty? state))
      (draw-robot gc (hps.state/predict robot.state/robot-pose :command cmd :initial-state state :delta-time prediction-horizon)
        (.setLineWidth 0.05)
        (.setFill robot-stroke-color)
        (.setStroke hps-lightblue)
        (.fill)
        (.stroke))))

  (defn draw-selected-command [gc cmd state [stroke-color _]]
    (when-not (or (empty? cmd) (empty? (second cmd)) (empty? state))
      (draw-robot gc (hps.state/predict robot.state/robot-pose :command cmd :initial-state state :delta-time prediction-horizon)
        (.setLineWidth 0.05)
        (.setStroke (Color/web stroke-color))
        (.stroke))))

)