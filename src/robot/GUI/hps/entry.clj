; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.hps.entry
  (:require [clojure.java.io :as io]
            [robot.GUI.hps.decision-process :as dp]
            [utilities.gui :as gut])
  (:import java.lang.Double
           javafx.scene.control.Button
           [javafx.scene.image Image ImageView]
           javafx.scene.layout.VBox))

(defn make-entry-screen [scene]
  (let [[_ stateRegion] (vec (.getItems (.lookup scene "#hps-centerSplitPane")))
        elements-box (VBox.)
        button-goal (Button. "New Goal")
        button-load (Button. "Load Trace")]

    (gut/set-button-action button-goal (dp/set-goal scene))
    (gut/set-button-action button-load (dp/load-trace scene))

    (doseq [btn [button-goal button-load]]
      (.setMaxWidth btn Double/MAX_VALUE) ; diese Einstellung habe ich nicht per CSS hinbekommen
      (gut/add-style btn "welcome-button"))

    (gut/add-to-container elements-box (ImageView. (Image. (io/input-stream (io/resource "images/startimage.png")))) button-goal button-load)
    (gut/add-style elements-box "entry-screen-box")

    (gut/add-to-container stateRegion elements-box)

    ))

