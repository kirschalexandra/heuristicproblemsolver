; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.decision-process)


(declare dm-colors)
(declare update-dm-slider-value)
(declare dig-out-canvasses)


;;; --------------------------------------------------------------------- ;;;
;;; Hilfsfunktionen für trace control allgemein und dm-spezifisch         ;;;
;;; --------------------------------------------------------------------- ;;;

(defn get-trace-event-from-slider-index [trace index]
  (nth trace (math/round index)))

(defn lookup-event-in-trace [trace event]
  (first (keep-indexed #(when (= %2 event) %1) trace)))

(defn get-slider-index-from-trace-event [trace [event-timestamp & _ :as event]]
  (if-let [found-index (lookup-event-in-trace trace event)]
    found-index
    (lookup-event-in-trace
      trace
      (loop [[[this-timestamp & _ :as this-trace-event] & other-trace-events] trace]
        (when trace
          (if (> this-timestamp event-timestamp)
            this-trace-event
            (if other-trace-events
              (recur other-trace-events)
              this-trace-event)))))))

(defn make-control-button [img]
  (let [btn (Button. "" (ImageView. (Image. (io/input-stream (io/resource (str "icons/" img ".gif"))))))]
    (gut/add-style btn "control-button")
    btn))

;;; --------------------------------------------------------------------- ;;;
;;; Kontrollelemente                                                      ;;;
;;; --------------------------------------------------------------------- ;;;

(let [play-image (ImageView. (Image. (io/input-stream (io/resource "icons/play.gif"))))
      stop-image (ImageView. (Image. (io/input-stream (io/resource "icons/stop.png"))))
      playing-process (atom nil)]

  (defn play-button-action [step-slider play-button state-indices]
    (if (nil? @playing-process)
      (let [play-done (promise)
            framerate-option (loop [[curelem & restelems] (vec (.getChildren (.lookup (.getScene play-button) "#optionstable")))]
                               (cond (and curelem (= (str (type curelem)) "class javafx.scene.control.Label") (= (.getText curelem) "Playback framerate"))
                                     (#(if (= % "") 24 (read-string %)) (.getText (first restelems)))
                                     curelem
                                     (recur restelems)
                                     :else 24))             ; als Notfalloption, damit er überhaupt was tut
            sleeptime (max 4 (/ 1000 framerate-option))]    ; bei kürzeren Abständen kommt er nicht hinterher und alles stockt
        (.setGraphic play-button stop-image)
        (reset! playing-process
                (future
                  (doseq [step state-indices]
                    (gut/run-in-fx-thread (.setValue step-slider step))
                    (Thread/sleep sleeptime))
                  (deliver play-done true)))
        (future
          (deref play-done)
          (reset! playing-process nil)
          (gut/run-in-fx-thread (.setGraphic play-button play-image))))
      (do
        (future-cancel @playing-process)
        (reset! playing-process nil)
        (gut/run-in-fx-thread (.setGraphic play-button play-image))))))


(defn make-button-box [& buttons]
  (let [button-box (TilePane. Orientation/HORIZONTAL)]
    (.add (.getStyleClass button-box) "morph-control-button-box")
    (apply gut/add-to-container button-box buttons)
    button-box))

(defn make-trace-control-elements [trace display-trace-state-fun]
  (let [max-trace-count (dec (count trace))
        step-slider (Slider. 0 max-trace-count 0)
        step-button (make-control-button "next")
        prev-button (make-control-button "previous")
        reset-button (make-control-button "reset")
        play-button (make-control-button "play")
        timestep-field (Label.)
        control-elements-box (VBox.)
        [state-indices _] (reduce (fn [[indices cur-index] [_ event-type _ _]]
                                    [(if (= event-type :state) (conj indices cur-index) indices) (inc cur-index)])
                                  [[] 0] trace)]

    (.setId step-slider "timeslider")
    (.add (.getStyleClass control-elements-box) "morph-control-elements-box")

    (gut/add-to-container control-elements-box
                          timestep-field
                          step-slider
                          (make-button-box reset-button prev-button step-button play-button))

    (gut/set-button-action step-button (.setValue step-slider (inc (.getValue step-slider))))
    (gut/set-button-action prev-button (.setValue step-slider (dec (.getValue step-slider))))
    (gut/set-button-action reset-button (.setValue step-slider 0))
    (gut/set-button-action play-button (play-button-action step-slider play-button state-indices))

    (.addListener (.valueProperty step-slider)
                  (proxy [ChangeListener] []
                    (^void changed [^ObservableValue ov ^Number oldval ^Number newval]
                      (let [trace-index (math/round newval)
                            trace-event (nth trace trace-index)
                            scene (.getScene control-elements-box)
                            active-tab-name (.getText (.getSelectedItem (.getSelectionModel (.lookup scene "#dm-tabpane"))))]
                        ;Darstellung
                        (.setText timestep-field
                                  (str (.format (java.text.SimpleDateFormat. "dd/MM/yyyy HH:mm:ss:SSS") (java.util.Date. (first trace-event)))
                                       " -- "
                                       (first trace-event)))
                        ; Anzeige von hervorgehobenen Zuständen löschen
                        (let [[_ _ _ selected-alternatives-canvas _ & _] (dig-out-canvasses (.lookup scene "#hps-stateRegion"))]
                          (gut/clear-canvas selected-alternatives-canvas))
                        ; neuen Zustand anzeigen
                        (display-trace-state-fun trace-index)
                        ; Slider für aktuellen Tab passend zum Haupt-Slider setzen
                        (update-dm-slider-value (keyword active-tab-name) trace-event)

                        ))))

    control-elements-box))

(defn make-dm-control-elements [dm-subtrace dm-name trace]
  (let [control-elements-box (VBox. 15.0)
        max-trace-count (dec (count dm-subtrace))
        step-slider (Slider. 0 max-trace-count 0)
        step-button (make-control-button "next")
        prev-button (make-control-button "previous")]

    ; Farben für Control Buttons anpassen
    (mapv #(.setStyle % (str "-fx-background-color: #" (first (get @dm-colors dm-name)) ";")) [step-button prev-button])

    (.add (.getStyleClass control-elements-box) "morph-control-elements-box")
    (.add (.getStyleClass step-slider) "dm-slider")

    (gut/add-to-container control-elements-box
                          step-slider
                          (make-button-box prev-button step-button))

    (gut/set-button-action step-button (.setValue step-slider (inc (.getValue step-slider))))
    (gut/set-button-action prev-button (.setValue step-slider (dec (.getValue step-slider))))

    (.setId step-slider (str "dm-timeslider-" (name dm-name)))

    (.addListener (.valueProperty step-slider)
                  (proxy [ChangeListener] []
                    (^void changed [^ObservableValue ov ^Number oldval ^Number newval]
                      (.setValue (.lookup (.getScene control-elements-box) "#timeslider")
                                 (get-slider-index-from-trace-event trace (get-trace-event-from-slider-index dm-subtrace newval))))))

    [control-elements-box
     (fn [event] (.setValue step-slider (get-slider-index-from-trace-event dm-subtrace event)))]
    ))


;;; --------------------------------------------------------------------- ;;;
;;; Darstellung von Statistiken                                           ;;;
;;; --------------------------------------------------------------------- ;;;

(defn fill-statistics-into-grid [statistics]
  (let [stats-table (GridPane.)]
    (.add (.getStyleClass stats-table) "morph-statistics-table")
    (doseq [row-number (range (count statistics))]
      (let [row-content (nth statistics row-number)]
        (.add stats-table (Label. (str (:label row-content) ":")) 0 row-number)
        (.add stats-table (Label. (str (:value row-content))) 1 row-number)))
    stats-table))

(defn display-trace-statistics [trace trace-header]
  (let [{stat-time :time stat-result :result stat-vel :velocity stat-rot-vel :rot-velocity stat-acc :acceleration
         stat-coll :collisions stat-move :movement}
        (calculate-trace-statistics trace trace-header)]
    (fill-statistics-into-grid
      [{:label "Result" :value (gut/format-keyword (:result stat-result))}
       {:label "Duration" :value (format "%.2f s" (:duration stat-time))}
       {:label "Velocity " :value (format "%.2f m/s (%.2f / %.2f)" (:average stat-vel) (:min stat-vel) (:max stat-vel))}
       {:label "Rot. Velocity " :value (format "%.2f m/s (%.2f / %.2f)" (:average stat-rot-vel) (:min stat-rot-vel) (:max stat-rot-vel))}
       ;{:label "Collisions" :value (format "%d / %d" (:number-of-collisions stat-coll) (:number-of-states stat-coll))}

       ;{:label "Decrease Gamma" :value (format "%.2f" (* 1.0 (:deceleration-gamma stat-acc)))}
       {:label "Decrease (Eta)" :value (format "%.2f" (* 1.0 (:deceleration-eta stat-acc)))}

       {:label "Side-/Backward" :value (format "%d / %d" (:sideward-backward stat-move) (:number-of-states stat-move))}]
      ;{:label "Acceleration " :value (format "%.2f m/s^2 (%.2f / %.2f)" (:average stat-acc) (:min stat-acc) (:max stat-acc))}]]
      )))

(defn display-bb-statistics [trace dm-name]
  (let [stats-table (GridPane.)
        {stat-decisions :decisions stat-alternatives :alternatives} (calculate-statistics-for-bb trace dm-name)]
    (fill-statistics-into-grid
      [{:label "Decisions " :value (format "%d / %d" (:executed stat-decisions) (:internal stat-decisions))}
       {:label "Rejected (%)"
        :value (let [stat (:rejected-percent stat-alternatives)]
                 (if (not-empty stat)
                   (format "%.1f (%.1f / %.1f)" (* 100.0 (:average stat)) (* 100.0 (:min stat)) (* 100.0 (:max stat)))
                   ""))}
       {:label "Diff. Decision-Next"
        :value (let [stat (:diff-decision-next stat-alternatives)]
                 (if (not-empty stat)
                   (format "%.2f (%.2f / %.2f)" (* 1.0 (:average stat)) (* 1.0 (:min stat)) (* 1.0 (:max stat)))
                   ""))}
       {:label "Evaluations Dev."
        :value (let [stat (:evaluations-stdev stat-alternatives)]
                 (if (not-empty stat)
                   (format "%.2f (%.2f / %.2f)" (* 1.0 (:average stat)) (* 1.0 (:min stat)) (* 1.0 (:max stat)))
                   ""))}
       {:label "Evaluations Range"
        :value (let [stat (:evaluations-range stat-alternatives)]
                 (if (not-empty stat)
                   (format "%.2f (%.2f / %.2f)" (* 1.0 (:average stat)) (* 1.0 (:min stat)) (* 1.0 (:max stat)))
                   ""))}])))


;;; --------------------------------------------------------------------- ;;;
;;; Optionen                                                              ;;;
;;; --------------------------------------------------------------------- ;;;

(defn configure-topomap-checkbox [show-topomap-checkbox trace-control-box environment-choicebox]
  (.addListener (.selectedProperty show-topomap-checkbox)
                (proxy [ChangeListener] []
                  (changed [^ObservableValue ov oldval newval]
                    (let [canvas-box (.lookup (.getScene trace-control-box) "#hps-stateRegion")
                          [background-canvas & _] (dig-out-canvasses canvas-box)]
                      (draw/draw-environment background-canvas (.getValue environment-choicebox) newval))))))


(defn make-environment-choices [{humans :humans env :environment} show-topomap-checkbox]
  (let [environment-options (keys (:environments (meta (find-ns 'robot.GUI.hps.drawing))))
        ordered-environment-options (map name
                                         (if env
                                           (cons env (remove #(= % env) environment-options))
                                           environment-options))
        environment-choices (ChoiceBox.)]

    (.addAll (.getItems environment-choices) (into-array String ordered-environment-options))

    (.setId environment-choices "environment-choices")

    (.addListener (.selectedItemProperty (.getSelectionModel environment-choices))
                  (proxy [ChangeListener] []
                    (changed [^ObservableValue ov oldval newval]
                      ; hier anderes Verhalten als bei topomap-checkbox: hier wird Canvas überhaupt erst definiert, Verlagerung dieser Funktionalität an andere Stelle hätte größere Umbauten erfordert
                      (let [state-box (.lookup (.getScene show-topomap-checkbox) "#hps-stateRegion")
                            canvas-box (VBox. 10.0)
                            slider (.lookup (.getScene show-topomap-checkbox) "#timeslider")]
                        (.setId canvas-box "morph-canvas-box")
                        (gut/clear-container state-box)
                        (gut/add-to-container state-box canvas-box)
                        (apply gut/add-to-container canvas-box (draw/define-canvas (keyword newval) humans (.isSelected show-topomap-checkbox)))
                        (.setValue slider 0)))))

    environment-choices))


(defn display-options [trace-control-box traceinfo]
  (let [options-table (GridPane.)
        show-topomap-checkbox (CheckBox.)
        environment-choices (make-environment-choices traceinfo show-topomap-checkbox)
        framerate-textfield (TextField. "6")]

    (configure-topomap-checkbox show-topomap-checkbox trace-control-box environment-choices)

    (gut/add-style options-table ".morph-info-box")

    (doseq [[row label option] [[0 "Environment:" environment-choices]
                                [1 "Show topological map?" show-topomap-checkbox]
                                [2 "Playback framerate" framerate-textfield]]]
      (.add options-table (Label. label) 0 row)
      (.add options-table option 1 row))

    (.setId options-table "optionstable")
    options-table))


(defn make-info-box
  ([title content] (make-info-box title content "-darkblue"))
  ([title content foreground-color]
   (let [info-box (VBox.)
         label (Label. title)]
     (.add (.getStyleClass info-box) "morph-info-box")
     (gut/add-to-container info-box label content)
     info-box)))


;;; --------------------------------------------------------------------- ;;;
;;; Controllboxen zusammenbauen                                           ;;;
;;; --------------------------------------------------------------------- ;;;

(defn make-trace-info-and-control-box [trace trace-control-box trace-header]
  (let [info-and-control-box (HBox.)
        stats-box (make-info-box "Trace Statistics" (display-trace-statistics trace trace-header))
        options-box (make-info-box "Options" (display-options trace-control-box trace-header))]
    (gut/add-style info-and-control-box "morph-info-and-control-box")
    (gut/add-to-container info-and-control-box
                          stats-box
                          trace-control-box
                          options-box)
    info-and-control-box))

(defn make-dm-control-box [dm-subtrace dm-name trace]
  (let [[control-elements-box slider-adapt-fun] (make-dm-control-elements dm-subtrace dm-name trace)
        outer-control-elements-box (HBox.)
        stats-box (make-info-box "DM Statistics" (display-bb-statistics trace dm-name) (str "#" (first (get @dm-colors dm-name))))]
    (gut/add-style outer-control-elements-box "morph-info-and-control-box")
    (gut/add-to-container outer-control-elements-box
                          stats-box
                          control-elements-box)
    [outer-control-elements-box slider-adapt-fun]))

