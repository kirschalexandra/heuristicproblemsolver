; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.decision-process)

;;; --------------------------------------------------------------------- ;;;
;;; Weltzustand anzeigen                                                  ;;;
;;; --------------------------------------------------------------------- ;;;


(defn dig-out-canvasses [display]
  (vec (.getChildren (first (.getChildren (first (.getChildren display)))))))


; die vielen einzelnen Canvases sind eigentlich nicht mehr nötig, nachdem jetzt states statt events angezeigt werden, aber so könnte man zumindest entscheiden, welche Dinge man gerade sehen will
(defn draw-world-state [{[_ _ _ robot-data :as robot-state] :robot human-states :humans} display]
  (let [[_ _ _ _ state-canvas & human-canvases] (dig-out-canvasses display)]
    (when robot-state
      (let [gc (.getGraphicsContext2D state-canvas)]
        (draw/clear-graphics-context gc)
        (draw/draw-robot-state gc robot-data)))
    (doseq [[_ _ source data] human-states]
      (let [gc (.getGraphicsContext2D (first (filter #(= (.getId %) (format "%s canvas" source)) human-canvases)))]
        (draw/clear-graphics-context gc)
        (draw/draw-human-state gc data)))))


;;; --------------------------------------------------------------------- ;;;
;;; Zustände der DMs anzeigen, individualiert duch multimethods           ;;;
;;; --------------------------------------------------------------------- ;;;

(def dm-hierarchy (atom (make-hierarchy)))
(def dm-colors (atom {nil ["29425b" "d4deea"]}))            ; (standard-)blau

(defmulti format-goals (fn [dm-name & _] dm-name) :hierarchy dm-hierarchy)

(defmethod format-goals :default [_ goals]
  (cl-format nil "current goals: ~{~a~^, ~}"
             (map #(cl-format nil "{~{~a~^, ~}}"
                              (map (fn [[key val]] (str key " " (if (number? val) (cl-format nil "~,2f" val) val))) %))
                  goals)))


(defmulti format-command (fn [dm-name & _] dm-name) :hierarchy dm-hierarchy)

(defmethod format-command :default [_ command]
  (str command))

(defmulti draw-alternative (fn [dm-name & _] dm-name) :hierarchy dm-hierarchy)

(defmethod draw-alternative :default [& _])

(defn draw-selected-alternative [displayed-dm alternative-data state-display last-state]
  (let [[_ _ _ selected-alternatives-canvas _ & _] (dig-out-canvasses state-display)
        gc (.getGraphicsContext2D selected-alternatives-canvas)]
    (draw/clear-graphics-context gc)
    (draw-alternative displayed-dm :selected alternative-data last-state gc)))


(defn configure-decision-display [displayed-dm decision-display state-display last-state]
  (let [tree-table-view-node (first (vec (.getChildren decision-display)))
        columns (vec (.getColumns tree-table-view-node))]
    ; Darstellung Alternativen in Tabelle
    (.setCellValueFactory
      (first columns)
      (proxy [Callback] []
        (call [^TreeTableColumn$CellDataFeatures nodecontent]
          (let [nodecontentmap (.. nodecontent (getValue) (getValue))
                node (Label. (get nodecontentmap "alternative-or-evaluator"))]
            (case (get nodecontentmap "linecontent")
              "alternative"
              (let [alternative-data (read-string (get nodecontentmap "alternative-or-evaluator"))]
                (.setText node (format-command displayed-dm alternative-data))
                ; add display of selected alternative
                (gut/set-action .setOnMousePressed node
                                (draw-selected-alternative displayed-dm alternative-data state-display last-state)))

              "evaluation"
              (do
                ; add click event -> action as if clicked on parent alternative
                (gut/set-action .setOnMousePressed node
                                (let [parentnodecontentmap (.getValue (.getParent (.getValue nodecontent)))
                                      alternative-data (read-string (get parentnodecontentmap "alternative-or-evaluator"))]
                                  (draw-selected-alternative displayed-dm alternative-data state-display last-state)))
                (when-not (= (get nodecontentmap "reason") "nil")
                  (let [original-reasons-data (read-string (get nodecontentmap "reason"))]
                    ; add tooltips for evaluators
                    (let [tooltip (Tooltip. (with-out-str (pprint original-reasons-data)))]
                      (.setStyle node (str "-fx-background-color: " (second (get @dm-colors displayed-dm)) ";"))
                      (.setTooltip node tooltip))))))

            (ReadOnlyObjectWrapper. node)))))))

(defn display-considered-alternatives [displayed-dm alternatives-data state-display last-state]
  (let [[_ _ alternatives-canvas _ _ & _] (dig-out-canvasses state-display)
        gc (.getGraphicsContext2D alternatives-canvas)]
    (draw/clear-graphics-context gc)
    (draw-alternative displayed-dm :considered alternatives-data last-state gc)))

(defmulti draw-goal (fn [dm-name & _] dm-name) :hierarchy dm-hierarchy)

(defmethod draw-goal :default [& _])


(defn draw-goals [displayed-dm goals-data state-display]
  (let [[_ goal-canvas _ _ _ & _] (dig-out-canvasses state-display)
        gc (.getGraphicsContext2D goal-canvas)]
    (draw/clear-graphics-context gc)
    (mapv #(draw-goal displayed-dm % gc) goals-data)))


; :command wird nicht explizit angezeigt
; Grund: man sieht sowieso die höchste Entscheidung
; und durch das Weglassen von Daten kann es sein, dass die commands nicht zur letzten gezeigten Entscheidungsfindung passen
(defn display-trace-state [tab-pane state-display trace {humans :humans} trace-index]
  (letfn [(get-trace-data [trace-entry] (nth trace-entry 3))]
    (let [selected-tab (.getSelectedItem (.getSelectionModel tab-pane))
          displayed-dm (keyword (.getText selected-tab))
          {goals :goals setup :setup decision :decision} (get-dm-state displayed-dm trace trace-index)
          [goal-label hps-displays] (vec (.getChildren (.getContent selected-tab))) ; !!! command!
          [setup-display decision-display] (vec (.getItems hps-displays))
          {[_ _ _ state-event-data :as robot-state] :robot :as state-events} (get-world-state trace humans trace-index)]
      (hpsgui/update-setup-display (get-trace-data setup) setup-display)
      (hpsgui/update-decision-display (get-trace-data decision) hps-displays)

      (.setMaxWidth goal-label 700)
      (.setText goal-label (format-goals displayed-dm goals))
      (draw-goals displayed-dm goals state-display)

      (when robot-state
        (configure-decision-display displayed-dm decision-display state-display state-event-data)
        (display-considered-alternatives displayed-dm (get-trace-data decision) state-display state-event-data))

      (draw-world-state state-events state-display))))


;;; --------------------------------------------------------------------- ;;;
;;; GUI zusammenbauen                                                     ;;;
;;; --------------------------------------------------------------------- ;;;

(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dms/get-dm-from-name dm-keyword 'robot.navigation))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]

    (run! (fn* [p1__2608456#] (apply hpsgui/initialize-experts-in-display p1__2608456#)) [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])))


(defn make-decision-module-tabs [tab-pane decision-modules display-trace-state-fun trace]
  (let [slider-adapt-funs (atom {})]
    (doseq [dm-keyword decision-modules]
      (let [tab (Tab. (name dm-keyword))
            tabcontentpane-with-goal (BorderPane.)
            goal-label (Label. "current goals")
            tabcontentpane-hps-displays (structure/make-decision-module-basic-display)
            [fg-color bg-color] (get @dm-colors dm-keyword)
            dm-subtrace (filter (fn [[_ _ source _]] (= source dm-keyword)) trace)
            [dm-control-box dm-slider-adapt-fun] (make-dm-control-box dm-subtrace dm-keyword trace)]

        (swap! slider-adapt-funs assoc dm-keyword dm-slider-adapt-fun)

        (.setDividerPosition tabcontentpane-hps-displays 0 0.4)
        (.add (.getStyleClass tabcontentpane-with-goal) "morph-tabpane-with-goal")
        (.setStyle tab (str "-fx-background-color: #" bg-color ";"))
        (.setStyle tabcontentpane-with-goal (str "-fx-background-color: #" bg-color ";"))

        (.setTop tabcontentpane-with-goal goal-label)
        (.setCenter tabcontentpane-with-goal tabcontentpane-hps-displays)
        (.setBottom tabcontentpane-with-goal dm-control-box)
        (.setContent tab tabcontentpane-with-goal)
        (.add (.getTabs tab-pane) tab)

        (initialize-expert-display tabcontentpane-hps-displays dm-keyword)

        (.setOnSelectionChanged tab
                                (proxy [EventHandler] []
                                  (handle [^Event event]
                                    (when (.isSelected tab)
                                      (let [slider-value (.getValue (.lookup (.getScene tab-pane) "#timeslider"))
                                            [_ goal-canvas alternatives-canvas selected-alternatives-canvas _ & _] (dig-out-canvasses (.lookup (.getScene tab-pane) "#hps-stateRegion"))]
                                        (mapv #(draw/clear-graphics-context (.getGraphicsContext2D %)) [goal-canvas alternatives-canvas selected-alternatives-canvas])
                                        ; Slider für Tab passend zum Haupt-Slider setzen
                                        (.setValue (.lookup (.getScene tab-pane) (str "#dm-timeslider-" (name dm-keyword)))
                                                   (get-slider-index-from-trace-event dm-subtrace (get-trace-event-from-slider-index trace slider-value)))
                                        (display-trace-state-fun (math/round slider-value)))))))))

    (defn update-dm-slider-value [dm-name event]
      ((get @slider-adapt-funs dm-name) event))))


(defn display-decision-process [scene [trace-header trace]]
  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        decision-modules-tab-pane (TabPane.)
        display-trace-state-fun (partial display-trace-state decision-modules-tab-pane stateRegion trace trace-header)
        info-and-control-box (make-trace-info-and-control-box trace (make-trace-control-elements trace display-trace-state-fun) trace-header)]

    (.setId decision-modules-tab-pane "dm-tabpane")
    (gut/add-to-container controlRegion info-and-control-box)
    (gut/add-to-container decisionModules decision-modules-tab-pane)

    (HBox/setHgrow decision-modules-tab-pane Priority/ALWAYS)
    (.setDividerPosition hps-split-pane 0 0.6)

    (make-decision-module-tabs
      decision-modules-tab-pane
      (or (:decision-modules trace-header) [:decision-module])
      display-trace-state-fun
      trace)
    (.. (.lookup scene "#environment-choices") (getSelectionModel) (selectFirst))
    (display-trace-state-fun 0)
    )
  (doto (.getWindow scene)
    (gut/adjust-stage-size)
    (.centerOnScreen)))


