; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.control.robot)


(defn pprint-jointname [jointname]
  (clojure.string/join (let [[side & restname] (replace {\- \space} (rest (str jointname)))] (concat (if (= side \r) "right" "left") restname))))

(defn make-joint-slider [label min max on-action-fun]
  (let [slider (Slider.)
        sliderdef {:slider slider
                   :label (Label. label)
                   :content (TextField. (str (.getValue slider)))}]
    (gut/add-style (:content sliderdef) "numberlabel")
    (gut/add-style (:label sliderdef) "joint-label")

    (gut/set-button-action (:content sliderdef)
      (.setValue slider (read-string (.getText (:content sliderdef)))))

    (doto (:slider sliderdef)
      (.setMin min)
      (.setMax max))
    (.addListener (.valueProperty slider)
      (proxy [ChangeListener] []
         (^void changed [^ObservableValue ov ^Number oldval ^Number newval]
            (.setText (:content sliderdef) (format "%.2f" newval))
            (on-action-fun newval))))
    sliderdef))


(defn define-arm-sliders [side]
  (let [joint-limits-def (if (= side :left)
                           robot.connection/pr2-joint-limits-left
                           robot.connection/pr2-joint-limits-right)
        arm-sliders-box (GridPane.)
        sliders (map (fn [[jointname [min max]]]
                       (merge
                         (make-joint-slider
                           (pprint-jointname jointname) min max
                           (fn [newval]
                             (robot.connection/send-command
                               (keyword (str "set-" (name side) "-arm-joint"))
                               {:joint (clojure.string/replace (name jointname) #"-" "_")
                                :value newval})))
                         {:joint jointname}))
                     (vec joint-limits-def))]

    ;; arrange in GUI
    (dotimes [ii (count sliders)]
      (let [slider (nth sliders ii)]
        (doto arm-sliders-box
          (.add (:label slider) 0 ii)
          (.add (:slider slider) 1 ii)
          (.add (:content slider) 2 ii))))

    ;; configure and style elements
    (gut/add-style arm-sliders-box "sliders-box")

    ;; return element with sliders
    arm-sliders-box
))

(defn define-arm-controls [side]
  (let [arm-control-box (VBox.)
        gripper-box (HBox.)
        gripper-label (Label. (format "%s gripper" (name side)))
        grab-button (Button. "Grab")
        release-button (Button. "Release")]
    (gut/add-style arm-control-box "control-components-box")
    (gut/add-style gripper-label "joint-label")
    (.setStyle gripper-box "-fx-spacing: 10;")

    (gut/add-to-container gripper-box gripper-label grab-button release-button)

    (gut/add-to-container arm-control-box
      (define-arm-sliders side) gripper-box)

    (gut/set-button-action grab-button
      (robot.connection/send-command (keyword (format "%s-gripper-grab" (name side)))))

    (gut/set-button-action release-button
      (robot.connection/send-command (keyword (format "%s-gripper-release" (name side)))))

    arm-control-box))

(defn define-other-joint-controls []
  (let [control-box (GridPane.)
        torso-slider     (make-joint-slider "torso" 0.0 0.31
                           (fn [newval] (robot.connection/send-command :torso {:value newval})))
        head-pan-slider  (make-joint-slider "head pan" -3.0 3.0
                           (fn [newval] (robot.connection/send-command :head
                                          {:pan newval :tilt (:head_tilt_joint @robot.state/joint-state)})))
        head-tilt-slider (make-joint-slider "head tilt" -0.47 1.396
                           (fn [newval] (robot.connection/send-command :head
                                          {:tilt newval :pan (:head_pan_joint @robot.state/joint-state)})))]
    (letfn [(add-slider [slider row]
              (doto control-box
                (.add (:label slider) 0 row)
                (.add (:slider slider) 1 row)
                (.add (:content slider) 2 row)))]
      (add-slider torso-slider 0)
      (add-slider head-pan-slider 1)
      (add-slider head-tilt-slider 2))

    (gut/add-style control-box "control-components-box" "sliders-box")

    control-box))
