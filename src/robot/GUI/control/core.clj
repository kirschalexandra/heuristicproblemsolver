; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.control.core
  (:gen-class
   :extends javafx.application.Application)
  (:require [clojure.java.io :as io]
            clojure.test
            robot.GUI.control.menu
            robot.GUI.control.robot
            [utilities.gui :as gut])
  (:import javafx.scene.layout.VBox
           javafx.scene.Scene
           javafx.stage.Stage))

(defn -start [app ^Stage stage]
  (let [scene (Scene. (VBox.))]

    ;; set style
    ;;(.add (.getStylesheets scene) (.toExternalForm (io/resource "gui.css")))
    (.add (.getStylesheets scene) (.toExternalForm (io/resource "robot.css")))

    (gut/add-to-container (.getRoot scene)
                          (robot.GUI.control.menu/create-menu)
                          (robot.GUI.control.robot/make-robot-control-gui))

    ;; show GUI
    (doto stage
      (.setTitle "ROBOT GUI")
      (.setScene scene)
      (.show))))


(def cleanup-fun-at-stop (atom nil))

(defn -stop [_]
  (when (clojure.test/function? @cleanup-fun-at-stop)
    (@cleanup-fun-at-stop))
  (System/exit 0))
