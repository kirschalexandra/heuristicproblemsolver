; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.GUI.control.robot
  (:require
   [robot.connection]
   [robot.state]
   [utilities.gui :as gut])
  (:import
   (javafx.beans.value ChangeListener ObservableValue)
   (javafx.scene.control Label Slider Button TextField)
   (javafx.scene.layout VBox HBox GridPane))
  (:load "robot/commands" "robot/percepts"))


(defn make-robot-control-gui []
  (let [content-box (VBox.)
        arm-control-box (HBox.)
        state-box (make-state-elements)]

    (gut/add-style arm-control-box "arm-control-box")
    (gut/add-style content-box "structural-box")

    ;; put elements together
    (gut/add-to-container arm-control-box
      (define-arm-controls :left)
      (define-arm-controls :right))
    (gut/add-to-container content-box
      state-box arm-control-box (define-other-joint-controls))

    ; return GUI element with robot control
    content-box))
