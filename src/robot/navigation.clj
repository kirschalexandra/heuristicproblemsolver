; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.navigation
  (:use [hps.types]
        [robot.control]
        [hps.decision-modules]
        [hps.goals]
        [clojure.pprint]
        [hps.heuristics]
        )
  (:require [robot.connection :as conn :refer [send-command]]
            [robot.environment :as env]
            [robot.state :as state]
            [robot.utilities :as util]
            [utilities.math :as mut]
            [utilities.misc :as utmisc]
            [hps.library :as lib]
            [clojure.math.numeric-tower :as math])
  (:load "navigation/goalpoint-to-command/contingency"
         "navigation/goalpoint-to-command/pose-closeness"
         "navigation/goalpoint-to-command/constancy"
         "navigation/goalpoint-to-command/safety"
         "navigation/goalpoint-to-command/movement-comfort"
         "navigation/goalpoint-to-command/dwa"
         "navigation/plan-to-goalpoint/select-subgoal"
         "navigation/goalpoint-to-plan/topomap-plan"
    ;"navigation/goaltypes"
         "navigation/configurations"
         ))

(def robot-standard-params
  {:max-iterations-per-step   3                             ; Anzahl Versuche bis bei step einfach die beste Option genommen wird, egal ob sie viel besser ist als die anderen
   :relative-limit-accept     0.1                           ; soviel muss die beste Bewertung relativ über der zweitbesten liegen
   :accept-single-alternative false
   :run-frequency-fun         (run-frequency-fun-by-time 100)
   :evaluator-aggregation-fun [:weighted-sum :raw]})

(def dm-nav-params
  (assoc robot-standard-params
    :default-decision (map->DecisionAlternative {:value       [:motion {:x 0 :y 0 :w 0}]
                                                 :produced-by :emergency-command})
    :merge-similar-alternatives? false))

;;; interagierende Module
(def current-config (atom :hps-sub))                        ;:classical) ;:hps-sub)

(declare-top-level-dm :complex-nav-dm
                      :goaltype (map->Goaltype
                                  {:name               'nav-goaltype-complex
                                   :statevars          [state/robot-pose]
                                   :closeness-function util/point-distance
                                   :threshold          0.1
                                   :parameters         [:x :y :yaw]})
                      :dm-dependencies [:plan-bdm :subgoals-bdm :nav-bdm]
                      :available-experts {:producers  [:repeat-command :motion-primitives :turn
                                                       :random
                                                       :p-control :towards-goalpose]
                                          :evaluators [:dwa-align :dwa-velocity :dwa-goal-region
                                                       :look-at-path :look-at-goalpoint :velocity
                                                       :p-control :towards-goalpose :goal-distance
                                                       :safety-humans :human-approach :safety-laser :safety-tables
                                                       :accept-all]
                                          :adaptors   [:turn :slow-down]}
                      :configure-fun (lib/make-configure-fun-static (:nav-complex (@current-config @dm-configurations)))
                      :decision-parameters dm-nav-params)


(declare-dm :plan-bdm
            :goaltype (map->Goaltype
                        {:name           'plan-goaltype
                         :parameters     [:x :y :yaw]
                         :log-parameters [:x :y :yaw]})
            :available-experts {:producers  [:a-star]
                                :evaluators [:a-star]}
            :configure-fun (lib/make-configure-fun-static (:plan (@current-config @dm-configurations)))
            :decision-parameters robot-standard-params)


(declare-dm :subgoals-bdm
            :goaltype (map->Goaltype
                        {:name           'subgoals-goaltype
                         :parameters     [:path]
                         :log-parameters [:path]})
            :available-experts {:producers  [:first-planpoint :reachable-mappoint :all-plan-points :goalpoint :work-off-plan]
                                :evaluators [:prefer-point-near-goal :prefer-late-point-in-plan :prevent-unreachable-points :keep-orientation :accept-all]}
            :configure-fun (lib/make-configure-fun-static (:subgoals (@current-config @dm-configurations)))
            :decision-parameters robot-standard-params)



(declare-dm :nav-bdm
            :goaltype (map->Goaltype
                        {:name               'nav-goaltype-simple
                         :statevars          [state/robot-pose]
                         :closeness-function util/point-distance
                         :threshold          0.05
                         :parameters         [:x :y :yaw]   ; :yaw wird in Experten nicht verwendet, bleibt aber drin für Vergleichszwecke und für graphische Darstellung Zwischenziele
                         :log-parameters     [:x :y :yaw]})
            :available-experts {:producers  [:repeat-command :motion-primitives :turn
                                             :random
                                             :p-control :towards-goalpoint] ; p-control macht nicht viel Sinn, weil keine sinnvolle Orientierung gesetzt ist, ist aber nützlich zum Testen
                                :evaluators [:dwa-align :dwa-velocity :dwa-goal-region
                                             :look-at-path :look-at-goalpoint :velocity
                                             :p-control :towards-goalpoint :goal-distance
                                             :safety-humans :human-approach
                                             :safety-laser :safety-tables]
                                :adaptors   [:turn :slow-down]}
            :configure-fun (lib/make-configure-fun-static (:nav-simple (@current-config @dm-configurations)))
            ;:configure-fun (fn [dm]
            ;                 (let [config-strategy (lib/get-evaluators :minimalist dm)] ; Optionen: :take-the-best :minimalist
            ;                   (map->DecisionSetup
            ;                     {:experts    {:producers  (:producers (:experts (:nav-simple (@current-config @dm-configurations))))
            ;                                   :evaluators (concat (:evaluators (:experts config-strategy)) [:safety-laser :safety-tables])}
            ;                      :parameters (:parameters config-strategy)})))
            ;:reconfigure-fun (fn [dm]
            ;                   (let [config-strategy (lib/update-evaluators :minimalist dm)] ; Optionen: :take-the-best :minimalist
            ;                     (map->DecisionSetup
            ;                       {:experts    {:producers  (:producers (:experts (:nav-simple (@current-config @dm-configurations))))
            ;                                     :evaluators (concat (:evaluators (:experts config-strategy)) [:safety-laser :safety-tables])}
            ;                        :alternatives (lib/update-alternatives :elimination dm)
            ;                        :parameters (:parameters config-strategy)})))
            :decision-parameters dm-nav-params)

(declare-dm-connections
  :goal-to-goal [{:from :complex-nav-dm :to :plan-bdm :transform-fun #(when-not (nil? %) (select-keys % [:x :y :yaw]))}]
  :decision-to-goal [{:from :plan-bdm :to :subgoals-bdm :transform-fun #(hash-map :path %) :inherit-goal true}
                     {:from :subgoals-bdm :to :nav-bdm :transform-fun #(assoc % :yaw 0)}]
  :decision-to-alternatives [{:from                         :nav-bdm :to :complex-nav-dm
                              :transform-fun                identity :filter-fun #(take 10 %)
                              :transfer-original-evaluation 1.0}])

;;; nur lokale Navigation

(declare-top-level-dm :simple-nav-dm
                      :goaltype (map->Goaltype {:statevars          [state/robot-pose]
                                                :closeness-function util/point-distance
                                                :threshold          0.1
                                                :parameters         [:x :y :yaw]})
                      :available-experts {:producers  [:repeat-command :motion-primitives :turn
                                                       :random
                                                       :p-control :towards-goalpose]
                                          :evaluators [:dwa-align :dwa-velocity :dwa-goal-region
                                                       :look-at-path :look-at-goalpoint :velocity
                                                       :p-control :towards-goalpose :goal-distance
                                                       :safety-humans :human-approach :safety-laser :safety-tables]
                                          :adaptors   [:turn :slow-down]}
                      :configure-fun (lib/make-configure-fun-static {:experts    {:producers  [:p-control]
                                                                                  ;[:random :quantity 31]]
                                                                                  :evaluators [[:p-control :weight 0.5]]}
                                                                     :parameters {:merge-similar-alternatives? false}})
                      ; [:goal-distance :weight 0.81]]}}))
                      :decision-parameters dm-nav-params)


