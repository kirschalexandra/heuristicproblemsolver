; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.core
  (:require [robot.connection :refer [send-command]]
            [robot.control :as control :refer [tuck-arms]]
            [utilities.math :as mut]
            [robot.GUI.control.core]
            [robot.GUI.hps.core]
            [hps.core]
            [clojure.java.io :as io]
            [clojure.string :as string])
  (:use [clojure.repl :only (doc find-doc apropos)]
        [hps.types]
        [hps.decision-modules]
        [robot.connection]
        [robot.state]
        [robot.environment]
        [robot.achieve]
        [robot.navigation]
        [robot.tests.exp-geradeaus.control]
        [robot.GUI.hps.dm-specific] ; die Datei muss unbedingt hier im core geladen werden, weil sie sonst nirgends eingebunden wird
        [robot.tests.nav-safety]
        [robot.GUI.hps.export])
                                        ; [robot.monitoring] ; erstmal nicht benutzen, vielleicht irgendwann ordentlich machen
  (:import (java.lang Runtime System))
                                        ;(:load "tests/test_scripts/exp-geradeaus-screenshots" "tests/test_scripts/navigation")
  (:gen-class))


(def morse-tmp-script-file "morse-tmp.py")

(defn build-morse-script-path [filename]
  (io/file (System/getenv "MORSE_SCRIPTS") filename))

(defn prepare-morse-script [original-morse-script robot-start-pose]
  (let [original-script-path (build-morse-script-path original-morse-script)
        changed-script-path  (build-morse-script-path morse-tmp-script-file)]
    (spit changed-script-path
          (clojure.string/replace
           (clojure.string/replace
            (slurp original-script-path)
            "robot.translate(x=0.0, y=0.0, z=0.0)"
            (format "robot.translate(x=%.2f, y=%.2f, z=0.0)" (:x robot-start-pose) (:y robot-start-pose)))
           "robot.rotate(x=0.0, y=0.0, z=0.0)"
           (format "robot.rotate(x=0.0, y=0.0, z=%.4f)" (:yaw robot-start-pose))))
    changed-script-path))

;; start the desired parts of robot
(defn start-hps-robot [& {:keys [start-robot avatars
                             start-control-gui start-hps-gui
                             start-sim robot-start-pose morse-script-file]
                      :or {start-sim false start-control-gui false start-robot true start-hps-gui true
                           morse-script-file "pr2-apartment.py"}}]
  ; starten der einzelnen Komponenten noch nicht perfekt:
  ; start-robot wird bisher immer gebraucht
  ; wenn Morse nicht gestartet wird, gehe ich davon aus, dass er schon läuft
  ; -> am besten robot und morse aus der GUI starten lassen
  ; Es kann nur eine der beiden GUIs laufen
  (when start-sim
    (let [morse-script-path (if robot-start-pose
                              (prepare-morse-script morse-script-file robot-start-pose)
                              (build-morse-script-path morse-script-file))]
      (.exec (Runtime/getRuntime) (clojure.string/join (concat "morse run " (str morse-script-path))))
      (Thread/sleep 6000) ; geratene Zeit, schöner wäre natürlich ein Test, ob Morse gestartet ist (bei Apartment fastmode genügen 5s, mit Texturen besser 10s; geradeaus mit allem 6.5s)
      ;(.exec (Runtime/getRuntime) "wmctrl -r 'Blender' -t 5") ; tut leider momentan nichts
    ))
  (when start-robot
    (reset! robot.GUI.control.core/cleanup-fun-at-stop
            #(robot.connection/stop-robot start-sim))
    (robot.connection/start-robot)
    (tuck-arms)
    (when (not-empty avatars)
      (initialize-avatars avatars))) ; z.B. [ella] ; avatars müssen vordefiniert sein in robot/connection/avatars.clj
  (when start-control-gui
    (future (javafx.application.Application/launch robot.GUI.control.core (into-array String []))))
  (when start-hps-gui
    (robot.GUI.hps.core/start-robot.GUI)))
    ;(future (javafx.application.Application/launch robot.GUI.hps.core (into-array String [])))))


(defn stop-robot-now []
  (shutdown-agents))


