; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.achieve
  (:use [robot.control]
        [robot.navigation]
        [hps.types]
        [hps.decision-modules]
        [hps.goals]
        [clojure.pprint]
        [clojure.set])
  (:require [robot.connection :refer [send-command]]
            [robot.environment :as env]
            [robot.connection :as con]
            [robot.state :as state]
            [clojure.math.numeric-tower :as math]
            [clojure.java.io :as io]
            [utilities.misc :as mut])
  (:load "achieve/logging-types" "achieve/logging"))

;; Achieve soll (noch) kein HPS-Konstrukt werden, sondern ist eher ein Sonderfall zum Testen, normalerweise sollten decision modules dauerhaft laufen


(defn achieve
  ;([] (achieve :simple-nav-dm {:x 4.1 :y -1.2 :yaw 0 :timeout 2000}))
  ([] (achieve :complex-nav-dm {:x 4.1 :y -1.2 :yaw 0 :timeout 2000}))
  ([dm-name goal-params]
    ; start decision modules and initialize variables
    ;(println (start-toplevel-dm dm-name 'robot.navigation))

   (let [[main-dm & sub-dms :as all-dms] (start-toplevel-dm dm-name 'robot.navigation)
         goal-instance (make-goal (:goaltype main-dm) goal-params)
         trace (atom ())
         watch-tag (gensym)]

     ;reset and start logging
     (reset-log-times)
     (doseq [dm-spec all-dms]
       (log-decision-module-start trace dm-spec (:name dm-spec) watch-tag))
     (log-state-start trace (:statevars (:type goal-instance)))

     ;; let it run
     ;(add-watch (:executed-decision (hps.decision-modules/get-dm-from-name :nav-bdm 'robot.navigation)) :execute ; get commands and send them to execution
     (add-watch (:executed-decision main-dm) :execute ; get commands and send them to execution
                (fn [_ _ _ newval]
                  (when newval
                    (apply send-command (:value newval)))))

     (let [goal-achieved? (monitor-goal goal-instance)]     ; set and monitor goal
       (set-goal goal-instance main-dm)
       (deref goal-achieved? (:timeout goal-instance) nil))

     ;; stop everything
     (remove-goal main-dm)
     (remove-watch (:executed-decision main-dm) :execute-command)
     (apply send-command (:value (:default-decision (:decision-parameters main-dm))))
     (log-state-stop trace (:statevars (:type goal-instance)))
     (doseq [dm-spec all-dms]
       (log-decision-module-stop trace dm-spec (:name dm-spec) watch-tag))
     (stop-toplevel-dm dm-name 'robot.navigation)

     ;; write log file and return trace
     (let [finished-trace (package-trace @trace (cons dm-name (:dm-dependencies main-dm)))
           log-file (System/getenv "HPS_ROBOT_LOG_FILE")]
       (if log-file
         (spit (System/getenv "HPS_ROBOT_LOG_FILE") (str finished-trace)) ; unformatiertes Schreiben geht bedeutend schneller und Datei ist etwas kleiner, aber dafür auch unlesbar
         (println "path HPS_ROBOT_LOG_FILE not set, therefore not writing log file"))
       finished-trace))))

; Befehl zum schön formatieren, wenn man trace lesen will
; (spit "/work/kirsch/morph-log/trace-pp.hps" (with-out-str (pprint (read-string (slurp "/work/kirsch/morph-log/trace.hps")))))
