; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.control
  (:require [hps.rpl-constructs :as rpl]
            [robot.connection :refer [send-command]]
            [robot.state :as state]
            [robot.utilities :as util]))

(defn signal-robot-moved-with-timeout [& {:keys [timeout distance] :or {timeout 5000 distance 1.0}}]; [& [timeout]]
  (let [first-pos @state/robot-pose
        pos-changed (promise)]
    (add-watch state/robot-pose :change
               (fn [_ _ oldval newval]
                 (when (and (not= oldval newval) (> (util/point-distance first-pos newval) distance))
                   (deliver pos-changed true))))
    (if (deref pos-changed timeout false)
      (println "robot has moved")
      (println "stopped waiting for robot to move"))
    (remove-watch state/robot-pose :change)))

(defn move-forward [& {:keys [distance] :or {distance 2.0}}]
  (send-command :motion {:x 1.0 :y 0.0})
  (signal-robot-moved-with-timeout :distance distance)
  (send-command :motion {:x 0.0 :y 0.0}))



;; Wertebereiche und Erklärung der Armgelenke s. ~/arbeit/docs/manuals/projects/robot/joint-limits.txt
(let [left-tucked {:l-shoulder-pan-joint 0.06024
                   :l-shoulder-lift-joint 1.248526
                   :l-upper-arm-roll-joint -1.789070
                   :l-elbow-flex-joint 1.683386
                   :l-forearm-roll-joint -1.7343417
                   :l-wrist-flex-joint -0.0962141
                   :l-wrist-roll-joint -0.0864407}
      right-tucked {:r-shoulder-pan-joint -0.023593
                    :r-shoulder-lift-joint 1.39
                    :r-upper-arm-roll-joint 1.5566882
                    :r-elbow-flex-joint 2.124408
                    :r-forearm-roll-joint 1.4175
                    :r-wrist-flex-joint 1.8417
                    :r-wrist-roll-joint 0.21436}]

  (defn tuck-arms []
    (send-command :set-right-arm-joints right-tucked)
    (send-command :set-left-arm-joints left-tucked))
)





(defn control-robot-to-point [point controller]
  (while (not (Thread/interrupted))
    (send-command :motion (controller point))
    (Thread/sleep 200)))

(defn move-straight [point]
  (let [robot-rotation (:yaw @state/robot-pose)
        sin-robot-rot (Math/sin robot-rotation)
        cos-robot-rot (Math/cos robot-rotation)
        delta-x-unturned (- (:x point) (:x @state/robot-pose))
        delta-y-unturned (- (:y point) (:y @state/robot-pose))
        delta-x (- (* delta-x-unturned cos-robot-rot) (* delta-y-unturned sin-robot-rot))
        delta-y (+ (* delta-x-unturned sin-robot-rot) (* delta-y-unturned cos-robot-rot))]
    ; (println "delta x: " delta-x)
    ; (println "delta y: " delta-y)
    (if (> (Math/abs delta-x) (Math/abs delta-y))
      {:x (* (Math/signum delta-x) 1.0) :y (/ delta-y delta-x)}
      {:x (/ delta-x delta-y) :y (* (Math/signum delta-y) 1.0)})))


(defn reach-point
  ([] (reach-point (merge @state/robot-pose {:x (- (:x @state/robot-pose) 2.0) :y (- (:y @state/robot-pose) 2.0)})))
  ([point & {:keys [timeout tolerance controller] :or {timeout 7000 tolerance 0.2 controller move-straight}}]
   (let [point-reached (promise)]
     (add-watch state/robot-pose :absolute
       (fn [_ _ _ newval]
         (when (< (util/point-distance @state/robot-pose point) tolerance)
           (deliver point-reached true))))
     (rpl/pursue
       (control-robot-to-point point controller)
       (deref point-reached timeout false))
     (if (and (realized? point-reached) @point-reached)
       (println "point reached")
       (println "timeout"))
     (send-command :motion)
     (remove-watch state/robot-pose :absolute))))




