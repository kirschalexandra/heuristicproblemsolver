; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; Ich konnten den Code leider nicht in robot.state packen, weil es dann eine zyklische Abhängigkeit mit robot.connection gibt
                                        ; momentan nur für einzelne Tests benutzen, vielleicht irgendwann ordentlich machen
(ns robot.monitoring
  (:require [clojure.pprint :refer :all]
            [hps.state :as hps :refer :all]
            [robot.connection :as conn :refer :all]
            [robot.state :as state :refer :all]
            [robot.utilities :as util]))

(defn get-last-state [[last-state & previous-states] timestamp]
  (when last-state
    (if (> timestamp (:timestamp last-state))
      last-state
      (get-last-state previous-states timestamp))))


;;; Funktioniert so nicht: überlastet den Rechner
(defn activate-pose-monitor [statevar command-type]
  (let [last-command (atom nil)
        prediction-tolerance {:x 0.01 :y 0.01 :yaw 0.01}]

    ; Annahme, dass Kommando in command-variables ist, wenn es ein Service ist, geht es so nicht (kann man rausfinden in robot-definitions)
    (add-watch (command-type @conn/command-variables) :modelmonitor
      (fn [_ _ _ newval]
        (println "x")
        (swap! last-command [(.getTime (java.util.Date.)) newval])))

    ; (add-watch statevar command-type
    ;   (fn [_ _ _ newstate]
    ;     (when @last-command
    ;       (let [[last-command-timestamp command] @last-command]
    ;         (when (< last-command-timestamp (:timestamp newstate)) ;;; checken, ob es eine history gibt und ggf. eine anlegen
    ;           (when-let [last-state (get-last-state (:history (meta statevar)) last-command-timestamp)]
    ;             (let [prediction (hps/predict statevar :command command :initial-state last-state :delta-time (- (:timestamp newstate) (:timestamp last-state)))
    ;                   diff (util/hashmap-distance prediction newstate)]
    ;               (when (some (fn [[k v]] (> v (k prediction-tolerance))))
    ;                 (println "prediction error detected!")
    ;                 (println "original state: " last-state)
    ;                 (println "command: " command)
    ;                 (println "resulting state: " newstate)
    ;                 (println "prediction: " prediction)))
    ;             (swap! last-command nil)))))))
))

(defn deactivate-pose-monitor [statevar command-type]
  (remove-watch (command-type conn/command-variables) :modelmonitor)
  (remove-watch statevar command-type))

;; diese Tests gehen prinzipiell
;; noch nicht schön: es gibt eine Verzögerung zwischen Vorhersage und Beobachtung von etwa 2-4 Zeitschritten
;; mögliche Gründe/Reparaturen:
;; - Zustand wird vor add-watch aufgezeichnet -> das add-watch könnte eine Weile dauern
;; - Kommandozeitpunkt mit aufzeichnen, evtl. auch mehr Zustände -> würde genauere Aussage zu Verzögerungen zulassen
(defn check-pose-prediction []
  (let [test-commands [{:x 1 :y 0 :w 0}
                       {:x 0 :y 1 :w 0}
                       {:x 0 :y -1 :w 0}
                       {:x 0 :y 0 :w 1}
                       {:x 0 :y 0 :w -1}
                       {:x 0.5 :y 0.5 :w 0.5}
                       {:x -0.5 :y -0.5 :w -0.5}
                       {:x (rand) :y (rand) :w (rand)}]
        command-length 2000
        data (atom [])]
    (doseq [cmd test-commands]
      (let [state-before @state/robot-pose
            predictions (map #(hps/predict state/robot-pose :initial-state state-before :command [:motion cmd] :delta-steps %) (range 1 11))
            states-observed (atom [])]
        (add-watch state/robot-pose :check
          (fn [_ _ _ newval]
            (swap! states-observed conj newval)))
        (send-command :motion cmd)
        (Thread/sleep command-length)
        (remove-watch state/robot-pose :check)
        (send-command :motion)
        (swap! data conj [state-before cmd predictions @states-observed])))
    (spit "/home/kirsch/predictions.txt" (with-out-str (pprint @data)))
    @data))

(defn filter-observations [[obs & otherobs] [tp & othertps :as timesteps] start-time obs-candidate]
  (cond
    (and tp obs)
      (let [time-to-find (+ start-time (/ (+ 0 tp) 60.0))] ; es scheint irgendwo eine Verschiebung um einen Zeitschritt zu geben
        (if (>= (:timestamp obs) time-to-find)
          (cons
            (if (and obs-candidate (< (- time-to-find (:timestamp obs-candidate)) (- (:timestamp obs) time-to-find)))
              obs-candidate
              obs)
            (filter-observations otherobs othertps start-time obs))
          (filter-observations otherobs timesteps start-time obs)))
    (empty? obs)
      obs-candidate
    :else ()))




(defn analyse-preds []
  (map
    (fn [[s0 c preds obs]]
      (let [relevant-obs (filter-observations obs (range 1 11) (:timestamp s0) nil)]
        {:s0 s0 :c c :diffs
          (map
            (fn [p o]
              {:prediction-diff (util/point-distance p s0)
               :observation-diff (util/point-distance o s0)
               :posediff (util/point-distance p o)
               :anglediff (util/delta-radian (:yaw p) (:yaw o))})
            preds relevant-obs)}))
    (read-string (slurp "/home/kirsch/predictions.txt"))))
