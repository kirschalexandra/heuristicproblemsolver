; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.io
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [hps.decision-modules :as dms]))

(defn get-filename-with-json-extension [filename]
  (if (= (subs filename (- (count filename) 5)) ".json")
    filename
    (str filename ".json")))

(defn save-config-to-file [config filename]
  (spit (get-filename-with-json-extension filename) (json/write-str config)))

(defn save-dm-config-to-file [dm-name ns-name filename]
  (save-config-to-file ((:configure-fun (dms/get-dm-from-name dm-name ns-name)) nil)
                       filename))

(defn load-config-from-file [filename]
  (letfn [(convert-config-vector [value]
            (cond (string? value) (keyword value)
                  (vector? value) (vec (map convert-config-vector value))
                  :else value))]
    (json/read-str (slurp (get-filename-with-json-extension filename))
                   :key-fn keyword
                   :value-fn (fn [_ val]
                               (if (vector? val)
                                 (mapv convert-config-vector val)
                                 val)))))

(defn load-config-into-dm [dm-name ns-name filename]
  (dms/update-dm-config dm-name ns-name (load-config-from-file filename)))


