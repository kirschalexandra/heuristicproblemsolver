; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.parameter-fitting
  (:require [hps.decision-modules :as dms]
            hps.types
            [utilities.math :as mut]
            hps.io
            [utilities.math :as math]))

;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Automatische Durchläufe eines DM --------------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn update-dm-params [dm-name ns-name config]
  (dms/update-dm-declaration dm-name (find-ns ns-name)
                             (fn [current-dm & args]
                               (hps.types/map->DecisionModule
                                 (assoc current-dm
                                   :configure-fun
                                   (fn [last-config]
                                     (hps.types/map->DecisionSetup
                                       (merge-with merge ((:configure-fun current-dm) last-config)
                                                   config))))))))

(defn run-test [dm-name ns-name world-setup dm-config prepare-world-fun run-dm-fun get-result-fun]
  (prepare-world-fun world-setup)
  (update-dm-params dm-name ns-name dm-config)
  (let [dm (dms/get-dm-from-name dm-name ns-name)]
    (run-dm-fun dm)
    (merge (get-result-fun dm)
           world-setup
           dm-config)))

;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Optimierung mit genetischem Algorithmus -------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn maybe-mutate [config {:keys [mutation-rate mutation-update-spec]}]
  (let [mutation-options [-0.1 0.1]]
    (if (< (rand 1.0) mutation-rate)
      (update-in config (into [] (concat [(rand-int (count config))] mutation-update-spec))
                 #(min (max (mut/round2 (+ % (nth mutation-options (rand-int (count mutation-options)))))
                            0.0)
                       1.0))
      config)))

(defn cross-over [config1 config2 cross-over-rate]
  (let [max-flips (int (/ (count config1) 2))]
    (loop [[next-conf1 & rest-conf1] config1
           [next-conf2 & rest-conf2] config2
           flip-count 0
           count 0
           new-conf1 []
           new-conf2 []]
      (if next-conf1
        (if (and (or (>= flip-count max-flips) (< (rand 1.0) cross-over-rate))
                 (not (>= (- count flip-count) max-flips)))
          (recur rest-conf1 rest-conf2 flip-count (inc count) (conj new-conf1 next-conf1) (conj new-conf2 next-conf2))
          (recur rest-conf1 rest-conf2 (inc flip-count) (inc count) (conj new-conf1 next-conf2) (conj new-conf2 next-conf1)))
        [new-conf1 new-conf2]))))

(defn resample-and-mutate [processed-results {:keys [population-size survival-number cross-over-rate] :as ga-params}]
  ; Annahme: Fitness ist eine positive Zahl und die Individuen sind aufsteigend nach Fitness sortiert
  (let [count-reproduce (- population-size survival-number) ; gerade Zahl!
        intervals (reductions + (map second processed-results))
        sum-of-weights (last intervals)
        selected-for-reproduction (take count-reproduce (repeatedly #(nth processed-results (count (take-while (fn [ii] (< ii (rand sum-of-weights))) intervals)))))
        reproduced-configs (mapcat (fn [[c1 c2]] (cross-over c1 c2 cross-over-rate)) (partition 2 (map first selected-for-reproduction)))]
    (concat (map first (take survival-number processed-results))
            (map #(maybe-mutate % ga-params) reproduced-configs))))

(defn run-genetic-algorithm [{:keys [max-iterations population-size] :as ga-params} generate-individuals-fun run-test-fun order-individuals-fun]
  (loop [population (take population-size (generate-individuals-fun))
         ii 1
         population-quality []]
    (let [iteration-result (run-test-fun population)
          processed-result (order-individuals-fun iteration-result)
          this-population-quality (mut/average (map second processed-result))]
      (println "genetic algorithm iteration " ii)
      (if (> ii max-iterations)
        [(first (first processed-result)) population-quality]
        (recur (resample-and-mutate processed-result ga-params) (inc ii) (conj population-quality this-population-quality))))))

;; ga-params:
;; :max-iterations
;; :population-size        ; population-size - survival-number muss gerade sein
;; :survival-number        ; direkt kopierte Individuen pro Iteration
;; :cross-over-rate        ; zwischen 0 und 1 -> Zufallsfaktor wie viel Material ausgetauscht wird (0.5 scheint ein sinnvoller Wert zu sein)
;; :mutation-rate          ; zwischen 0 und 1 -> Zufallsfaktor wie oft eine Mutation vorkommt
;; :mutation-update-spec   ; sagt an welcher Stelle in einem Individuum der neue Wert eingetragen werden soll (mit update-in)