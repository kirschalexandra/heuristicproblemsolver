; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.make-decision
  (:require [hps.types :refer :all]
            [utilities.math :as math]
            [utilities.misc :as mut]))

; diese default params sind nur dann der default, wenn get-and-evaluate-alternatives direkt aufgerufen wird
; wenn es über step-decision-module aufgerufen wird, sind sie schon mit default-decision-params gefüllt
(def produce-and-evaluate-default-params
  {:evaluator-aggregation-fun   math/average
   :merge-similar-alternatives? true})


(defn call-expert-fun [expert & other-args]
  (let [fun-without-params (apply partial (:fun expert) other-args)]
    (if (and (contains? expert :parameters)
             (not-empty (dissoc (:parameters expert) :weight)))
      (fun-without-params (:parameters expert))
      (fun-without-params))))


(defn produce [producer]
  (call-expert-fun producer))

(defn evaluate [evaluator alternative]
  (call-expert-fun evaluator alternative))

(defn adapt [adaptor alternative]
  (call-expert-fun adaptor alternative))

(defn produce-alternatives [producer]
  (let [prod-result (produce producer)]
    (map #(map->DecisionAlternative {:value % :produced-by [(:name producer)]})
         (if (or (nil? prod-result) (seq? prod-result)) prod-result (list prod-result)))))

(defn generate-alternatives-by-adaptation [initial-alternatives adaptor]
  (mapcat (fn [alt]
            (let [new-alternatives (adapt adaptor alt)]
              (map #(map->DecisionAlternative
                      {:value % :produced-by (:produced-by alt) :altered-by (conj adaptor (:altered-by alt))})
                   (if (or (nil? new-alternatives) (seq? new-alternatives))
                     new-alternatives
                     (list new-alternatives)))))
          initial-alternatives))


;;; --- AGGREGATION FUNCTIONS ------------------------------------------- ;;;
(defmulti aggregate-evaluations (fn [aggregation-fun-spec _ _] (first (mut/as-vector aggregation-fun-spec))))
; bei Aggregierung muss Zahlenvektor rauskommen, dessen Werte so sortiert sind wie bei den alternatives, die reinkommen





;;; --- weighted sums --------------------------------------------------- ;;;
(defmethod aggregate-evaluations :weighted-sum [aggregation-fun-spec evaluations alternatives]
  (let [[_ normalization-spec weights-spec] (mut/as-vector aggregation-fun-spec)]
    (when (not-empty evaluations)
      (let [weights (map
                      (case weights-spec
                        (nil :use-weights) (comp #(or % 0.5) :weight :parameters key)
                        :equal-weights (fn [_] 1.0))
                      evaluations)
            number-of-evaluators (count evaluations)]
        (apply map (fn [& args]
                     (math/sum-product args weights))
               (case normalization-spec
                 :raw (map vals (vals evaluations))
                 (nil :normalized) (map (fn [evaluation-pair]
                                          (when-let [unnormalized-vals (vals (second evaluation-pair))]
                                            (math/normalize-number-list unnormalized-vals)))
                                        evaluations)))))))

;;; --- scoring --------------------------------------------------------- ;;;
(defn get-scores [scoring-fun-spec sorted-distinct-values]
  (mapcat (fn [[_ alt-val-pairs] associated-value]
            (map #(vector (first %) associated-value) alt-val-pairs))
          sorted-distinct-values
          (case scoring-fun-spec
            :borda (reverse (take (count sorted-distinct-values) (range)))
            :formula-one-championship (take (count sorted-distinct-values) (concat [25 18 15 12 10 8 6 4 2 1] (repeat 0))))))


(defmethod aggregate-evaluations :scoring [aggregation-fun-spec evaluations alternatives]
  (let [scoring-values (map (fn [[evaluator evaluations-per-alt]]
                              (let [sorted-distinct-values (sort-by key > (group-by val evaluations-per-alt))]
                                (into (sorted-map-by (partial mut/comp-order-like-seq alternatives :value))
                                      (get-scores (or (second (mut/as-vector aggregation-fun-spec)) :borda) sorted-distinct-values))))
                            evaluations)]
    (apply map (fn [& args]
                 (math/sum-product args (repeat 1)))
           (map vals scoring-values))))


;;; --- ranking --------------------------------------------------------- ;;;

(defn get-net-preferences [alternatives]
  (mut/map-to-kv (fn [{evaluations-alt-top :evaluations :as alt-top}]
                   (when (not-empty evaluations-alt-top)
                     (mut/map-to-kv (fn [{evaluations-alt-cmp :evaluations :as alt-cmp}]
                                      ;(compare-alternative-rankings alt-top alt-cmp))
                                      (if (identical? alt-top alt-cmp)
                                        0
                                        ; Evaluatoren müssen gleich sortiert sein (was sie aber auch sein sollten durch Umwandlung in apply-evaluators)
                                        (loop [[{value-alt-cmp :value} & rest-evaluations-alt-cmp] evaluations-alt-cmp
                                               [{value-alt-top :value} & rest-evaluations-alt-top] evaluations-alt-top
                                               net-count 0]
                                          (let [new-count (when-not (or (nil? value-alt-cmp) (nil? value-alt-top))
                                                            (cond (< value-alt-cmp value-alt-top) (inc net-count)
                                                                  (> value-alt-cmp value-alt-top) (dec net-count)
                                                                  :else net-count))]
                                            (if (empty? rest-evaluations-alt-cmp) ;ich muss nur auf eines testen, die andere Liste muss genauso lang sein
                                              new-count
                                              (recur rest-evaluations-alt-cmp rest-evaluations-alt-top new-count))))))
                                    alternatives)))
                 alternatives))

(defmulti aggregate-voting (fn [aggregation-fun-spec _] aggregation-fun-spec))

(defmethod aggregate-voting :copeland [_ net-preferences]
  (map (fn [[_ alt-comps]]
         (loop [[next-net-pref & rest-net-prefs] (vals alt-comps)
                copeland-score 0]
           (let [new-score (cond (> next-net-pref 0) (inc copeland-score)
                                 (< next-net-pref 0) (dec copeland-score)
                                 :else copeland-score)]
             (if (empty? rest-net-prefs)
               new-score
               (recur rest-net-prefs new-score)))))
       net-preferences))

(defmethod aggregate-voting :sequential-majority-comparison [_ net-preferences]
  (cond (empty? net-preferences) []
        (empty? (rest net-preferences)) [1]
        :else
        (let [[best-alternative best-alt-index]
              (loop [winner (key (first net-preferences))   ; erstes Alternative in net-preferences Liste
                     winner-index 0
                     [_ [alt2 _ :as current] & rest-alts] (vec net-preferences) ;
                     current-index 1]                       ; Vergleich ab zweiter Alternative in net-preferences Liste
                (let [[new-winner new-winner-index] (if (< (get (get net-preferences winner) alt2) 0)
                                                      [alt2 current-index]
                                                      [winner winner-index])]
                  (if (empty? rest-alts)
                    [new-winner new-winner-index]
                    (recur new-winner new-winner-index (cons current rest-alts) (inc current-index)))))]
          ; Rückgabewert: Liste mit lauter Nullen, nur Gewinner hat eine 1
          (assoc (vec (take (count net-preferences) (repeat 0))) best-alt-index 1))))



(defmethod aggregate-evaluations :ranking [aggregation-fun-spec evaluations alternatives]
  (aggregate-voting (or (second (mut/as-vector aggregation-fun-spec)) :copeland)
                    (get-net-preferences alternatives)))


; Rückgabewert von Evaluatoren: Hashmap mit {<alternative> [<value> <reason>] ... }
(defn apply-evaluators [evaluators alternatives parameters]
  (let [evaluations (mut/map-to-kv #(call-expert-fun % alternatives) evaluators) ; = decision-matrix, zeilenweise organisiert
        [promising-alternatives rejected-alternatives]
        (reduce (fn [[palts ralts] alt]
                  (let [evaluations-for-alt (map (fn [[evaluator res]]
                                                   (let [[value reason] (mut/as-vector (get res alt))]
                                                     {:value value :evaluator (:name evaluator) :reason reason}))
                                                 evaluations)
                        alt-with-evals (assoc alt :evaluations evaluations-for-alt)]
                    (if (some #(nil? (:value %)) evaluations-for-alt)
                      [palts (conj ralts (assoc alt-with-evals :aggregated-evaluation nil))]
                      [(conj palts alt-with-evals) ralts])))
                [[] []]
                alternatives)
        promising-evaluations (mut/kv-map (fn [altmap]
                                            (into (sorted-map-by (partial mut/comp-order-like-seq promising-alternatives :value)) ; sorted-map: sicherstellen, dass Alternativen bei allen Evaluatoren gleich sortiert sind
                                                  (map (fn [[alt eval-result]] ; Reasons wegwerfen
                                                         [alt (first (mut/as-vector eval-result))])
                                                       (apply dissoc altmap
                                                              (filter (fn [altdef]
                                                                        (some #(identical? (:value altdef) (:value %)) rejected-alternatives))
                                                                      (keys altmap))))))
                                          evaluations)
        ;aggregated-eval-per-alternative (aggregate-evaluations [:scoring :borda] promising-evaluations promising-alternatives)]
        aggregated-eval-per-alternative (aggregate-evaluations (:evaluator-aggregation-fun parameters) promising-evaluations promising-alternatives)]

    (concat
      (map (fn [alt aggregated-eval]
             (assoc alt :aggregated-evaluation aggregated-eval))
           promising-alternatives
           aggregated-eval-per-alternative)
      rejected-alternatives)))



(defn distinct-alts
  "Removes duplicates from the collection of alternatives.
   The producer of the duplicant is added to the list of producers for
   the alternative kept in the collection."
  ([alternatives]
   (map (fn [[value equal-alts]]
          (map->DecisionAlternative
            {:value       value
             :produced-by (mapcat :produced-by equal-alts)}))
        (group-by :value alternatives))))


(defn get-and-evaluate-alternatives [{:keys [experts parameters alternatives]}]
  (let [params (merge produce-and-evaluate-default-params parameters)
        ; merge nur zur Sicherheit falls die notwendigen Parameter fehlen, werden aber sowieso in default-decision-params gesetzt
        {:keys [producers evaluators adaptors]} experts
        all-alternatives (concat (mapcat produce-alternatives producers)
                                 alternatives
                                 (mapcat #(generate-alternatives-by-adaptation alternatives %) adaptors))]
    (apply-evaluators
      evaluators
      (if (:merge-similar-alternatives? params)
        (distinct-alts all-alternatives)
        all-alternatives)
      params)))

