; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.heuristics
  (:require [clojure.walk :refer :all]
            [hps.types :refer :all]
            [clojure.tools.logging :as log]))

;(:require [clojure.math.numeric-tower :as math]))



(defn parse-expert-parameters [args]
  ;;; Möglichkeiten für args:
  ;;; ein oder kein Argument für Ziel
  ;;; bei Evaluators ein Argument für Alternative(n)
  ;;; :optional-Argument für Parameters (Syntax angelehnt an Lisp &optional)
  (let [[fixed-params {:keys [goalvar optional reset-fun]}] (split-with (comp not keyword?) args)
        [optional-params-var optional-params-default] optional
        final-params-with-goal (vec (if goalvar (cons goalvar fixed-params) fixed-params))]
    [(if optional-params-var
       (conj final-params-with-goal optional-params-var)
       final-params-with-goal)
     optional-params-default
     reset-fun
     goalvar]
    ))

(defn make-save-expert-code [expert-type expert-name content]
  `(do
     (alter-meta! *ns* #(assoc-in % [:experts ~expert-type ~(keyword expert-name)] ~content))))

(defn get-expert
  [namespace expert-type expert-name]                       ; bei Angabe des Namespace das Objekt übergeben, z.B. (find-ns 'robot.navigation)
  (let [generic-expert-type (if (= (last (name expert-type)) \s) ; erlaubt jeweils singular oder plural, also :expert oder :experts
                              (keyword (clojure.string/join (butlast (name expert-type))))
                              expert-type)]
    (expert-name (generic-expert-type (:experts (meta namespace))))))

(defmacro def-expert-fun [args & body]
  (let [[funargs# params# reset-fun# goalvar#] (parse-expert-parameters args)]
    (merge {:fun        `(fn ~funargs#
                           ~@(if goalvar#
                               `((when (deref ~goalvar#)    ; man kann sich streiten, ob when die beste Lösung ist, bei Evaluators vielleicht besser 0 zurückliefern, wenn kein Ziel gesetzt ist
                                   ~@(postwalk #(if (= % goalvar#) `(deref ~goalvar#) %) body)))
                               body))
            :parameters params#
            :uses-goal  (boolean goalvar#)}
           (when reset-fun#
             {:reset-fun reset-fun#}))))



(defmacro def-heuristic [name & experts]
  `(do
     ~@(map (fn [[type expert]]
              (make-save-expert-code type name
                                     `(map->Expert
                                        (merge {:name ~(keyword name) :type ~type}
                                               ~expert))))
            (apply hash-map experts))))



