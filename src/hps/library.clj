;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns hps.library
  (:require [hps.types :refer :all]
            [utilities.math :as mut]))


;;; --------------------------------------------------------------------- ;;;
;;; configure/reconfigure funs                                            ;;;
;;; --------------------------------------------------------------------- ;;;

;; get/update evaluators
(defmulti get-evaluators (fn [label _] label))
(defmulti update-evaluators (fn [label _] label))

(defmethod get-evaluators :minimalist [_ dm]
  (let [available-evaluators (map :name (:evaluators (:available-experts dm)))]
    {:experts    {:evaluators [(rand-nth available-evaluators)]}
     :parameters {:remaining-tsp-evaluators available-evaluators}}))

(defmethod update-evaluators :minimalist [_ dm]
  (let [last-setup @(:decision-setup dm)
        last-evaluator (:name (first (:evaluators (:experts last-setup))))
        remaining-tsp-evaluators (:remaining-tsp-evaluators (:parameters last-setup))]
    {:experts    {:evaluators [(rand-nth remaining-tsp-evaluators)]}
     :parameters {:remaining-tsp-evaluators (remove #(= % last-evaluator) remaining-tsp-evaluators)}}))

(def evaluators-by-value '(:nearest-neighbour :convex-hull (:remaining-acc-dist :regions-convex-hull) :last-chance
                            :no-intersections :follow-lines :regions :avoid-splitting :start-intersection))

(defn pick-next-evaluator [[next & rest]]
  (if (coll? next)
    (if (== 1 (count next))
      [(first next) rest]
      (let [chosen-one (rand-nth next)]
        [chosen-one (cons (remove #(= % chosen-one) next) rest)]))
    [next rest]))

(defmethod get-evaluators :take-the-best [_ dm]
  (let [evaluator-order (or (:take-the-best-evaluator-order (:decision-parameters dm))
                            (shuffle (:evaluators (:available-experts dm)))) ; Wenn in Parameters keine Reihenfolge angegeben ist, wird eine zufällige gewählt
        [next-evaluator remaining-evaluators] (pick-next-evaluator evaluator-order)]
    {:experts    {:evaluators [next-evaluator]}
     :parameters {:remaining-tsp-evaluators remaining-evaluators}}))

(defmethod update-evaluators :take-the-best [_ dm]
  (let [[next-evaluator remaining-evaluators] (pick-next-evaluator (:remaining-tsp-evaluators (:parameters @(:decision-setup dm))))]
    {:experts    {:evaluators [next-evaluator]}
     :parameters {:remaining-tsp-evaluators remaining-evaluators}}))

;; update alternatives

(defmulti update-alternatives (fn [label _] label))

(defmethod update-alternatives :all [_ dm]
  (:retained-alternatives @(:internal-decision dm)))

(defmethod update-alternatives :elimination [_ dm]
  (let [remaining-alternatives (:retained-alternatives @(:internal-decision dm))
        evaluations (map :aggregated-evaluation remaining-alternatives)
        stdev-eval-remaining (mut/stdev evaluations)
        mean-eval-remaining (mut/average evaluations)
        cutoff (or (:elimination-by-aspects-cutoff-stdev (:parameters @(:decision-setup dm)))
                   (:elimination-by-aspects-cutoff-stdev (:decision-parameters dm))
                   0)
        keep (remove #(< (:aggregated-evaluation %)
                         (- mean-eval-remaining (* stdev-eval-remaining cutoff)))
                     remaining-alternatives)]
    (if (empty? keep)
      (:retained-alternatives @(:internal-decision dm))
      keep)))

;;; default configure/reconfigure funs

(defn make-configure-fun-static [configuration]
  (fn [_] (map->DecisionSetup configuration)))


(defn make-configure-fun-one-good-reason [evaluators-mode]
  (fn [dm]
    (map->DecisionSetup
      (merge-with (fn [a b] (if (map? a) (merge a b) (or b a)))
                  {:experts {:producers (map :name (:producers (:available-experts dm)))}}
                  (get-evaluators evaluators-mode dm)))))
; evaluators-mode -> :take-the-best :minimalist

(defn make-reconfigure-fun-one-good-reason [evaluators-mode alternatives-mode]
  (fn [dm]
    (map->DecisionSetup
      (merge-with (fn [a b] (if (map? a) (merge a b) (or b a)))
                  {:experts      {:producers []}
                   :alternatives (update-alternatives alternatives-mode dm)}
                  (update-evaluators evaluators-mode dm)))))
; evaluators-mode -> :take-the-best :minimalist
; alternatives-mode -> :all :elimination


;;; --------------------------------------------------------------------- ;;;
;;; alternative acceptance funs                                           ;;;
;;; --------------------------------------------------------------------- ;;;

(defn accept-any-alternative [_ _ _]
  true)


(defn accept-relative [best-alternative other-alternatives params]
  (let [relative-limit-accept (or (:relative-limit-accept params) 0.1)]
    (if (and other-alternatives (< 0.01 (:aggregated-evaluation (first other-alternatives))))
      (> (/ (- (:aggregated-evaluation best-alternative)
               (:aggregated-evaluation (first other-alternatives)))
            (:aggregated-evaluation (first other-alternatives)))
         (:relative-limit-accept params))
      (:accept-single-alternative params))))
