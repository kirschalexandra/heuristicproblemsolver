; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'hps.GUI.make-decision)

;;; --- make and adapt displays ----------------------------------------;;;

(defn make-expert-choice-title [expert-pane]
  (format "%s (%d)"
          (first (clojure.string/split (.getText expert-pane) #" "))
          (apply + (map #(if (.isSelected %) 1 0)
                        (remove #(= (.getText %) "All") (.getChildren (.getContent expert-pane)))))))


(defn make-titled-pane [title content]
  (let [pane (TitledPane. title content)]
    (.add (.getStyleClass pane) "expert-pane")
    pane))

; per Hand ausgesuchte Parameter, die man sinnvollerweise in GUI setzen will
(def displayed-parameters {:max-iterations-per-step     [Integer 1]
                           :evaluator-aggregation-fun   [["[:weighted-sum :normalized]"
                                                          "[:weighted-sum :raw]"
                                                          "[:weighted-sum :normalized :equal-weights]"
                                                          "[:weighted-sum :raw :equal-weights]"
                                                          "[:ranking :copeland]"
                                                          "[:ranking :sequential-majority-comparison]"
                                                          "[:scoring :borda]"
                                                          "[:scoring :formula-one-championship]"]
                                                         "[:weighted-sum :normalized]"]
                           :merge-similar-alternatives? [Boolean true]})


(defn fill-parameter-grid! [parameter-grid]
  (loop [[[paramkeyword [type-or-values default]] & rest-params] (vec displayed-parameters)
         row-count 0
         param-row-widget {}]
    (let [widget (cond (= type-or-values Integer) (TextField.)
                       (= type-or-values Boolean) (CheckBox.)
                       (sequential? type-or-values) (ChoiceBox.))]
      ; Default-Wert bzw. Werteliste setzen
      (cond (= type-or-values Integer) (.setText widget (str default))
            (= type-or-values Boolean) (.setSelected widget default)
            (sequential? type-or-values)
            (do (.addAll (.getItems widget) (into-array String type-or-values))
                (.setValue widget default)))
      ; in Grid einfügen
      (.add parameter-grid (Label. (name paramkeyword)) 0 row-count)
      (.add parameter-grid widget 1 row-count)
      (when-not (empty? rest-params)
        (recur rest-params (inc row-count) (assoc param-row-widget paramkeyword [row-count widget]))))))

(defn read-parameters-from-grid [parameter-grid]
  (loop [[label widget & restparams] (vec (.getChildren parameter-grid))
         parameters {}]
    (let [paramkw (keyword (.getText label))
          [type-or-values _] (get displayed-parameters paramkw)
          new-parameters (assoc parameters paramkw
                                           (cond (= type-or-values Integer) (Integer. (read-string (.getText widget)))
                                                 (= type-or-values Boolean) (.isSelected widget)
                                                 (sequential? type-or-values) (read-string (.getValue widget))))]
      (if (empty? restparams)
        new-parameters
        (recur restparams new-parameters)))))


(defn make-setup-display []
  (let [input-box (FlowPane.)
        parameter-grid (GridPane.)]
    ; stylen
    (.add (.getStyleClass input-box) "setup-flow-pane")
    (.add (.getStyleClass parameter-grid) "parameter-grid-pane")
    (.setPrefWrapLength input-box 300.0)

    ; Boxen zusammenbauen
    (apply gut/add-to-container input-box
           (map #(make-titled-pane % (VBox. 5.0)) ["Producers" "Adaptors" "Evaluators"]))

    (gut/add-to-container input-box (make-titled-pane "Parameters" parameter-grid))
    (fill-parameter-grid! parameter-grid)

    input-box))


(defn initialize-experts-in-display [pane experts]
  (let [box (.getContent pane)
        expert-choices (map #(CheckBox. (str (name (:name %)))) experts)
        all-checkbox (CheckBox. "All")]

    (doseq [cb expert-choices]
      (gut/add-to-container box cb)
      (.addListener (.selectedProperty cb)
                    (proxy [ChangeListener] []
                      (changed [^ObservableValue ov oldval newval]
                        (.setText pane (make-expert-choice-title pane))))))

    ;(if (empty? experts)
    ;(.setExpanded pane false)
    ;(.setExpanded pane true))

    (.setExpanded pane false)

    (gut/add-to-container box all-checkbox)
    (.addListener (.selectedProperty all-checkbox)
                  (proxy [ChangeListener] []
                    (changed [^ObservableValue ov oldval newval]
                      (doseq [ec expert-choices]
                        (.setSelected ec newval)))))))

(defn update-experts-in-display [pane experts]
  (doseq [checkbox (.getChildren (.getContent pane))]
    (.setSelected checkbox (if (some #(= (.getText checkbox) %) experts) true false))))
; falls Experten ausgewählt sind, die nicht unter den available-experts sind, gibt es keinen Fehler aber auch keine Warnung o.ä.


(defn update-setup-display [{experts :experts parameters :parameters} input-box]
  (let [[producer-pane adaptor-pane evaluator-pane parameters-pane] (vec (.getChildren input-box))
        {:keys [evaluators producers adaptors]} (into (hash-map) (map (fn [[key explist]] [key (map #(str (name %)) explist)]) experts))]

    ; Experts
    (doseq [[pane experts] [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]]]
      (update-experts-in-display pane experts))

    ; Parameters
    (let [grid (.getContent parameters-pane)]
      (.clear (.getChildren (.getContent parameters-pane)))
      (doseq [[[paramname paramvalue] row-number] (map list parameters (range (count parameters)))]
        (let [textfield (TextField. (str paramvalue))]
          (.add grid (Label. (name paramname)) 0 row-number)
          (.add grid textfield 1 row-number))))))



;;; --- read out values for decision process ---------------------------;;;

(defn get-selected-experts [selected-expert-names expert-list]
  (filter (fn [prod] (some #(= (:name prod) (keyword %)) selected-expert-names)) expert-list))

;(defn access-param-value [gui-element]
;  ; die hässliche Lösung mit dem String-Vergleich ist das einzige, was ich zum Laufen gebracht habe
;  ; schöner wäre sowas wie instance? aber das hat nicht funktioniert
;  (let [value (case (str (type gui-element))
;                "class javafx.scene.control.TextField" (.getText gui-element)
;                (.getValue gui-element))]
;    (when-not (or (= value "") (nil? value))
;      (let [interpreted-value (read-string value)]
;        (if (symbol? interpreted-value)
;          (resolve (read-string (str "hps.make-decision/" value)))
;          interpreted-value)))))
;
;(defn prepare-parameters [make-decision-parameters]
;  (conj
;    (map (fn [[k v]] {:label k :type :number :default v})
;         make-decision-parameters)
;    {:label :evaluator-aggregation-fun :type :choice :choices '(aggregate-average)}))

;; Auslesen der Daten über die JavaFX Baumstruktur
;; -> ist etwas gefährlich, weil es mitgeändert werden muss, falls sich die Oberflächenzusammenstellung nochmal ändert
;; -> ist aber auch schöner getrennt vom Aufsetzen der Darstellung und ermöglicht es mehrere Decision Modules parallel darzustellen
(defn get-setup-data [dm-pane]
  (letfn [(get-expert-names-from-pane [pane]
            (map #(.getText %)
                 (filter #(and (.isSelected %) (not= (.getText %) "All"))
                         (.getChildren (.getContent pane)))))]
    (let [display (first (vec (.getItems dm-pane)))
          [adaptor-pane evaluator-pane parameter-pane producer-pane]
          (sort-by (comp str #(.getText %)) (.getChildren display))]
      [(get-expert-names-from-pane producer-pane)
       (get-expert-names-from-pane evaluator-pane)
       (get-expert-names-from-pane adaptor-pane)])))
;(into (hash-map)
;      (map (fn [[lbl object]] [(keyword (.getText lbl)) (access-param-value object)])
;           (apply hash-map (.getChildren (.getContent parameter-pane)))))])))
