; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'hps.GUI.make-decision)

;;; Icons from https://icons8.com/web-app

(defn format-experts [experts]

  (cond
    (nil? experts) ""
    (sequential? experts) (clojure.string/join ", " (map name experts))
    :else (name experts)))

(defn alternative-to-tree-item [{command :value
                                 producers :produced-by
                                 adaptors :altered-by
                                 evaluations :evaluations
                                 aggregated-evaluation :aggregated-evaluation}
                                style]
  (let [icon (ImageView. (Image. (io/input-stream (io/resource (str "icons/" style ".png")))))]
    (.setFitHeight icon 15.0)
    (.setFitWidth icon 15.0)
    (let [cmd-item
           (TreeItem. (HashMap. {"alternative-or-evaluator" (with-out-str (pprint command))
                                 "linecontent" "alternative"
                                 "evaluation" (if (nil? aggregated-evaluation)
                                                "---"
                                                (format "%.2f" (float aggregated-evaluation)))
                                 "adaptors"  (format-experts adaptors)
                                 "producers" (format-experts producers)})
                      icon)

          eval-items (map (fn [{:keys [value evaluator reason] :as all}]
                            (TreeItem. (HashMap. {"alternative-or-evaluator" (name evaluator)
                                                  "linecontent" "evaluation"
                                                  "evaluation" (format "%.2f" (when value (float value)))
                                                  "reason" (pr-str reason)}))) ; hilfreich wenn mehr Infos angezeigt werden sollen als im Standard (s. Navigation)
                          evaluations)]
      (.setGraphic cmd-item icon)
      (.setAll (.getChildren cmd-item) (into-array eval-items))
      cmd-item)))


(defn make-alternatives-tree [] ; inkl. defn für updates
  (let [results-root (TreeItem. (HashMap. {"alternative-or-evaluator" "Root" :evaluation 0}))
        result-tree (TreeTableView. results-root)
        column-alternative (TreeTableColumn. "Alternative")
        column-evaluation (TreeTableColumn.)
        column-producers (TreeTableColumn. "Produced by")]

    (doto result-tree
      (.setShowRoot false)
      (.setTableMenuButtonVisible true))

    (.add (.getStyleClass result-tree) "hps-alternatives-tree")

    (.setPrefWidth column-alternative 220)
    (.setPrefWidth column-evaluation 50)
    (.setPrefWidth column-producers 170)

    ;; alle Spalten außer Producer
    (doseq [[columnvar column-map-name] {column-alternative "alternative-or-evaluator"
                                         column-evaluation "evaluation"}]
                                         ; column-producers "producers"}]
      (.setCellValueFactory columnvar
        (proxy [Callback] []
          (call [^TreeTableColumn$CellDataFeatures nodecontent]
            (ReadOnlyObjectWrapper. (.. nodecontent (getValue) (getValue) (get column-map-name))))))
      (.add (.getColumns result-tree) columnvar))

    ; Producer-Spalte
    (.setCellValueFactory column-producers
        (proxy [Callback] []
          (call [^TreeTableColumn$CellDataFeatures nodecontent]
            (let [nodecontentmap (.. nodecontent (getValue) (getValue))
                  adaptorstring (get nodecontentmap "adaptors")
                  node (Label. (get nodecontentmap "producers"))]
              (when-not (= adaptorstring "")
                (let [tooltip (Tooltip. (str "altered by " adaptorstring))]
                  (.setStyle node "-fx-background-color: -lightblue;")
                  (.setTooltip node tooltip)))
              (ReadOnlyObjectWrapper. node)))))
    (.add (.getColumns result-tree) column-producers)


    result-tree))


(defn make-decision-display []
  (let [output-box (VBox.)
        output-warnings (Label.)
        alternatives-tree (make-alternatives-tree)]
    (gut/add-to-container output-box
      alternatives-tree output-warnings)
    (VBox/setVgrow alternatives-tree Priority/ALWAYS)
    ; Elemente stylen
    (.add (.getStyleClass output-box) "decision-output-vbox")
    (.add (.getStyleClass output-warnings) "decision-warnings-label")

    output-box))



(defn update-decision-display [{decision :decision
                                retained-alternatives :retained-alternatives
                                rejected-alternatives :rejected-alternatives}
                               dm-pane]
  (let [display (second (vec (.getItems dm-pane)))
        [result-tree no-decision-label] (seq (.getChildren display))
        results-root (.getRoot result-tree)]
    (.setText no-decision-label
              (if (nil? decision) "No acceptable alternative found" ""))
    (.setExpanded results-root true)
    (.setAll (.getChildren results-root)
             (into-array (concat
                          (when-not (nil? decision)
                            (list (alternative-to-tree-item decision
                                                            (if (some #(= decision %) retained-alternatives)
                                                              "retained-decision"
                                                              "accepted"))))
                          (map #(alternative-to-tree-item % "retained")
                               (sort-by :aggregated-evaluation > (remove #(= decision %) (distinct retained-alternatives))))
                          (map #(alternative-to-tree-item % "rejected") (distinct rejected-alternatives)))))))
