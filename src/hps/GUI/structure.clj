; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.GUI.structure
  (:require [clojure.java.io :as io]
            hps.GUI.make-decision
            [utilities.gui :as gut])
  (:import java.lang.Double
           javafx.application.Platform
           [javafx.scene Node Scene]
           [javafx.scene.control Menu MenuBar SplitPane]
           [javafx.scene.layout BorderPane HBox Priority StackPane]))

(declare add-hps-mode-to-menu)

(def hps-modes (atom {}))

(defn register-hps-mode
  ([modename modeadjustfun] (register-hps-mode modename modeadjustfun false))
  ([modename modeadjustfun preselect-mode?]
    (swap! hps-modes assoc modename [modeadjustfun preselect-mode?])))


(defn fill-standard-hps-menu [menubar]
  (let [menufile (Menu. "File")]
    (.add (.getMenus menubar) menufile)
    (gut/create-menu-item menufile "Quit" {:key "Q" :modifier :ctrl} (fn [] (shutdown-agents) (Platform/exit)))))

(defn clear-hps-screen [scene]
  (let [menubar (.lookup scene "#hps-menubar-left")
        [decisionModules stateRegion] (vec (.getItems (.lookup scene "#hps-centerSplitPane")))
        controlRegion (.lookup scene "#hps-controlRegion")]
    (.clear (.getMenus menubar))
    (fill-standard-hps-menu menubar)
    (.clear (.getChildren controlRegion))
    (.clear (.getChildren decisionModules))
    (.clear (.getChildren stateRegion))
    (.setMaxWidth stateRegion Double/MAX_VALUE)))


(defn make-hps-menu []
  (let [menulist      (HBox.)
        menubar-left  (MenuBar.)
        menubar-right (MenuBar.)
        menufile      (Menu. "File")
        menumode      (Menu. "Mode")]

    (HBox/setHgrow menubar-left Priority/ALWAYS)
    (gut/add-to-container menulist menubar-left menubar-right)

    (.setId menubar-left  "hps-menubar-left")
    (.setId menubar-right "hps-menubar-right")

    (fill-standard-hps-menu menubar-left)
    (.add (.getMenus menubar-right) menumode)

    (defn add-hps-mode-to-menu [modename modestartfun preselect-mode]
      (let [newitem (gut/create-menu-item menumode modename
                      #(let [scene (.getScene menulist)]
                        (clear-hps-screen scene)
                        (modestartfun scene)
                        (when-let [stage (.getWindow scene)]
                          (gut/adjust-stage-size stage)
                          (.centerOnScreen stage))))]
        (when preselect-mode
          (.fire newitem)))
      [menubar-left menufile])

    menulist))


(defn make-decision-module-basic-display []
  (let [splitpane       (SplitPane.)
        configbox       (hps.GUI.make-decision/make-setup-display)
        alternativesbox (hps.GUI.make-decision/make-decision-display)] ;make-alternatives-tree)]
    (HBox/setHgrow splitpane Priority/ALWAYS)
    (.addAll (.getItems splitpane) (into-array Node [configbox alternativesbox]))
    (.setId configbox "hps-configbox")
    splitpane))

(defn get-element-from-decision-module-display [dm-splitpane element]
  (first (filter #(= element (.getId %)) (.getItems dm-splitpane))))


(defn build-basic-hps-scene []
  (let [border (BorderPane.)
        scene (Scene. border)
        centerSplitPane (SplitPane.)
        decisionModules (HBox.)
        stateRegion (StackPane.)
        controlRegion (StackPane.)]

    (.setMinWidth  border 500)
    (.setMinHeight border 300)

    (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/hps.css")))

    (.setId centerSplitPane "hps-centerSplitPane")
    (.setId controlRegion   "hps-controlRegion")
    (.setId stateRegion     "hps-stateRegion")

    (.addAll (.getItems centerSplitPane) (into-array Node [decisionModules stateRegion]))

    (.setTop    border (make-hps-menu))
    (.setBottom border controlRegion)
    (.setCenter border centerSplitPane)

    (doseq [[modename [modefun preselect?]] @hps-modes]
      (add-hps-mode-to-menu modename modefun preselect?))

    (gut/add-keyboard-shortcut scene {:key "Q" :modifier :ctrl} (Platform/exit))

    scene))
