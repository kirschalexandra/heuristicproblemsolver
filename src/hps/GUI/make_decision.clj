; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.GUI.make-decision
  (:require [hps.make-decision]
            [clojure.java.io :as io]
            [utilities.gui :as gut])
  (:use [clojure.pprint])
  (:import (javafx.event ActionEvent EventHandler)
           (javafx.beans.value ChangeListener ObservableValue ObservableStringValue)
           (javafx.util Callback)
           (javafx.scene.control Label Button CheckBox TitledPane TextField ChoiceBox
                                 TreeView TreeTableView TreeItem TreeTableColumn TreeTableColumn$CellDataFeatures
                                 TreeTableCell Tooltip Separator)
           (javafx.scene.control.cell CheckBoxTreeTableCell )
           (javafx.scene.layout VBox HBox GridPane Priority FlowPane)
           (java.util HashMap)
           (javafx.beans.property ReadOnlyStringWrapper ReadOnlyObjectWrapper)
           (javafx.scene.image Image ImageView)
           (javafx.geometry VPos HPos))
  (:load "make_decision/input" "make_decision/output"))


(defn sort-experts [experts]
  (sort (fn [e1 e2] (let [p1 (:priority e1)
                          p2 (:priority e2)]
                      (or (nil? p1) (nil? p2) (< p1 p2))))
        experts))


