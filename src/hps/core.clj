; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.core
  (:gen-class)
  (:require hps-test.examples.food))

(defn start-hps-gui []
  (let [producers  '(hps-test.examples.food/producer-pizza
                     hps-test.examples.food/producer-asia
                     hps-test.examples.food/producer-none)
        adaptors   '(hps-test.examples.food/adaptor-healthy
                     hps-test.examples.food/adaptor-more)
        evaluators '(hps-test.examples.food/evaluator-yummy
                     hps-test.examples.food/evaluator-no-pizza
                     hps-test.examples.food/evaluator-close)]
    (javafx.application.Application/launch hps.GUI.main
                                           (into-array String [(str {:producers producers :adaptors adaptors :evaluators evaluators})]))))






(defn -main [& args]
  (start-hps-gui))
  ; (let [{:keys [options arguments errors summary]} (parse-tool/parse-opts args morph-options)]
  ;   (cond
  ;     (:help options) (exit 0 (usage summary))
  ;     errors (exit 1 (error-msg errors))
  ;     :else (start-hps-gui))))
