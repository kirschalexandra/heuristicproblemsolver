; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.state)

(defmacro defsv [name & {:keys [history]}]
  (if history
    `(let [svval# (atom nil :meta {:name '~name :history (repeatedly ~history (fn [] nil))})]
      (def ~name svval#)
      (add-watch svval# (keyword (gensym))
        (fn [_# _# _# newval#]
          (alter-meta! svval# #(assoc % :history (cons newval# (butlast (:history %))))))))
    `(def ~name  (atom nil :meta {:name '~name}))))


(let [update-frequency (atom 60.0)
      update-frequency-ms (atom (/ @update-frequency 1000))]

  (defn set-update-frequency [freq]
    (reset! update-frequency freq)
    (reset! update-frequency-ms (/ freq 1000)))

  (defn convert-time-or-steps [delta-time delta-steps]
    (if (nil? delta-time)
      (if (nil? delta-steps)
        [(/ 1 @update-frequency-ms) 1] ; default: 1 Zeitschritt
        [(/ delta-steps @update-frequency-ms) delta-steps]) ; Umrechnung Zeitschritte in ms
      (do
        (when delta-steps
          (println "Function predict called with input for delta-time and delta-steps. Ignoring delta-steps, using delta-time"))
        [delta-time (* delta-time @update-frequency-ms)]))))

(defn defmodel [statevar command-type function]
  (alter-meta! statevar #(update-in % [:models] assoc command-type function)))

; predict+ kann weitere Informationen (z.B. Zwischenergebnisse) ausgeben, nicht nur die Vorhersage
(defn predict+ [statevar & {:keys [command initial-state delta-time delta-steps]
                            :or {initial-state @statevar}}]
  (if-let [prediction-fun (get (:models (meta statevar)) (first command))]
    (let [[dtime dsteps] (convert-time-or-steps delta-time delta-steps)]
      (prediction-fun initial-state (second command) dtime dsteps))
    (println "no matching model found for statevar " (:name (meta statevar)) " and command " command)))

; predict gibt nur die reine Vorhersage zurück
(defn predict [& args]
  (first (apply predict+ args)))


;; automatische Prüfung von Modellen momentan noch in MORPH, weil dazu viele Annahmen über Kommando-Anbindung und Speicherung von Zeitstempeln gemacht werden müssen
