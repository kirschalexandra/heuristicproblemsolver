; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.goals)



(defn set-goal [goal dm-spec]
                                        ;(println "set goal for " (:name dm-spec) " to: " goal)
  (reset! (:goal dm-spec) goal))

(defn remove-goal [dm-spec]
  (reset! (:goal dm-spec) nil))

(defn goal-achieved? [goalspec]
  (let [{statevars :statevars threshold :threshold closeness-function :closeness-function} (:type goalspec)]
    (< (apply closeness-function goalspec (vec (map deref statevars))) threshold)))


; reach-goal blockiert bis Ziel erreicht ist -> entspricht erstmal der Benennung, kann man von außen immer noch in future packen
; Name "reach-goal" gefällt mir noch nicht, sollte lieber sowas wie "add-goal" sein, d.h. man erweitert ein DM um eine Zielkomponente
; -> sollte sowas wie ein (Lego-)Baukasten werden
(defn monitor-goal [goalspec]
  (let [goal-reached-var (promise)
        {:keys [statevars threshold closeness-function]} (:type goalspec)
        watch-key (keyword (gensym "goal"))]
    (dorun
     (loop [svpos 0]
       (add-watch (nth statevars svpos) watch-key
                  (fn [_ _ _ newval]
                    (when (< (closeness-function goalspec newval) threshold)
                      (deliver goal-reached-var true))))
       (when (seq (rest statevars)) (recur (inc svpos)))))
    (future
      (deref goal-reached-var)
      (loop [svpos 0]
        (remove-watch (nth statevars svpos) watch-key)
        (when (seq (rest statevars)) (recur (inc svpos)))))
    goal-reached-var))
