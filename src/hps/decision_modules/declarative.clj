; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'hps.decision-modules)

;;; --------------------------------------------------------------------- ;;;
;;; default parameters                                                    ;;;
;;; --------------------------------------------------------------------- ;;;
(defn run-frequency-fun-by-time [time-in-ms]
  #(let [p (promise)]
     (future
       (Thread/sleep time-in-ms)
       (deliver p true))
     p))

(def default-decision-params
  {:max-iterations-per-step     3                           ; Anzahl Versuche bis bei step einfach die beste Option genommen wird, egal ob sie viel besser ist als die anderen
   :accept-best-alternative-fun lib/accept-any-alternative
   :relative-limit-accept       0.1                         ; soviel muss die beste Bewertung relativ über der zweitbesten liegen
   :accept-single-alternative   true                        ; wenn nur eine Alternative zur Verfügung steht, wird diese akzeptiert (sonst müssen in weiteren Runde mehr Alt. generiert werden)
   :default-decision            (map->DecisionResult {:decision nil :retained-alternatives () :rejected-alternatives ()})
   :run-frequency-fun           (run-frequency-fun-by-time 100)
   :evaluator-aggregation-fun   :weighted-sum               ; aggregation der Einzelergebnisse der evaluators
   ; [:weighted-sum :normalized[default]/:raw :use-weights[default]/:equal-weights]
   ; [:ranking :copeland/ :sequential-majority-comparison]
   ; [:scoring :borda/ :formula-one-championship]
   :merge-similar-alternatives? true
   })

;;; --------------------------------------------------------------------- ;;;
;;; declare decision modules                                              ;;;
;;; --------------------------------------------------------------------- ;;;

;;; store and retrieve dm specifications by name
(defn store-dm-declaration
  ([dm-name content] (store-dm-declaration dm-name content (ns-name *ns*)))
  ([dm-name content ns]
   (alter-meta! (find-ns ns) #(assoc-in % [:decision-modules dm-name] content))))

(defn update-dm-declaration [dm-name namespace fun & args]
  (let [current-content (dm-name (:decision-modules (meta namespace)))]
    (alter-meta!
      namespace
      #(assoc-in % [:decision-modules dm-name] (apply fun current-content args)))))

(defn list-decision-modules
  ([] (list-decision-modules (ns-name *ns*)))
  ([ns] (keys (:decision-modules (meta (find-ns ns))))))

(defn get-dm-from-name
  ([dm-name] (get-dm-from-name dm-name (ns-name *ns*)))
  ([dm-name ns] (let [dm-spec (dm-name (:decision-modules (meta (find-ns ns))))]
                  (or dm-spec
                      (log/warn "decision module " dm-name "not found in namespace " ns)))))

(defn store-dm-connection
  ([dm-from-name dm-to-name content] (store-dm-connection dm-from-name dm-to-name content *ns*))
  ([dm-from-name dm-to-name content namespace]
   (let [existing-connections (dm-to-name (dm-from-name (:dm-connections (meta namespace))))]
     (alter-meta!
       namespace
       #(assoc-in % [:dm-connections dm-from-name dm-to-name] (conj existing-connections content))))))

(defn list-dm-connections
  ([] (list-dm-connections *ns*))
  ([namespace]
   (into {} (map (fn [[from destinations]] [from (keys destinations)]) (:dm-connections (meta namespace))))))

(defn get-dm-connections
  ([from-name namespace]                                    ; wird nur intern aufgerufen, daher immer namespace übergeben
   (from-name (:dm-connections (meta namespace))))
  ([from-name namespace event-spec]
   (mapcat
     #(map
        (case event-spec
          :start first
          :stop second)
        %)
     (vals (get-dm-connections from-name namespace)))))

; make-goal hätte ich lieber in goals.clj, geht aber nicht wegen gegenseitigen Abhängigkeiten
(defn make-goal
  ([goaltype goal-params] (make-goal goaltype goal-params nil (ns-name *ns*)))
  ([goaltype goal-params parent-dm-name ns]
   (let [goal-content (assoc goal-params :type goaltype)]
     (map->Goal
       (if parent-dm-name
         (assoc goal-content :parent-goal (:goal (get-dm-from-name parent-dm-name ns)))
         goal-content)))))

;;; declare decision modules

(defn make-goal-to-goal-connections-code [{:keys [from to transform-fun] :as goal-to-goal-connections-spec} ns]
  [`(let [to-dm# (get-dm-from-name ~to '~ns)]
      (add-watch (:goal (get-dm-from-name ~from '~ns)) :goal-to-goal
                 (fn [_# _# _# newgoal#]
                   (set-goal (when newgoal# (make-goal (:goaltype to-dm#) (~transform-fun newgoal#))) to-dm#))))
   `(remove-watch (:goal (get-dm-from-name ~from '~ns)) :goal-to-goal)])

(defn make-decision-to-goal-connections-code [{:keys [from to transform-fun inherit-goal] :as decision-to-goal-connections-spec} ns]
  [`(let [to-dm# (get-dm-from-name ~to '~ns)]
      (add-watch (:executed-decision (get-dm-from-name ~from '~ns)) :executed-decision-to-goal
                 (fn [_# _# oldval# newval#]
                   ;(when-not (= oldval# newval#)
                   (when-let [command# (:value newval#)]    ; statt command könnte man hier auch weitere akzeptable alternativen verwenden -> konfigurierbar machen
                     (when (or (not (coll? command#)) (seq command#)) ; not empty, funktioniert nur, wenn vorher auf coll? getestet wurde!
                       (set-goal (make-goal (:goaltype to-dm#) (~transform-fun command#) ~@(when inherit-goal [from `'~ns])) to-dm#)))))) ;)
   `(remove-watch (:executed-decision (get-dm-from-name ~from '~ns)) :executed-decision-to-goal)])



(defn make-decision-to-alternatives-connections-code [decision-to-alternatives-connections-spec ns]
  (let [grouped-connection-statements (vals (group-by :to decision-to-alternatives-connections-spec))]
    (into {}
          (map #(vector (:to (first %))
                        `(do
                           ~@(map (fn [{:keys [from transform-fun filter-fun transfer-original-evaluation]}]
                                    `(let [decision# @(:executed-decision (get-dm-from-name ~from '~ns))
                                           retained-alternatives# (:retained-alternatives @(:internal-decision (get-dm-from-name ~from '~ns)))]
                                       (when decision#
                                         (map (fn [alt#]
                                                (assoc (~transform-fun alt#)
                                                  :produced-by [~from]
                                                  :evaluations
                                                  (when ~transfer-original-evaluation
                                                    (list {:evaluator ~from
                                                           :value     (when-let [old-eval# (:aggregated-evaluation alt#)]
                                                                        (* ~transfer-original-evaluation old-eval#))}))
                                                  :aggregated-evaluation nil))
                                              (conj (~filter-fun (sort-by :aggregated-evaluation > retained-alternatives#)) decision#)))))
                                  %)))
               grouped-connection-statements))))


(defn instantiate-experts [experts goal-atom]
  (let [ns *ns*]
    (into {}
          (map (fn [[experttype expertnames]]
                 [experttype
                  (map #(let [expert-instance (heur/get-expert ns experttype %)]
                          (if (:uses-goal expert-instance)
                            (update-in expert-instance [:fun] partial goal-atom)
                            expert-instance))
                       expertnames)])
               experts))))


(defn declare-dm [dm-name & {:as dm-spec}]
  (let [goal-atom (atom nil)
        dm (map->DecisionModule
             (merge dm-spec
                    {:internal-decision   (atom nil)
                     :executed-decision   (atom nil)
                     :goal                goal-atom
                     :sleeptime           15                ; Morse läuft auf 60 Hz, das kommt hiermit auch ungefähr raus
                     :decision-setup      (atom nil)
                     :decision-process    (atom nil)
                     :active              (atom false)
                     :name                dm-name
                     :available-experts   (instantiate-experts (:available-experts dm-spec) goal-atom)
                     :decision-parameters (merge default-decision-params (:decision-parameters dm-spec))
                     :configure-fun       (or (:configure-fun dm-spec) (fn [_] {:experts (:available-experts dm-spec)}))}))]
    (store-dm-declaration dm-name dm)
    dm))

(defn store-top-level-dm
  ([dm-name] (store-top-level-dm dm-name (ns-name *ns*)))
  ([dm-name ns] (alter-meta! (find-ns ns) #(update-in % [:top-level-dms] conj dm-name))))

(defn list-top-level-dms
  ([] (list-top-level-dms (ns-name *ns*)))
  ([ns] (:top-level-dms (meta (find-ns ns)))))

(defn declare-top-level-dm [dm-name & {:keys [goaltype] :as dm-spec}]
  (when (nil? goaltype)
    (log/warn "dm " dm-name " defined as top-level, but has not goaltype"))
  (store-top-level-dm dm-name)
  (apply declare-dm dm-name (apply concat (vec dm-spec))))


(defn declare-dm-connections [& {:keys [goal-to-goal decision-to-goal decision-to-alternatives]}]
  (let [ns (ns-name *ns*)]
    (doseq [{:keys [from to] :as conn} goal-to-goal]
      (store-dm-connection from to (make-goal-to-goal-connections-code conn ns)))
    (doseq [{:keys [from to] :as conn} decision-to-goal]
      (store-dm-connection from to (make-decision-to-goal-connections-code conn ns)))
    (doseq [[to code] (make-decision-to-alternatives-connections-code decision-to-alternatives ns)]
      (update-dm-declaration to *ns* #(assoc % :decision-to-alternatives-connections-code code)))))


;; Interface for connecting, stepping and running decision modules
(defn reset-decision-module [dm-spec]
  (reset! (:internal-decision dm-spec) nil)
  (reset! (:executed-decision dm-spec) nil)
  (reset! (:goal dm-spec) nil)
  (reset! (:decision-setup dm-spec) nil)
  (reset! (:decision-process dm-spec) nil)
  (reset! (:active dm-spec) false)
  ; reset-Funktionen von Experten aufrufen
  (doseq [reset-fun (remove nil? (map :reset-fun (mapcat val (:available-experts dm-spec))))]
    (reset-fun)))

(defn activate-connections [dm-names namespace]
  (doseq [dm dm-names]
    (eval `(do ~@(get-dm-connections dm namespace :start)))))

(defn deactivate-connections [dm-names namespace]
  (doseq [dm dm-names]
    (eval `(do ~@(get-dm-connections dm namespace :stop)))))


(defn start-dm [dm-name ns]
  (let [dm-spec (get-dm-from-name dm-name ns)]
    (reset-decision-module dm-spec)
    (reset! (:active dm-spec) true)
    (run-decision-module dm-spec)))

(defn stop-dm [dm-name ns]
  (let [dm (get-dm-from-name dm-name ns)]
    (reset! (:active dm) false)
    (remove-goal dm)))

(defn start-toplevel-dm
  ([dm-name] (start-toplevel-dm dm-name (ns-name *ns*)))
  ([dm-name ns]
   (let [dm (get-dm-from-name dm-name ns)
         dm-names (cons dm-name (:dm-dependencies dm))]
     (activate-connections dm-names (find-ns ns))
     (map #(start-dm % ns) dm-names))))

(defn stop-toplevel-dm
  ([dm-name] (stop-toplevel-dm dm-name (ns-name *ns*)))
  ([dm-name ns]
   (let [dm (get-dm-from-name dm-name ns)
         dm-names (cons dm-name (:dm-dependencies dm))]
     (run! #(stop-dm % ns) dm-names)
     (deactivate-connections dm-names (find-ns ns)))))


;;; --------------------------------------------------------------------- ;;;
;;; convenience functions                                                 ;;;
;;; --------------------------------------------------------------------- ;;;

(defn update-dm-config [dm-name ns-name config]
  (update-dm-declaration dm-name (find-ns ns-name)
                         (fn [current-dm & args]
                           (hps.types/map->DecisionModule
                             (assoc current-dm :configure-fun
                                               (fn [last-config]
                                                 (hps.types/map->DecisionSetup
                                                   (merge ((:configure-fun current-dm) last-config) ; Ergebnis der alten config-fun
                                                          config)))))))
  config)