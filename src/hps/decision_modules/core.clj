; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'hps.decision-modules)

;;; --------------------------------------------------------------------- ;;;
;;; reconfigure decision module                                           ;;;
;;; --------------------------------------------------------------------- ;;;

(declare get-dm-from-name)

(defn resolve-expert [expert-conf available-dm-experts]
  (let [[expert-name & expert-params] (mut/as-vector expert-conf)
        expert-instance (some #(when (= (:name %) expert-name) %) available-dm-experts)] ; TODO Fehlermeldung einbauen, falls Experte nicht in der Liste ist
    (if expert-params
      (apply update-in expert-instance [:parameters] assoc expert-params)
      expert-instance)))

(defn resolve-abstract-expert-conf [dm-experts {:keys [producers evaluators adaptors] :as expert-conf}]
  {:producers  (map #(map->Expert (resolve-expert % (:producers dm-experts))) producers) ; brauche ich das map->Expert?
   :evaluators (map #(map->Expert (resolve-expert % (:evaluators dm-experts))) evaluators)
   :adaptors   (map #(map->Expert (resolve-expert % (:adaptors dm-experts))) adaptors)})

(defn prepare-setup [abstract-conf dm-spec]
  (assoc abstract-conf
    :experts (resolve-abstract-expert-conf (:available-experts dm-spec) (:experts abstract-conf))
    :alternatives (concat (eval (:decision-to-alternatives-connections-code dm-spec))
                          (:alternatives abstract-conf))))

(defn configure-decision-module [dm-spec dm-params]
  (let [setup ((:configure-fun dm-spec) dm-spec)]
    (prepare-setup (assoc setup :parameters (merge dm-params (:parameters setup)))
                   dm-spec)))

(defn reconfigure-decision-module [dm-spec dm-params]
  (let [reconfigure-fun (or (:reconfigure-fun dm-spec) (:configure-fun dm-spec))]
    (let [new-setup (reconfigure-fun dm-spec)]
      (prepare-setup (assoc new-setup
                       :parameters (merge dm-params (:parameters new-setup))
                       :alternatives (map #(assoc % :evaluations nil :aggregated-evaluation nil) (:alternatives new-setup)))
                     dm-spec))))

;;; --------------------------------------------------------------------- ;;;
;;; decision module execution cycle                                       ;;;
;;; --------------------------------------------------------------------- ;;;

; step-decision-module:
; * ruft get-and-evaluate-alternatives auf
; * evtl. mehrmals mit verschiedenen Konfigs bis Entscheidung zufriedenstellend und Aufgabe
; * bei knapper Entscheidung: Zwischenspeicherung der Alternativen
; Achtung: internal-decision ist vom Typ DecisionResult, aber external-decision enthält nur die gewählte Option als DecisionAlternative
(defn step-decision-module [dm-spec]
  ;(log/info "step decision module " (:name dm-spec))
  (let [params (:decision-parameters dm-spec)]
    (reset! (:decision-setup dm-spec) (configure-decision-module dm-spec params))
    (loop [iteration 1]
      ;(log/info "setup (" (:name dm-spec) "):" @(:decision-setup dm-spec))
      (let [evaluated-alternatives (get-and-evaluate-alternatives @(:decision-setup dm-spec))
            {rejected true retained false} (group-by #(nil? (:aggregated-evaluation %)) evaluated-alternatives)
            [best-alternative & other-retained-alternatives :as sorted-retained-alternatives] (sort-by :aggregated-evaluation > retained)
            accept-best? ((:accept-best-alternative-fun params) best-alternative other-retained-alternatives params)
            [accept-alternative? decision retained-alts]
            (cond (or (and accept-best? best-alternative)
                      (and (>= iteration (:max-iterations-per-step params)) (not-empty retained)))
                  [true best-alternative other-retained-alternatives]

                  (>= iteration (:max-iterations-per-step params)) ; (empty? retained))
                  [true (:default-decision params) []]

                  :else
                  [false nil sorted-retained-alternatives])]

        (reset! (:internal-decision dm-spec)
                (map->DecisionResult
                  {:decision              decision
                   :retained-alternatives retained-alts
                   :rejected-alternatives rejected}))

        (if accept-alternative?
          (reset! (:executed-decision dm-spec) decision)
          (do (reset! (:decision-setup dm-spec) (reconfigure-decision-module dm-spec params))
              (recur (inc iteration))))))))


; run-decision-module
(defn run-decision-module [dm-spec]
  (let [decision-process (future
                           (try
                             (loop [wait-promise ((:run-frequency-fun (:decision-parameters dm-spec)))]
                               (step-decision-module dm-spec)
                               (deref wait-promise)
                               (when (deref (:active dm-spec))
                                 (recur ((:run-frequency-fun (:decision-parameters dm-spec))))))
                             (catch Exception e
                               (let [last-decision @(:internal-decision dm-spec)]
                                 (log/error (type e) "in dm " (:name dm-spec) "\n internal decision in dm: \n decision: "
                                            (:value (:decision last-decision)) " evaluations: " (vals (:evaluations (:decision last-decision)))
                                            " (" (:aggregated-evaluation (:decision last-decision)) ")" e)))))]
    (reset! (:decision-process dm-spec) decision-process)
    dm-spec))


