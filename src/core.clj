; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns core
  (:gen-class)
  (:require [clojure.string :as string]
            [clojure.tools.cli :as parse-tool]
            [hps.GUI.main :refer :all]
            [robot.core :refer :all]
            [tsp.core :refer :all]
            [remind.core :as remind]
            [hps-test.gui]
            [robot-test.navigation]
            [clojure.tools.logging :as log]))

;; main method with command line arguments


(def options
  {:general [["-h" "--help"]]
   :morph   [[nil "--start-sim"             "Start Morse simulator? If false, Morse should be already running"
              :default false]
                                        ; Option -m ist anscheinend vorbesetzt, daher statt start-morse auf start-sim gewechselt
             [nil "--start-control-gui"     "Start the control GUI?"
              :default false]
             [nil "--start-hps-gui"         "Start the decision GUI?"
              :default false]
             [nil "--start-robot"           "Start robot program? Make sure Morse is running or started here"
              :default false]
             [nil "--avatars AVT"           "Provide list of avatar names to initialise control connection for them"
              :parse-fn read-string]
             [nil "--morse-script-file MSF" "Specify simulated world by providing morse script file (in path MORSE_SCRIPTS)"
              :parse-fn str]
             [nil "--robot-start-pose RSP"  "Define robot start pose (make sure your script file specifies origin as robot pose)"
              :parse-fn read-string]]
   :tsp     [["-f" "--file" "TSP File"]
             ["-s" "--stacktracking" "Solve with stacktracking"
              :default false]
             ["-b" "--backtracking" "Solve with backtracking - might take some time."
              :default false]
             ["-g" "--gui" "Start with GUI"
              :default false]
             ["-v" "--validate" "Validate parameters"
              :default false]
             ["-n" "--ntimes" "Run n times"]
             ["-w" "--writesol" "Write result to solution file"
              :default false]]
   :remind []})


(defn general-usage []
  (string/join \newline
               ["This is the heuristic problem solver. To run it choose one of the implemented applications."
                ""
                "Usage: <start> [options] <application>"
                ""
                "Possibilities for <start>:"
                "lein run"
                "java -jar morph-clojure-1.0.0-SNAPSHOT-standalone.jar"
                ""
                "Possibilities for <application>:"
                "morph"
                "tsp"
                ""
                "Options: each application supports different options, for a list of options call"
                "core -h application"
                ""]))


(defn usage [options-summary]
  (string/join \newline ["Options:" options-summary]))


(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))


(defn morph-main [args]
  (let [{:keys [options arguments errors summary]} (parse-tool/parse-opts args (:morph options))]
    (cond
      (:help options) (exit 0 (usage summary))
      errors (exit 1 (error-msg errors))
      :else (apply start-hps-robot (mapcat seq options)))))
 ;(mapcat seq options) scheint die eleganteste Methode zu sein eine hashmap als Keyword-Argument zu übergeben...


(defn tsp-main [args]
  (let [{:keys [options arguments errors summary]} (parse-tool/parse-opts args (:tsp options))]
    (println "tsp options: " options)
    (println "tsp args: " arguments)
    (println "tsp errors: " errors)
    (cond
      (:help options) (exit 0 (usage summary))
      errors (exit 1 (error-msg errors))
      (:gui options) (start-tsp-gui)
      ;(:stacktracking options) (apply start-with-stacktracking arguments)
      ;(:backtracking options) (start-parameter-fitting arguments)
      ;(:validate options) (validate-parameters arguments)
      :else (apply start-without-gui arguments))))


(defn remind-main [args]
  (remind/remind-start))

(defn start-hps-application [app args]
  (let [{:keys [options arguments errors summary]} (parse-tool/parse-opts args (get options app))]
    (case app
      :morph (morph-main args)
      :tsp   (tsp-main args)
      :remind   (remind-main args)
      (exit 0 (error-msg "Unknown application " app)))))


(defn -main [& args]
  (start-hps-gui))
                                        ;(let [{:keys [options arguments errors summary]} (parse-tool/parse-opts args (mapcat val options))]
                                        ;(println "options: " options)
                                        ;(println "args: " arguments)
                                        ;(println "errors: " errors)
                                        ;(cond errors (exit 1 (error-msg errors))
                                        ;(not-empty arguments) (start-hps-application (keyword (last arguments)) args)
                                        ;:else (exit 0 (general-usage)))))

(.addShutdownHook (Runtime/getRuntime)
                  (Thread. (fn [] (log/info "============= EXIT HPS =============="))))
