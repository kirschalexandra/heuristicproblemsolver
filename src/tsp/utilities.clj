; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.utilities
  (:require [clojure.data]
            [utilities.misc :as utmisc :refer [in?]]
            [utilities.math :as utmath]))


(defrecord TSPNode [id x y])

(defn match->node
  [matches]
  (->> (rest (first matches))
       (map #(Integer/parseInt %))
       (apply ->TSPNode)))

(defn euclidean-distance
  [node1 node2]
  (Math/hypot (- (:x node1) (:x node2)) (- (:y node1) (:y node2))))

(defn neighbour-distances
  [node neighbours]
  (mapv #(euclidean-distance node %1)
        neighbours))


(defn distance-map
  "Takes a vector of nodes and calculates the nxn distance vector."
  [nodes]
  (mapv #(neighbour-distances %1 nodes) nodes))



(defn accumulated-distance
  "Accumulates the distances of a node to the nodes in the given list"
  [node neighbours]
  (reduce + 0 (neighbour-distances node neighbours)))


(defn indexes-of [e coll] (keep-indexed #(if (= e %2) %1) coll))

(defn index-of-min
  "Returns a list of the indices of the smallest value in the collection."
  [coll]
  (let [minimum (apply min coll)]
    (indexes-of minimum coll)))

(defn index-of-max
  "Returns a list of the indices of the greatest value in the collection"
  [coll]
  (let [maximum (apply max coll)]
    (indexes-of maximum coll)))


(defn nearest-neighbour
  "Returns the nearest neighbour in neighbours to node"
  [node neighbourhood]
  (let [neighbours (vec neighbourhood)
        dists (neighbour-distances node neighbours)
        idx (index-of-min dists)
        result (map #(get neighbours %) idx)]
    (first result)))

(defn nearest-k-neighbours
  "Returns the k nearest neighbours in neighbours to node."
  [node neighbours k]
  (loop [n k
         result []
         remaining neighbours]
    (if (or (zero? n) (empty? remaining))
      (apply list result) ;Conversion to list because hsp produce-alternatives does not allow vectors
      (let [addition  (nearest-neighbour node remaining)]
        (recur
         (dec n)
         (conj result addition)
         (filterv #(not= addition %) remaining))))))


(defn angle-between-nodes
  "Takes 3 nodes and calculates the angle at point 2: 1---2---3 = 180°"
  [n1 n2 n3]
  (let [minusv #(vector (- (:x %1) (:x %2)) (- (:y %1) (:y %2)))
        v1 (minusv n2 n1)
        v2 (minusv n2 n3 )
        dot (utmath/dot-product v1 v2)
        l1 (utmath/vector-length v1)
        l2 (utmath/vector-length v2)]
    {:node n2 :angle (Math/toDegrees (Math/acos (/ dot (* l1 l2))))}))


(defn angles-between-nodes
  "Takes a collection of nodes and calculates the angle for each point and its
  predecessor and successor: o---n---s = 180°"
  [nodes]
  (let [shifted-right (cons (last nodes) (drop-last nodes))
        shifted-left (concat (rest nodes) (list (first nodes)))]
    (apply map angle-between-nodes (vector shifted-left nodes shifted-right))))

(defn convex-hull
  "Taken from https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
  and modified to work with TSPNodes"
  [c]
  (let [cc  (sort-by (juxt :x :y) c)
        ccw #(- (* (- (:x %2) (:x %1)) (- (:y %3) (:y %1)))
                (* (- (:y %2) (:y %1)) (- (:x %3) (:x %1))))
        half-hull  #(loop [h  [] ; returns the upper half-hull of the collection of vectors
                           c  %]
                      (if (seq c)
                        (if (and
                             (> (count h) 1)
                             (<= 0 (ccw (h (- (count h) 2))
                                        (h (dec (count h))) (first c))))
                          (recur (vec (butlast h)) c)
                          (recur (conj h (first c)) (rest c)))
                        h))
        upper-hull  (butlast (half-hull cc))
        lower-hull  (butlast (half-hull (reverse cc)))]
    (vec (concat upper-hull lower-hull ))))


(defn in-bounds?
  "Checks, if a point q is within the bounds defined by points p and r"
  [p q r]
  (let [px (:x p) py (:y p)
        qx (:x q) qy (:y q)
        rx (:x r) ry (:y r)]
    (and (<= qx (max px rx))
         (>= qx (min px rx))
         (<= qy (max py ry))
         (>= qy (min py ry)))))

(defn orientation
  "Returns one of three possible orientations for three points:
  colinear = 0, clockwise = 1, counterclockwise = 2"
  [p q r]
  (let [px (:x p) py (:y p)
        qx (:x q) qy (:y q)
        rx (:x r) ry (:y r)
        orient (- (* (- qy py)
                     (- rx qx))
                  (* (- qx px)
                     (- ry qy)))]
    (cond
      (zero? orient) 0
      (pos? orient) 1
      (neg? orient) 2)))

(defn intersect?
  "Checks if two lines, defined by their start(p) and end points(q), intersect."
  [p1 q1 p2 q2]
  (let [o1 (orientation p1 q1 p2)
        o2 (orientation p1 q1 q2)
        o3 (orientation p2 q2 p1)
        o4 (orientation p2 q2 q1)]
    (and
     (not= q1 p2)
     (not= q2 p1)
     (or (not (or (= o1 o2) (= o3 o4)))
         (and (zero? o1) (in-bounds? p1 p2 q1))
         (and (zero? o2) (in-bounds? p1 q2 q1))
         (and (zero? o3) (in-bounds? p2 p1 q2))
         (and (zero? o4) (in-bounds? p2 q1 q2))))))

(defn pairwise-intersections?
  "Take two points forming line1 and a vector of points representing a set
  of other lines and checks, if line1 intersects with one or more of
  the lines in the set"
  [p1 q1 points]
  (loop [remaining (vec points)]
    (if (< (count remaining) 2)
      false
      (or (intersect? p1 q1 (first remaining) (second remaining))
          (recur (rest remaining))))))

(defn has-intersections?
  "Checks if the solution contains line intersections.
  'solution is an ordered sequence of points. Lines exist between a point
  and its successor in the list."
  [solution]
  (loop [p1 (first solution)
         q1 (second solution)
         remaining (rest solution)]
    (if (< (count remaining) 2)
      false
      (or (pairwise-intersections? p1 q1 remaining)
          (recur q1 (first remaining) (rest remaining))))))

(defn drop-until
  [x coll]
  (if (or (empty? coll) (= x (first coll)))
    coll
    (drop-until x (rest coll))))

(defn take-until
  "Collects items in a sequence until the item x occurs in the sequence.
  x is included in the resulting collection."
  [x coll]
  (loop [acc []
         remaining coll]
    (if (or (empty? remaining) (= x (first remaining)))
      (conj acc (first remaining))
      (recur (conj acc (first remaining)) (rest remaining)))))



(defn center-of-mass
  [nodes]
  (let [n (count nodes)
        xsum (apply + (map :x nodes))
        ysum (apply + (map :y nodes))]
    [(/ xsum n) (/ ysum n)]))

(defn regions-rand
  "Take a collection of nodes and assigns them to k regions, where k is the
  number of nodes on the convex hull. The nodes are assigned by proximity to
  the center of mass of the regions."
  [nodes]
  (let [ch  (convex-hull nodes)
        initial (take (count ch) (shuffle nodes))]
    (letfn [(sort-by-hull [regs]
              (loop [hull ch
                     acc []]
                (if (empty? hull) acc
                    (recur (rest hull)
                           (conj acc (first (filter #(in? % (first hull)) regs)))))))]
      (loop [remaining (vec (get (clojure.data/diff (set nodes) (set initial))0))
             regions (vec (map list initial))]
        (if (empty? remaining)
          (sort-by-hull regions)
          (let [coms (map #(->TSPNode -1 (get % 0) (get % 1)) (map center-of-mass regions))
                nns (vec (map #(nearest-neighbour % remaining) coms))
                dists (map euclidean-distance coms nns)
                idx (first (index-of-min dists))
                winner (get regions idx)]
            (recur (vec (utmisc/without remaining (get nns idx)))
                   (vec (concat (conj (take idx regions)
                                      (conj winner (get nns idx)))
                                (rest (drop-until winner regions)))))))))))



(defn regions
  "Deterministic version of regions. Initial clusters are always the convex
  hull points"
  [nodes]
  (let [ch  (convex-hull nodes)]
    (letfn [(sort-by-hull [regs]
              (loop [hull ch
                     acc []]
                (if (empty? hull) acc
                    (recur (rest hull)
                           (conj acc (first (filter #(in? % (first hull)) regs)))))))]
      (loop [remaining (vec (get (clojure.data/diff (set nodes) (set ch))0))
             regions (vec (map list ch))]
        (if (empty? remaining)
          (sort-by-hull regions)
          (let [coms (map #(->TSPNode -1 (get % 0) (get % 1)) (map center-of-mass regions))
                nns (vec (map #(nearest-neighbour % remaining) coms))
                dists (map euclidean-distance coms nns)
                idx (first (index-of-min dists))
                winner (get regions idx)]
            (recur (vec (utmisc/without remaining (get nns idx)))
                   (vec (concat (conj (take idx regions)
                                      (conj winner (get nns idx)))
                                (rest (drop-until winner regions)))))))))))


(def m-regions
  ;;regions is relatively expensive and the nodes parameter is unlikely to change
  ;;often within a problem, so it's acceptable to store a memoized version of
  ;;the function.
  (memoize regions))
