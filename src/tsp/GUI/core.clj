; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.GUI.core
  (:require [clojure.java.io :as io]
            [hps.GUI.make-decision :as hpsgui]
            hps.GUI.structure
            [hps.decision-modules :as dms]
            [hps.types :refer :all]
            tsp.GUI.grid
            tsp.GUI.menu
            [tsp.state :as state]
            [utilities.gui :as gut]
            [tsp.make-decision :as tmd]
            [clojure.tools.logging :as log])
  (:import javafx.scene.control.Button
           javafx.scene.layout.HBox))

; TODO die folgenden Funktionen sind auch in test/gui.clj -> in HPS oder HPS-Tools umziehen?
; initialize-expert-display aus morph_gui/hps/decision_process/displays.clj geholt -> in HPS umziehen?
(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dms/get-dm-from-name dm-keyword 'tsp.pathfinding))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
    (run! (fn [[pane experts]]
            (hpsgui/initialize-experts-in-display pane experts)
            (.setSelected (some #(when (= (.getText %) "All") %) (.getChildren (.getContent pane))) true))
          [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])))


(defn read-configuration-fun [dm-pane]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems dm-pane))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))
        read-out-experts-fun (fn [pane]
                               (remove #(= % :All) (mapcat #(when (.isSelected %) (list (keyword (.getText %)))) (.getChildren (.getContent pane)))))]
    (fn [_]
      (map->DecisionSetup {:experts    {:producers  (read-out-experts-fun producer-pane)
                                        :evaluators (read-out-experts-fun evaluator-pane)}
                           :parameters (hpsgui/read-parameters-from-grid (.getContent parameters-pane))}))))

(defn solve-problem [dm-pane dm-object dm-step-fun]
  (let [[producer-names evaluator-names adaptor-names] (hpsgui/get-setup-data dm-pane)]
    (log/info (str "Solve problem with producers: " (apply str producer-names) ", evaluators: " (apply str evaluator-names)
                   ", adaptors: " (apply str adaptor-names)))
    (cond (empty? @state/problemdata)
          (gut/show-alert-message "Please load a problem.")

          (or (empty? producer-names) (empty? evaluator-names))
          (gut/show-alert-message "Please choose at least one producer and one evaluator.")

          :else
          (do
            (tsp.state/throw-away-future)
            (while (<= (count @tsp.state/solution)
                       (count @tsp.state/problemdata))
              (dm-step-fun dm-object)
              (hpsgui/update-decision-display @(:internal-decision dm-object) dm-pane)
              ;;TODO: das argument für apply-decision sollte eigentlich äquivalent zu (:internal-decision), sein, allerdings
              ;; steht da bei abruf immer nil in :decision... keine ahnung warum das für update-decision-display funktioniert.
              (state/apply-decision {:decision              @(:executed-decision dm-object)
                                     :retained-alternatives (:retained-alternatives @(:internal-decision dm-object))
                                     :rejected-alternatives (:rejected-alternatives @(:internal-decision dm-object))})
              )))))

(defn make-control-elements [dm-pane]
  (let [run-button-box (HBox.)
        button-reset (Button. "Reset")
        button-step-fw (Button. "->")
        button-step-bw (Button. "<-")
        button-run (Button. "Solve")
        button-run-back (Button. "Solve with Backtracking")
        original-tsp-dm (dms/get-dm-from-name :tsp 'tsp.pathfinding)
        dm-object (assoc original-tsp-dm
                    :configure-fun (read-configuration-fun dm-pane))]
    (gut/add-to-container run-button-box button-reset button-step-bw button-run
                          button-run-back button-step-fw)
    (.add (.getStyleClass run-button-box) "control-buttons-hbox")

    (gut/set-button-action
      button-step-fw
      (do
        (when (and (empty? @state/decisions) (empty? @state/future-decisions))
          (.fire button-run)
          (while (> (count @state/decisions) 0)
            (.fire button-step-bw)))
        (state/redo-decision)
        (hpsgui/update-decision-display (last @state/decisions) dm-pane)))


    (gut/set-button-action
      button-run
      (solve-problem dm-pane dm-object dms/step-decision-module))


    (gut/set-button-action
      button-run-back
      (solve-problem dm-pane dm-object tmd/step-decision-module-with-stacktracking))


    (gut/set-button-action
      button-step-bw
      (do
        (state/revert-decision)
        (hpsgui/update-decision-display (last @state/decisions) dm-pane)))

    (gut/set-button-action
      button-reset
      (do
        (reset! state/unused-nodes @state/problemdata)
        (reset! state/solution [])
        (reset! state/decisions [])
        (reset! state/future-decisions [])
        (reset! state/uncertainties [])
        (reset! state/uncertainty-history [])))

    run-button-box))


(defn adjust-gui-for-tsp [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/tsp.css")))

  (tsp.GUI.menu/adjust-menu-for-tsp (.lookup scene "#hps-menubar-left") scene)

  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        dm-pane (hps.GUI.structure/make-decision-module-basic-display)]

    (gut/add-to-container decisionModules dm-pane)
    (gut/add-to-container stateRegion (tsp.GUI.grid/make-grid-screen))
    (gut/add-to-container controlRegion (make-control-elements dm-pane))
    (initialize-expert-display dm-pane :tsp)))


; always add "TSP" mode
(hps.GUI.structure/register-hps-mode "TSP" adjust-gui-for-tsp false)
