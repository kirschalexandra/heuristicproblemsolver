; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.GUI.menu
  (:require [tsp.io :as io]
            [utilities.gui :as gut])
  (:import javafx.stage.FileChooser))

(defn adjust-menu-for-tsp [menubar scene]
  (let [menufile (first (filter #(= (.getText %) "File") (.getMenus menubar)))
        file-chooser (FileChooser. )
        stage (.getWindow scene)]

    (gut/create-menu-item menufile
                          "Open TSP File"
                          {:key "O" :modifier :ctrl}
                          (fn [] (do (io/open-tsp-file (.showOpenDialog file-chooser stage))
                                     (gut/adjust-stage-size (.getWindow scene)))))))


