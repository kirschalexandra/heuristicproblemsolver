; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.GUI.grid
  (:require tsp.rating
            tsp.state
            tsp.utilities
            [utilities.gui :as gut])
  (:import javafx.geometry.VPos
           javafx.scene.canvas.Canvas
           javafx.scene.control.Label
           [javafx.scene.layout GridPane StackPane VBox]
           javafx.scene.paint.Color))

;;Canvas size definition - for now fixed to these values.
(def width 500)
(def height 500)


(def colors [Color/BLUE Color/GREEN Color/RED Color/YELLOW Color/PINK
             Color/ORANGE Color/CYAN Color/MAGENTA Color/BROWN
             Color/HONEYDEW Color/PLUM])


(defn get-scaling
  [nodes]
  (let [max-x (:x (apply max-key :x nodes))
        max-y (:y (apply max-key :y nodes))]
    (vector (/ (- width 50) max-x) (/ (- height 50) max-y))))


(defn scaled-node-coordinates
  "Returns the node coordinates scaled to fit in the canvas space.
   To prevent border nodes from disappearing the canvas space is padded."
  [nodes]
  (let [[sx sy] (get-scaling @tsp.state/problemdata)]
    (map #(vector (* (:x %) sx) (* (:y %) sy) (:id %)) nodes)))

(defn draw-nodes
  "Takes a collection of nodes and draws them onto the canvas.
   Node coordinats are scaled to use the complete canvas area."
  [gc color nodes]
  (when (seq nodes)
    (let [scaled (scaled-node-coordinates nodes)]
      (doseq [[x y id] scaled]
        (doto gc
          (.setFill color)
          (.fillOval (+ 15 x) (+ 15 y) 10 10)
          (.setTextBaseline (VPos/TOP))
          (.fillText (str id) x (+ 15 y)))))))

(defn draw-solution
  "Draws lines between the specified collection of nodes.
  A scaling vector [x y] must be provided to draw to the correct coordinates."
  [gc nodes]
  (let [[sx sy] (get-scaling @tsp.state/problemdata)
        scaled (map #(vector (* (:x %) sx) (* (:y %) sy)) nodes)
        start (first scaled)]
    (.beginPath gc)
    (.moveTo gc (+ (first start) 20) (+ (second start) 20))
    (.setLineWidth gc 2.0)
    (doseq [[x y] scaled]
      (doto gc
        (.lineTo (+ 20 x) (+ 20 y))
        (.stroke)))
    (.closePath gc)))

(defn draw-region
  [gc nodes colorn]
  (let [col (get colors colorn)
        [sx sy] (get-scaling @tsp.state/problemdata)
        scaled (map #(vector (* (:x %) sx) (* (:y %) sy)) nodes)
        scaled-hull (map #(vector (+ 20 (* (:x %) sx))
                                  (+ 20 (* (:y %) sy)))
                         (tsp.utilities/convex-hull nodes))
        start (first scaled)
        scnd (second scaled)]
    (.setStroke gc col)
    (.setLineDashOffset gc 1.0)
    (cond
      (= 1 (count nodes)) (.strokeOval gc (+ 10 (first start))
                                       (+ 10 (second start)) 20 20)
      (= 2 (count nodes)) (.strokeRect gc
                                       (+ 10 (min (first start) (first scnd)))
                                       (+ 10 (min (second start) (second scnd)))
                                       (+ 20 (Math/abs (float (- (first start) (first scnd)))))
                                       (+ 20 (Math/abs (float (- (second start) (second scnd))))))
      :else (.strokePolygon
              gc
              (double-array (map first scaled-hull))
              (double-array (map second scaled-hull))
              (count scaled-hull)))))

(defn draw-regions
  [gc nodes]
  (let [reg (tsp.utilities/regions nodes)]
    (doseq [[i r] (map-indexed vector reg)]
      (draw-region gc r i))))

(defn clear-canvas
  "Delete all content in the canvas"
  [gc]
  (.clearRect gc 0 0 width height))



(defn make-state-box
  []
  (let [wrapper (GridPane.)
        pathlabel (Label. "Path: ") path (Label. "[1 2 3 4]")
        paolabel (Label. "PAO: ") pao (Label. "-")
        ratinglabel (Label. "Rating: ") rating (Label. "-")
        humtourlabel (Label. "Human Tour exists: ") humtour (Label. "-")]
    (add-watch tsp.state/solution :pathlabel
               (fn [key atom old-state new-state]
                 (.setText path (clojure.string/join ", " (map :id (deref tsp.state/solution))))))
    (add-watch tsp.state/solution :rating
               (fn [key atom old-state new-state]
                 (if (and (> (count (deref tsp.state/solution)) (count (deref tsp.state/problemdata)))
                          (not-empty (deref tsp.state/ratingdata)))
                   (do
                     (.setText pao (format "%.3f" (tsp.rating/calculate-pao @tsp.state/solution)))
                     (.setText rating (str (tsp.rating/rate-tour (deref tsp.state/solution))
                                           "\t | Human:"
                                           (utilities.math/round2 (:minRating (deref tsp.state/ratingdata)))
                                           ", "
                                           (utilities.math/round2 (:medianRating (deref tsp.state/ratingdata)))
                                           ","
                                           (utilities.math/round2 (:maxRating (deref tsp.state/ratingdata)))))
                     (.setText humtour (str (tsp.rating/human-tour-exists (deref tsp.state/solution)) "/"
                                            (apply + (vals (:solutions @tsp.state/ratingdata))))))
                   (run! #(.setText % "-") [pao rating humtour]))))
    (loop [row 0
           [[label content] & other] [[pathlabel path] [paolabel pao] [ratinglabel rating] [humtourlabel humtour]]]
      (.add wrapper label 0 row)
      (.add wrapper content 1 row)
      (when other
        (recur (inc row) other)))
    wrapper))



(defn make-grid-screen
  []
  (let [wrapper-box (VBox.)
        inner-container (StackPane.)
        node-layer (Canvas. width height)
        solution-layer (Canvas. width height)
        decision-layer (Canvas. width height)
        regions-layer (Canvas. width height)
        gc (.getGraphicsContext2D node-layer)
        sgc (.getGraphicsContext2D solution-layer)
        dgc (.getGraphicsContext2D decision-layer)
        rgc (.getGraphicsContext2D regions-layer)]
    (.add (.getStyleClass inner-container) "tsp-canvas")
    (.add (.getStyleClass wrapper-box) "tsp-canvas-wrapper")

    ;;Add the watcher to the problemdata state, so we can
    ;;redraw the node-layerwhen the data changes
    (add-watch tsp.state/problemdata :watcher
               (fn [key atom old-state new-state]
                 (clear-canvas gc)
                 (clear-canvas rgc)
                 (draw-nodes gc Color/GRAY new-state)
                 (draw-regions rgc new-state)))
    (add-watch tsp.state/solution :watcher
               (fn [key atom old-state new-state]
                 (do
                   (clear-canvas sgc)
                   (when (> (count new-state) 1)
                     (draw-solution sgc new-state)))))
    (add-watch tsp.state/decisions :watcher
               (fn [key atom old-state new-state]
                 (let [decision (last new-state)
                       accepted (:value (:decision decision))
                       retained (vec (map :value (:retained-alternatives decision)))
                       rejected (vec (map :value (:rejected-alternatives decision)))]
                   (clear-canvas dgc)
                   (draw-nodes dgc Color/RED rejected)
                   (draw-nodes dgc Color/ORANGE retained)
                   (when-not (nil? accepted)
                     (draw-nodes dgc Color/GREEN (list accepted))))))
    (gut/add-to-container inner-container
                          regions-layer
                          solution-layer
                          node-layer
                          decision-layer)
    (gut/add-to-container wrapper-box inner-container)
    (gut/add-to-container wrapper-box (make-state-box))
    wrapper-box))


