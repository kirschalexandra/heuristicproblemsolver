; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.parameter-fitting
  (:require [clojure.math.combinatorics :as combo]
            [clojure.data.json :as json]
            [utilities.math :as mut]
            [hps.decision-modules :as dms]
            [hps.parameter-fitting :as pf]
            hps.io
            [hps.types :as types]
            tsp.io
            [tsp.rating :as rating]
            [tsp.state :as state]
            [hps.library :as lib]))
;[tsp.pathfinding :as tsppf]))

(defn print-progress-bar [percent]
  (let [bar (StringBuilder. "[")]
    (doseq [i (range 50)]
      (cond (< i (int (/ percent 2))) (.append bar "=")
            (= i (int (/ percent 2))) (.append bar ">")
            :else (.append bar " ")))
    (.append bar (str "] " percent "%     "))
    (print "\r" (str bar))
    (flush)))

;(defn set-weights
;  [evaluators paramdata]
;  (let [weight-list (:params paramdata)
;        nevaluators (map #(assoc % :weight (get weight-list
;                                                (:name %))) evaluators)]
;    nevaluators))
;
;(defn randomize-weights
;  [evaluators]
;  (let [n (count evaluators)
;        ;; weights (take n (repeatedly #(rand-int 11)))
;        weights (repeatedly n #(rand-int 2))
;        maxweight (apply max weights)
;        ;; normweights (map #(/ % maxweight) weights)
;        normweights weights]
;    ;; (println normweights)
;    (map #(assoc %1 :weight %2) evaluators normweights)))
;
;
;
;(defn produce-tour
;  [file producers adaptors evaluators]
;  (loop []
;    (let [decision (tsp.make-decision/make-decision {:producers  producers
;                                                     :adaptors   adaptors
;                                                     :evaluators evaluators})]
;      (println (count @tsp.state/solution))
;      (if (or (> (count @tsp.state/solution) (count @tsp.state/problemdata))
;              (nil? (:decision decision)))
;        @tsp.state/solution
;        (do
;          (tsp.state/apply-decision decision)
;          (recur))))))
;
;(defn make-n-weights
;  [items n]
;  (combo/selections items n))
;
;(defn start-fitting
;  [file producers adaptors evaluators-raw iterations iterations2]
;  (tsp.io/open-tsp-file file)                               ;; Load the files once
;  (let [median-rating (:medianRating @tsp.state/ratingdata)
;        problemdata @tsp.state/problemdata
;        ratingdata @tsp.state/ratingdata
;        initial-weights (make-n-weights [0 1] (count evaluators-raw))]
;    (loop [n iterations
;           weights initial-weights
;           good-params []]
;      (let [evaluators (map #(assoc %1 :weight %2) evaluators-raw (first weights))]
;        (tsp.state/reset-all problemdata)
;        (tsp.state/set-ratingdata ratingdata)
;        (print-progress-bar (float (* 100 (- 1 (/ n iterations)))))
;        (if (< n 1)
;          good-params
;          (recur (dec n)
;                 (rest weights)
;                 (vec (concat good-params
;                              (loop [tries iterations2
;                                     result []]
;                                (if (< tries 1)
;                                  result
;                                  (let [tour-params (zipmap (map :name evaluators)
;                                                            (map :weight evaluators))
;                                        ;; tour (produce-tour file producers adaptors evaluators)
;                                        tour (tsp.make-decision/make-decision-with-stacktracking {:producers  producers
;                                                                                                  :evaluators evaluators})
;                                        tour-rating (rating/rate-tour tour)]
;                                    (if (or (nil? tour-rating)
;                                            (< (Math/abs (/ tour-rating median-rating)) 0.95))
;                                      (recur (dec tries) result)
;                                      (recur (dec tries) (conj result {:rating tour-rating
;                                                                       :params tour-params}))))))))))))))
;
;
;
;(defn validate
;  [parameterlist problem producers adaptors evaluators-raw iterations]
;  (tsp.io/open-tsp-file problem)
;  (let [problemdata @tsp.state/problemdata
;        ratingdata @tsp.state/ratingdata
;        total (count parameterlist)]
;    (println (str "Read " (count parameterlist) " Parameters"))
;    (loop [parameters parameterlist
;           good-params []
;           n 0]
;      (when (zero? total) (println good-params))
;      (print-progress-bar (float (* 100 (/ n (or total 1)))))
;      (tsp.state/reset-all problemdata)
;      (tsp.state/set-ratingdata ratingdata)
;      (if (empty? parameters)
;        good-params
;        (let [evaluators (set-weights evaluators-raw (first parameters))
;              ;; tour (produce-tour problem producers adaptors evaluators)
;              tour (tsp.make-decision/make-decision-with-stacktracking {:producers  producers
;                                                                        :evaluators evaluators})
;              tour-rating (tsp.rating/rate-tour tour)]
;          (if (or (nil? tour-rating)
;                  (< (/ tour-rating (:medianRating ratingdata)) 0.9))
;            (recur (rest parameters) good-params (inc n))
;            (recur (rest parameters) (conj good-params (first parameters)) (inc n))))))))
;
;
;
;
;(defn find-best-parameter
;  [file producers adaptors evaluators-raw iterations]
;  (tsp.io/open-tsp-file file)                               ;; Load the files once
;  (let [maxrating (:maxRating @tsp.state/ratingdata)
;        problemdata @tsp.state/problemdata
;        ratingdata @tsp.state/ratingdata
;        initial-weights (make-n-weights [0 1] (count evaluators-raw))]
;    (loop [n iterations
;           weights initial-weights
;           bestparams []]
;      (let [evaluators (map #(assoc %1 :weight %2) evaluators-raw (first weights))]
;        (tsp.state/reset-all problemdata)
;        (tsp.state/set-ratingdata ratingdata)
;        (print-progress-bar (float (* 100 (- 1 (/ (count weights) (count initial-weights))))))
;        (if (empty? weights)
;          bestparams
;          (recur (dec n)
;                 (rest weights)
;                 (loop [tries iterations
;                        result bestparams]
;                   (if (< tries 1)
;                     result
;                     (let [tour-params (zipmap (map :name evaluators)
;                                               (map :weight evaluators))
;                           tour (tsp.make-decision/make-decision-with-stacktracking {:producers  producers
;                                                                                     :evaluators evaluators})
;                           tour-rating (rating/rate-tour tour)]
;                       (if (empty? result)
;                         (recur (dec tries) (list {:rating tour-rating
;                                                   :params tour-params
;                                                   :file   file}))
;                         (if (or (nil? tour-rating)
;                                 (< tour-rating (:rating (first result))))
;                           (recur (dec tries) result)
;                           (if (> tour-rating (:rating (first result)))
;                             (recur (dec tries) (list {:rating tour-rating
;                                                       :params tour-params
;                                                       :file   file}))
;                             (recur (dec tries) (conj result {:rating tour-rating
;                                                              :params tour-params
;                                                              :file   file}))))))))))))))
;
;
;


;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Allgemeine Hilfsfunktionen --------------------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;
(defn aggfun-to-filename [agg-fun]
  (apply str (interpose "--" (map name (utilities.misc/as-vector agg-fun)))))

(defn store-config [config agg-fun dm-name ns-name file]
  (pf/update-dm-params dm-name ns-name
                       {:experts    {:evaluators config}
                        :parameters {:evaluator-aggregation-fun agg-fun}})
  (hps.io/save-dm-config-to-file dm-name ns-name file))

;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Funktionen für automatisierte Durchläufe ------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn setup-tsp-task [{problem-name :task}]
  ; load problem
  (tsp.io/open-tsp-file (str (System/getenv "HPS_TSP_DATA_DIR") "/" problem-name ".tsp"))
  ; reset state
  (reset! state/unused-nodes @state/problemdata)
  (reset! state/solution [])
  (reset! state/decisions [])
  (reset! state/future-decisions [])
  (reset! state/uncertainties [])
  (reset! state/uncertainty-history []))

(defn run-dm-for-tsp [dm]
  (while (<= (count @state/solution) (count @state/problemdata))
    (dms/step-decision-module dm)
    (state/apply-decision @(:internal-decision dm))))

(defn get-tsp-result [_]
  (when (not-empty @state/ratingdata)
    {:pao             (tsp.rating/calculate-pao @state/solution)
     :rating          (tsp.rating/rate-tour @state/solution)
     :times-by-humans (or (tsp.rating/human-tour-exists @state/solution) 0)}))

(defn test-tsp-for-conditions
  ([tsp-files evaluator-configs aggfun] (test-tsp-for-conditions tsp-files evaluator-configs aggfun 1))
  ([tsp-files evaluator-configs aggfun repetitions]
   (let [dm-name :tsp
         ns-name 'tsp.pathfinding
         configs-count (* (count tsp-files) (count evaluator-configs))
         for-count (atom 0)]
     (for [problem-name tsp-files
           config evaluator-configs
           ii (range repetitions)]
       (pf/run-test
         dm-name ns-name
         {:task problem-name}                               ; world-setup
         {:experts    {:evaluators config}                  ; dm-config
          :parameters {:evaluator-aggregation-fun aggfun}}
         setup-tsp-task                                     ; prepare-world-fun
         run-dm-for-tsp                                     ; run-dm-fun
         get-tsp-result)))))                                ; get-result-fun


;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Bewertung von Durchläufen ---------------------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn normalize-values-per-task [results-of-one-run quality-measure filter-fun]
  (mapv vector
        (map filter-fun results-of-one-run)
        (mut/z-normalize (map quality-measure results-of-one-run))))
;(mut/normalize-number-list (map quality-measure results-of-one-run))))

(defn order-configs [sort-fun normalized-values-per-run]
  (sort-by second sort-fun (apply map (fn [[k v1] & v2] [k (mut/average (conj (map second v2) v1))]) normalized-values-per-run)))
; Aufpassen mit dem Vorzeichen der Sortierung! Bei PAO ist es <, sonst wahrscheinlich meistens >

(defn order-configs-by-quality [result-or-logfile quality-measure sort-fun filter-fun]
  (if (string? result-or-logfile)
    (order-configs-by-quality (read-string (slurp result-or-logfile)) quality-measure sort-fun filter-fun)
    (->> (group-by :task result-or-logfile)
         (mapv (fn [[task data]] (normalize-values-per-task data quality-measure filter-fun)))
         (order-configs sort-fun))))

;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Globale Parameter für Optimierungsalgorithmen -------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(def tsp-training-problems ["Dahl" "Dantzig" "Darwin" "Dewey" "Dijkstra" "Dirac" "Einstein" "Ellis" "Erikson" "Euklid"])
(def binary-agg-funs [[:weighted-sum :normalized :equal-weights]
                      [:ranking :copeland]
                      [:ranking :sequential-majority-comparison]
                      [:scoring :borda]
                      [:scoring :formula-one-championship]])
(def log-dir (str (System/getenv "HPS_TSP_LOG_DIR") "/tsp/"))

;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Brute Force Optimierung ------------------------------------------------------------------ ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn optimize-tsp-binary []
  (let [dm-name :tsp
        ns-name 'tsp.pathfinding
        dm (dms/get-dm-from-name dm-name ns-name)
        available-evaluators (map :name (:evaluators (:available-experts dm)))]
    (for [agg-fun binary-agg-funs]
      (do
        (let [result (test-tsp-for-conditions tsp-training-problems (remove empty? (combo/subsets available-evaluators)) agg-fun)
              logfile (str log-dir (aggfun-to-filename agg-fun) ".log")]
          (spit logfile (pr-str result))
          (println "log file written for agg fun " agg-fun))))))


(defn convert-optimization-logs-to-config []
  (let [dm-name :tsp
        ns-name 'tsp.pathfinding
        config-dir (.getPath (clojure.java.io/resource "dm-configs/tsp/"))]
    (doseq [agg-fun binary-agg-funs]
      (let [filename (aggfun-to-filename agg-fun)
            logfile (str log-dir filename ".log")]
        (store-config
          ;(first (first (order-configs-by-quality logfile :pao < :config)))
          (first (first (order-configs-by-quality logfile :rating > :config))) ; times-by-humans
          agg-fun
          dm-name ns-name
          (str config-dir filename ".json"))))))


;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Optimierung mit genetischem Algorithmus -------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;

(defn order-individuals-pao [configs]
  (let [scaled-configs (order-configs-by-quality configs :pao < (comp :evaluators :experts))
        min-scale (apply min (map second scaled-configs))]
    (map (fn [[c val]]
           ;(println "val vorher: " val ", norm: " (- val min-scale) ", nachher: " (/ 1 (* 0.2 (+ 0.2 (- val min-scale)))))
           [c (/ 1 (* 0.2 (+ 0.2 (- val min-scale))))])
         scaled-configs)))

(defn order-individuals-others [objective configs]
  (let [scaled-configs (order-configs-by-quality configs objective > (comp :evaluators :experts))
        min-scale (apply min (map second scaled-configs))]
    (map (fn [[c val]]
           ;(println "val vorher: " val ", norm: " (+ 0.5 (- val min-scale)))
           [c (+ 0.1 (- val min-scale))])                   ; Parameter 0.1, damit auch das schlechteste Individuum noch irgendeine Möglichkeit hat weiter zu kommen (insbesondere wichtig, wenn alle Configs gleich sind und damit alle 0 wären)
         scaled-configs)))

(defn optimize-tsp-weights []
  (let [dm-name :tsp
        ns-name 'tsp.pathfinding
        dm (dms/get-dm-from-name dm-name ns-name)
        config-dir (.getPath (clojure.java.io/resource "dm-configs/tsp/"))
        available-evaluators (mapv :name (:evaluators (:available-experts dm)))
        [best-config _]
        (pf/run-genetic-algorithm
          {:max-iterations       64
           :population-size      16                         ; population-size - survival-number muss gerade sein
           :survival-number      2
           :cross-over-rate      0.5
           :mutation-rate        1/4
           :mutation-update-spec [2]}                       ; sagt an welcher Stelle in einem Individuum der neue Wert eingetragen werden soll (mit update-in)
          (fn [] (repeatedly #(mapv (fn [e] [e :weight (mut/round2 1 (rand 1.0))]) available-evaluators)))
          (fn [population] (test-tsp-for-conditions tsp-training-problems population [:weighted-sum :normalized :use-weights]))
          order-individuals-pao)]
    ;(partial order-individuals-others :rating))]
    (store-config best-config [:weighted-sum :normalized :use-weights] dm-name ns-name (str config-dir "weighted-sum--normalized--use-weights-16.json"))))


;;; --------------------------------------------------------------------------------------------- ;;;
;;; -- Testen von gefundenen Parametern --------------------------------------------------------- ;;;
;;; --------------------------------------------------------------------------------------------- ;;;
(def tsp-test-problems ["adleman" "adler" "aho" "Amdahl" "Backus" "Bell" "Binet" "boole" "Born" "Brown" "campbell" "Carmack" "Carson" "Cattell" "Church" "Cohen" "Cook" "Cooley" "Crick" "Curry" "Eysenck" "Fermat" "Fourier" "Fresnel" "Gauss" "Germain" "Gosling" "Graham" "Hamming" "Hellman" "Hooke" "Hopper" "Huffman" "Hughes" "Joule" "Jung" "Kepler" "Knuth" "Koch" "Koenig" "Moore" "Morris" "Neumann" "Newell" "Newton" "Ohm" "Pascal" "Piaget" "Platon" "Ritchie" "Rivest" "Rossum" "Salam" "Sethi" "Shamir" "Shaw" "Simon" "Skinner" "Steele" "Torvalds" "traub" "Turing" "Ullman" "Wall" "Watson" "Wilmut" "Wilson" "Wirth" "Wolfram" "Zuse"
                        ])
(def all-agg-funs (conj binary-agg-funs [:weighted-sum :normalized :use-weights]))


(defn evaluate-agg-funs []
  (let [test-problems tsp-test-problems                     ;(take 1 tsp-test-problems)            ; nur zum Testen: wenige Aufgaben aus der Liste verwenden)
        repetitions 1
        dm-name :tsp
        ns-name 'tsp.pathfinding
        config-dir (.getPath (clojure.java.io/resource "dm-configs/tsp/"))
        log-dir (str (System/getenv "HPS_TSP_LOG_DIR") "/tsp-tests/optimized-for-pao/")] ; "/tsp-tests/")]
    (doseq [agg-fun [[:weighted-sum :normalized :use-weights]]] ;all-agg-funs]
      ;(doseq [iterations [16]]; 32 128]]
      (let [;agg-fun [:weighted-sum :normalized :use-weights]
            filename (aggfun-to-filename agg-fun)           ;(str "weighted-sum--normalized--use-weights-" iterations)
            config (hps.io/load-config-into-dm dm-name ns-name (str config-dir filename ".json"))]
        (let [result (test-tsp-for-conditions test-problems [(:evaluators (:experts config))] agg-fun repetitions)
              logfile (str log-dir filename ".json")]
          (spit logfile (json/write-str result))
          (println "log file written for agg fun " agg-fun))))))

(defn generate-config []
  (let [dm-name :tsp
        ns-name 'tsp.pathfinding
        dm (dms/get-dm-from-name dm-name ns-name)
        available-evaluators (mapv :name (:evaluators (:available-experts dm)))
        ;chosen-evaluators [:convex-hull :nearest-neighbour :remaining-acc-dist :regions-convex-hull] ; best-config
        chosen-evaluators [:convex-hull :last-chance :nearest-neighbour :remaining-acc-dist :regions-convex-hull] ; best-config-plus
        ;chosen-evaluators available-evaluators              ; full-config
        ;chosen-evaluators (reduce (fn [chosen candidate] (if (== 0 (rand-int 2)) chosen (conj chosen candidate))) ; 2/3 Chance ausgewählt zu werden
        ;                          []
        ;                          available-evaluators)
        weights (take (count chosen-evaluators) (repeatedly #(mut/round2 1 (+ 0.1 (rand 0.9)))))]
    {:experts {:producers  (mapv :name (:producers (:available-experts dm)))
               :evaluators (mapv (fn [evl weight] [evl :weight weight]) chosen-evaluators weights)}}))

(defn add-aggfun-to-config [config aggfun]
  (assoc config :parameters {:evaluator-aggregation-fun aggfun}))

(defn evaluate-random-config []
  (let [test-problems tsp-test-problems
        dm-name :tsp
        ns-name 'tsp.pathfinding]
    (dotimes [ii 5]
      (let [rand-config (generate-config)
            log-dir (str (System/getenv "HPS_TSP_LOG_DIR") "/tsp-tests/best-config-plus-" ii "/") ; "/tsp-tests/")]
            agg-fun (last all-agg-funs)]
        (println "config " ii ": " rand-config)
        ;(for [agg-fun all-agg-funs]
        (let [filename (aggfun-to-filename agg-fun)
              config (add-aggfun-to-config rand-config agg-fun)]
          (let [result (test-tsp-for-conditions test-problems [(:evaluators (:experts config))] agg-fun)
                logfile (str log-dir filename ".json")]
            (spit logfile (json/write-str result))
            (println "log file written for agg fun " agg-fun)))))))


;;; Bewertung one-good-reason Heuristik

(defn test-tsp-without-conditions
  ([tsp-files] (test-tsp-without-conditions tsp-files 1))
  ([tsp-files repetitions]
   (let [dm-name :tsp
         ns-name 'tsp.pathfinding]
     (for [problem-name tsp-files]
       (loop [ii 0
              results []]
         (if (< ii repetitions)
           (recur
             (inc ii)
             (conj results
                   (assoc
                     (pf/run-test
                       dm-name ns-name
                       {:task problem-name}                 ; world-setup
                       {}                                   ; dm-config
                       setup-tsp-task                       ; prepare-world-fun
                       run-dm-for-tsp                       ; run-dm-fun
                       get-tsp-result)                      ; get-result-fun
                     :iteration ii)))
           results))))))

(def one-good-reason-conditions {;:minimalist                 [:minimalist :all]
                                 :take-the-best [:take-the-best :all]
                                 ;:elimination-by-aspects     [:minimalist :elimination]
                                 ;:elimination-by-aspects-min [:take-the-best :elimination]
                                 })

(defn evaluate-dm-configs []
  (let [test-problems tsp-test-problems                     ;(take 2 tsp-test-problems)            ; nur zum Testen: wenige Aufgaben aus der Liste verwenden)
        repetitions 20                                      ; 10
        dm-name :tsp
        ns-name 'tsp.pathfinding
        log-dir (str (System/getenv "HPS_TSP_LOG_DIR") "/tsp-tests/one-good-reason/")] ; "/tsp-tests/")]
    (loop [[[cond-name [evaluator-mode alternatives-mode]] & rest-conds] (vec one-good-reason-conditions)]
      (loop [[rel-limit-accept & other-limits] [0.01, 0.25, 0.5, 0.75, 1.0, 2.0]] ;[0.1]]
        (loop [[elimination-cutoff & other-cutoffs] [0]] ; 1 2]]
               (dms/update-dm-declaration dm-name (find-ns ns-name)
                                          (fn [current-dm & args]
                                            (types/map->DecisionModule
                                              (-> current-dm
                                                  (assoc :configure-fun (lib/make-configure-fun-one-good-reason evaluator-mode)
                                                         :reconfigure-fun (lib/make-reconfigure-fun-one-good-reason evaluator-mode alternatives-mode))
                                                  (assoc-in [:decision-parameters :relative-limit-accept] rel-limit-accept)
                                                  (assoc-in [:decision-parameters :elimination-by-aspects-cutoff-stdev] elimination-cutoff)))))
               (let [filename (str (name cond-name) "-" rel-limit-accept "-" elimination-cutoff)]
                 (let [result (test-tsp-without-conditions test-problems repetitions)
                       logfile (str log-dir filename ".json")]
                   (spit logfile (json/write-str result))
                   (println "log file " logfile " written")))
               (when other-cutoffs
                 (recur other-cutoffs)))
        (when other-limits
          (recur other-limits)))
      (when rest-conds
        (recur rest-conds)))))

