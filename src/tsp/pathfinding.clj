; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.pathfinding
  (:require [hps.heuristics :refer :all]
            [hps.types :refer :all]
            [hps.decision-modules :refer :all]
            [hps.library :as lib]
            [tsp.state :as state]
            [tsp.utilities :as util]
            [utilities.math :as mut]
            [utilities.misc :as utmisc :refer [in?]]))

;;Helper defs
;;Must be defn instead of def because the values change over time
(defn startnode-needed? [] (and (empty? @state/solution)
                                (not-empty @state/unused-nodes)))
(defn current-node [] (last @state/solution))




;; =============================================================================
;; =============================== STARTNODES ==================================
;; =============================================================================

(defn smallest-angle-model
  "Returns the node on the convex hull with the smallest angle."
  [nodes]
  (let [ch (util/convex-hull nodes)
        angls (util/angles-between-nodes ch)
        minang (apply min-key :angle angls)]
    (map :node (filter #(> (/ (:angle minang) (:angle %)) 0.80) angls))))


(defn max-distance-model
  "Take a vector of nodes and returns the node with the maximum accumulated
  distance to its neighbours"
  [nodes]
  (let [distances (map #(util/accumulated-distance % nodes) nodes)
        maxdistance (apply max distances)
        validists (filter #(> (/ % maxdistance) 0.95) distances)
        idxs (flatten (map #(util/indexes-of % distances) validists))]
    (map #(get nodes %) idxs)))


(defn proximity-model
  "Takes a vector of nodes and returns the node with the best score
   according to the proximity model  "
  [nodes]
  (->> (util/distance-map nodes)
       (map (comp rest sort))
       (utmisc/transpose)
       (map #(concat (util/index-of-min %) (util/index-of-max %)))
       (flatten)
       (frequencies)
       (apply max-key val)
       (key)
       (get nodes)))


(def-heuristic start-maxdist
               :producer (def-expert-fun []
                                         (when (startnode-needed?)
                                           (max-distance-model @state/unused-nodes))))

(def-heuristic start-smallest-angle
               :producer (def-expert-fun [] (when (startnode-needed?)
                                              (smallest-angle-model @state/unused-nodes))))

(def-heuristic start-proximity
               :producer (def-expert-fun [] (when (startnode-needed?)
                                              (proximity-model @state/unused-nodes))))


;; =============================================================================
;; ============================= TOUR PRODUCERS ================================
;; =============================================================================

;"Neighbourhood Producer returns the k nearest neighbours to the current node.
; TODO Find out 'best' k value"
(def-heuristic neighbourhood
               :producer (def-expert-fun [] (when (not-empty @state/solution)
                                              (util/nearest-k-neighbours (current-node)
                                                                         @state/unused-nodes
                                                                         3)))) ;TODO k als Parameter definieren


;; =============================================================================
;; =============================== CONVEX HULL =================================
;; =============================================================================

(defn intact-hull?
  "Checks, if the points in sol, that are part of the convex hull of nodes,
  are provided in the same order as in the convex hull
  (or in the reverse order).
  Returns true for solutions that include less than two hull points.
  Example: Assuming [0 4 2 3] represents the convex hull of a problem,
  the function would return true for the solutions [0 3 5 1 4 2] and
  [1 4 2 3 5 0], but false for [4 1 2 3 5 0] or [0 5 3 1 4 2]."
  [sol nodes]
  (let [ch (util/convex-hull nodes)
        hull-points (filter (partial in? ch) sol)
        match (util/take-until (last hull-points)
                               (util/drop-until (first hull-points) (concat ch ch)))
        revmatch (util/take-until (last hull-points)
                                  (util/drop-until (first hull-points)
                                                   (concat (reverse ch) (reverse ch))))]
    (or (< (count hull-points) 2)
        (= match hull-points)
        (= revmatch hull-points))))


(def-heuristic convex-hull
               ;"Convex Hull Producer returns the next and the previous point on the
               ;convex hull if they are not already part of the solution.
               ;The result should only contain two points if the current point is the
               ;first ch point in the tour. The result will be empty if no ch point is
               ;yet part of the tour, or if no ch points are left."
               :producer (def-expert-fun [] (when (not-empty @state/solution)
                                              (let [ch (util/convex-hull @state/problemdata)
                                                    current (last (filter (partial in? ch) @state/solution))]
                                                (loop [prev (last ch)
                                                       remaining ch]
                                                  (let [next (if (empty? (rest remaining))
                                                               (first ch)
                                                               (first (rest remaining)))]
                                                    (if (= (first remaining) current)
                                                      (filter #(not (in? @state/solution %)) (list prev next))
                                                      (when (seq remaining)
                                                        (recur (first remaining) (rest remaining)))))))))
               ;"Intact Convex Hull - Checks, if the order of convex hull points would be
               ;violated by choosing an alternative. If so, the tour will eventually have
               ;a crossing which is bad. Those alternatives should be rated very low."
               :evaluator (def-expert-fun [alts] (utmisc/map-to-kv
                                                   (fn [alt]
                                                     (let [sol (conj @state/solution (:value alt))
                                                           nodes @state/problemdata]
                                                       (if (intact-hull? sol nodes) 1 0)))
                                                   alts)))




;"Closes the tour at the end by adding the startpoint to the solution again
;if there are no points left and the tour was not closed already"
(def-heuristic close-tour
               :producer (def-expert-fun [] (when (and (empty? @state/unused-nodes)
                                                       (not= (first @state/solution)
                                                             (current-node)))
                                              (first @state/solution))))

;; =============================================================================
;; =============================== EVALUATORS ==================================
;; =============================================================================

;"Majority-Evaluator: higher ratings for alternatives that occur more than
;once in the alternatives list. More a voting method than an evaluator."
; -> bringt nichts, wenn man Duplikate ausfiltert
;(def-heuristic majority-evaluator
;  (map->Evaluator {:name :majority :priority 1 :weight 1.0
;                   :evaluation-fun
;                         (fn [alt other-alts]
;                           (- 1.0 (/ 1.0 (+ 1.0 (count (filter #(= (:value alt)
;                                                                   (:value %))
;                                                               other-alts))))))}))


;"Rates the nearest neighbour with 1. The other ratings linearly depend on the
;  distance compared to the nearest neighbour distance: (nn-dist / alt-dist)"
(def-heuristic nearest-neighbour
               :evaluator (def-expert-fun [alts]
                                          (if (empty? @state/solution)
                                            (utmisc/map-to-kv (fn [alt] 1.0) alts)
                                            (let [current (current-node)
                                                  inverted-distances (map #(/ 100 (util/euclidean-distance current (:value %))) alts)]
                                              (into {} (map vector alts inverted-distances))))))



(defn successor-straightness
  "Returns the difference lines (prev  - curr - and each point in nodes)
  in a map containing the node and its corresponding difference."
  [prev curr nodes]
  (map (fn [%1] (Math/abs (- (:angle %1) 180)))
       (map #(util/angle-between-nodes prev curr %) nodes)))

(defn straightness
  "Returns a value [0,1] representing the straightness of the line
  prev -- curr -- alt relative to the points in other.
  1 is the best rating (the alternative with the smallest difference to
  the line prev -- curr)."
  [prev curr alt other]
  (let [angles (successor-straightness prev curr other)
        minangle (apply min angles)
        maxangle (apply max angles)]
    (if (< (count other) 2)
      1
      (if (= 0.0 maxangle)
        0
        (- 1 (/ (- (first angles) minangle) maxangle))))))

;"Follow Lines - Alternatives that would cause sharp corners in the tour are
;less preferred than (relatively) straight tours.
;For now, the ranks are given by ratio to the straightest alternative."
(def-heuristic follow-lines                                 ; Wertebereich zwischen 0 und 1 wird nicht notwendigerweise ausgeschöpft (anders als bei nearest-neighbour)!
               :evaluator (def-expert-fun [alts]
                                          (if (or (< (count @state/solution) 2) (<= (count alts) 1))
                                            (utmisc/map-to-kv (fn [alt] 1.0) alts)
                                            (let [prev (second (reverse @state/solution))
                                                  curr (current-node)]
                                              (utmisc/map-to-kv (fn [alt]
                                                                  (straightness prev curr (:value alt) (map :value (remove #(= alt %) alts))))
                                                                alts)))))


;"Checks if the line from the current point to the alternative results
;in intersecting lines. Intersections are rated 0 - valid lines are rated 1. "
(def-heuristic no-intersections
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if (< (count @state/solution) 2)
                                                                1
                                                                (let [p (current-node)
                                                                      q (:value alt)]
                                                                  (utmisc/bool2int
                                                                    (not (util/pairwise-intersections? p q @state/solution))))))
                                                            alts)))

(defn splitting-points?
  "Returns true if the line p q divides points in two sets.
  FIXME: Checks for the complete infinite line, should only check
  the relevant section."
  [p q points]
  (count (clojure.set/difference (set (map #(util/orientation p q %) points))
                                 #{0})))

;"Alternatives resulting in a split set of remaining points are rated with 0 -
;Others with 1."
(def-heuristic avoid-splitting
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if-not (empty? @state/solution)
                                                                (let [split (splitting-points?
                                                                              (current-node)
                                                                              (:value alt)
                                                                              (filter #(not= (:value alt) %)
                                                                                      @state/unused-nodes))]
                                                                  (utmisc/bool2int (< split 1)))
                                                                1.0))
                                                            alts)))

;"Alternatives that would loose their nearest neighbour when not chosen at this
;point are rated with 1 - Others with 0. Attempts to prevent 'forgotten points'."
(def-heuristic last-chance
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if-not (empty? @state/solution)
                                                                (let [node (:value alt)
                                                                      nn (util/nearest-neighbour
                                                                           node
                                                                           (cons (current-node)
                                                                                 (utmisc/without @state/unused-nodes node)))]
                                                                  (utmisc/bool2int (= nn (current-node))))
                                                                1.0))
                                                            alts)))

;"Rates alternatives with 0 of which a direct connection to the starting point
;results in an intersection. FIXME: Doesn't work on problems with few inner
;points"
(def-heuristic start-intersection
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if (< (count @state/solution) 2)
                                                                1
                                                                (let [p (first @state/solution)
                                                                      q (:value alt)]
                                                                  (utmisc/bool2int
                                                                    (not (util/pairwise-intersections? q p @state/solution))))))
                                                            alts)))


;"Rates an alternative by the accumulated distance to the remaining points
;in the problem. The idea is to have an estimation of the remaining costs
;to solve the problem - similar to h(x) in A*.
;By choosing the alternative with the greatest accumulated distance to the
;remaining points the remaining costs are reduced by its greatest contributor."
(def-heuristic remaining-acc-dist
               :evaluator (def-expert-fun [alts]
                                          (if (or (<= (count @state/unused-nodes) 1)
                                                  (<= (count @state/solution) 0))
                                            (utmisc/map-to-kv (fn [alt] 1.0) alts)
                                            (utmisc/map-to-kv #(util/accumulated-distance (:value %) @state/unused-nodes) alts))))

;(let [distance-per-node (map #(util/accumulated-distance (:value %) @state/unused-nodes) alts)
;      max-dist (apply max distance-per-node)]
;
;  (map (fn [alt dist]
;         [alt (/ dist max-dist)])
;       alts
;       distance-per-node)))))


;"Alternatives of the current region receive a higher rating than alternatives
;of neighboring regions."
(def-heuristic regions
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if (and (pos? (count (deref state/solution)))
                                                                       (pos? (count (deref state/unused-nodes))))
                                                                (let [curr (current-node)
                                                                      regns (util/m-regions @state/problemdata)
                                                                      curr-reg (first (filter #(in? % curr) regns))]
                                                                  (utmisc/bool2int (in? curr-reg (:value alt))))
                                                                1.0))
                                                            alts)))

;"Alternatives of the next or previous region are rated with 1 - others with 0
;The order of regions is given by the convex hull formed by the center of mass
;of regions."
(def-heuristic regions-convex-hull
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if (> (count @state/solution) 1)
                                                                (let [curr (current-node)
                                                                      regns (util/m-regions @state/problemdata)
                                                                      curr-reg (first (filter #(in? % curr) regns))
                                                                      alt-reg (first (filter #(in? % (:value alt)) regns))
                                                                      com-nodes (map #(let [com (util/center-of-mass %)]
                                                                                        (util/->TSPNode (.indexOf regns %)
                                                                                                        (first com)
                                                                                                        (second com))) regns)
                                                                      com-hull (util/convex-hull com-nodes)
                                                                      curridx (.indexOf regns curr-reg)
                                                                      altidx (.indexOf regns alt-reg)
                                                                      n (count regns)]
                                                                  (if (or (= curridx -1)
                                                                          (= altidx -1))
                                                                    0.0
                                                                    (utmisc/bool2int (or (= (mod (inc curridx) n) altidx)
                                                                                         (= (mod (dec curridx) n) altidx)))))
                                                                1.0))
                                                            alts)))

;; =============================================================================
;; == DECISION MODULES =========================================================
;; =============================================================================

;(declare-dm :tsp
;            :available-experts {:producers  [:start-maxdist :start-smallest-angle :start-proximity :neighbourhood
;                                             :convex-hull :close-tour]
;                                :evaluators [:convex-hull :no-intersections :avoid-splitting :last-chance
;                                             :nearest-neighbour :follow-lines :remaining-acc-dist
;                                             :start-intersection :regions :regions-convex-hull]}
;            :configure-fun (lib/make-configure-fun-static {:experts {:producers  [:start-maxdist :start-smallest-angle :start-proximity :neighbourhood
;                                                                                  :convex-hull :close-tour]
;                                                                     :evaluators [[:convex-hull :weight 0.6]
;                                                                                  [:nearest-neighbour :weight 0.8]]}})
;            ;[:follow-lines :weight 0.2]
;            ;[:last-chance :weight 0.4]
;            ;:avoid-splitting]}}))
;            :decision-parameters {:merge-similar-alternatives? true
;                                  :evaluator-aggregation-fun   [:scoring :formula-one-championship] ;:weighted-sum
;                                  :run-frequency-fun           (run-frequency-fun-by-time 10)
;                                  :max-iterations-per-step     1})


;;; configurations to mimic one-good-reason heuristics
(declare-dm :tsp
            :available-experts {:producers  [:start-maxdist :start-smallest-angle :start-proximity :neighbourhood
                                             :convex-hull :close-tour]
                                :evaluators [:convex-hull :no-intersections :avoid-splitting :last-chance
                                             :nearest-neighbour :follow-lines :remaining-acc-dist
                                             :start-intersection :regions :regions-convex-hull]}
            :configure-fun (lib/make-configure-fun-one-good-reason :minimalist) ; Optionen: :take-the-best :minimalist
            :reconfigure-fun (lib/make-reconfigure-fun-one-good-reason :minimalist :elimination) ; Optionen: :all :elimination
            :decision-parameters {:merge-similar-alternatives?         true
                                  :evaluator-aggregation-fun           [:weighted-sum :raw :equal-weights]
                                  ;:run-frequency-fun           (run-frequency-fun-by-time 10)
                                  :max-iterations-per-step             10 ; Anzahl Versuche bis bei step einfach die beste Option genommen wird, egal ob sie viel besser ist als die anderen
                                  :accept-best-alternative-fun         lib/accept-relative
                                  :relative-limit-accept               0.2 ; soviel muss die beste Bewertung relativ über der zweitbesten liegen
                                  :accept-single-alternative           true ; wenn nur eine Alternative zur Verfügung steht, wird diese akzeptiert (sonst müssen in weiteren Runde mehr Alt. generiert werden)
                                  :take-the-best-evaluator-order       '(:nearest-neighbour :convex-hull (:remaining-acc-dist :regions-convex-hull) :last-chance
                                                                          :no-intersections :follow-lines :regions :avoid-splitting :start-intersection)
                                  :elimination-by-aspects-cutoff-stdev 1 ; wenn eine Alternative diese Zahl * stdev unter mean liegt, fliegt sie raus; default: 0 (alle unter mean fliegen raus)
                                  })

