; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.make-decision
  (:require [hps.make-decision :as md]
            [hps.decision-modules :as dms]
            [hps.types :refer :all]
            [tsp.state :as state]
            [tsp.utilities :as util]
            [utilities.math :as mutil]
            [clojure.tools.logging :as log]))

(defn save-similar-alternatives
  "Compares the alternatives with the highest :aggregated-evaluation values.
   If an alternatives :aggregated-evaluation value is
   similar (90%) to the highest value, remember the current state as
   an uncertain state."
  [sorted-alts]
  (let [best-alts (shuffle
                   (filter #(= (mutil/round2 2 (:aggregated-evaluation %))
                               (mutil/round2 2 (:aggregated-evaluation (first sorted-alts)))) sorted-alts))]
    ;; (println (map :id @state/solution) ((comp :id :value) (first best-alts)))
    (doseq [alt (rest sorted-alts)]
      (when (not= 0.0 (:aggregated-evaluation (first best-alts)))
        (when
            (>= (/ (:aggregated-evaluation alt)
                   (:aggregated-evaluation (first best-alts)))
                0.9)
          (state/push-uncertain-state alt))))))

(defn check-decision
  [decisionresult]
  (when (nil? (:decision decisionresult)) (log/error "decision passed to check-decision was nil"))
  (state/apply-decision decisionresult)
  (let [verdict (and (not (util/has-intersections? @state/solution))
                     (> (:aggregated-evaluation (:decision decisionresult)) 0.1))]
    (state/revert-decision)
    verdict))

; Neue Version step-decision-module für Backtracking
(defn step-decision-module-with-stacktracking [dm-spec]
  (let [params (:decision-parameters dm-spec)]
    (reset! (:decision-setup dm-spec) (dms/configure-decision-module dm-spec params))
    (loop [iteration 1]
      (let [evaluated-alternatives (md/get-and-evaluate-alternatives @(:decision-setup dm-spec))
            {rejected true retained false} (group-by #(nil? (:aggregated-evaluation %)) evaluated-alternatives)
            [best-alternative & other-retained-alternatives :as sorted-retained-alternatives] (sort-by :aggregated-evaluation > retained)
            accept-best? ((:accept-best-alternative-fun params) best-alternative other-retained-alternatives params)
            decision-result (map->DecisionResult
                             {:decision              (when accept-best? best-alternative)
                              :retained-alternatives (if accept-best? other-retained-alternatives sorted-retained-alternatives)
                              :rejected-alternatives rejected})]
        (reset! (:internal-decision dm-spec) decision-result)
        (cond
          (and accept-best? best-alternative (check-decision decision-result))
          (reset! (:executed-decision dm-spec) best-alternative)

          (>= iteration (:max-iterations-per-step params))
          (reset! (:executed-decision dm-spec)
                  (cond (and (not (check-decision {:decision best-alternative
                                                   :retained-alternatives retained
                                                   :rejected-alternatives rejected}))
                             (pos? (count (deref state/uncertainties))))
                        (state/pop-uncertain-state)

                        (empty? retained)
                        (:default-decision params)

                        :else
                        (do
                          (save-similar-alternatives sorted-retained-alternatives)
                          best-alternative)))

          :else (do (reset! (:decision-setup dm-spec) (dms/reconfigure-decision-module dm-spec params))
                    (recur (inc iteration))))))))
