; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.rating
  (:require
   [tsp.state :as state]
   [clojure.string :as str]
   [tsp.utilities :as uttsp]))


(defn rotate-to-zero
  "Rotates a solution until 0 is the first item"
  [solution]
  (let [nodupes (rest solution)
        zero-ind (.indexOf nodupes 0)]
    (conj (vec (concat (drop zero-ind nodupes) (take zero-ind nodupes))) 0)))


(defn shift-to-zero
  "Shifts solution to be zero indexed"
  [solution]
  (let [min-index (apply min solution)]
    (map #(- % min-index) solution)))


(defn rate-tour
  "Takes a tour and tries to rate it"
  [tour]
  (when-not (or (empty? (deref state/ratingdata)) (empty? tour))
    (let [solution (shift-to-zero (map :id tour))
          connections (map vector solution (rest solution))
          mat (:connections (deref state/ratingdata))]
      (utilities.math/round2 (reduce + (map #(get-in (first %) (second %))
                                            (map vector mat connections)))))))



(defn human-tour-exists
  "Compares a tour to the tourset provided in the ratingdata"
  [tour]
  (if (not-empty @state/ratingdata)
    (let [solution (rotate-to-zero (shift-to-zero (map :id tour)))]
      (loop [remaining (:solutions @state/ratingdata)
             matches 0]
        (if (empty? remaining)
          matches
          (let  [[solkey cnt] (first remaining)
                 sol (vec (map #(Integer/parseInt %)
                               (str/split
                                (str/replace (name solkey) #"[\[\]\'\s]" "") #",")))]
            (if (or (= sol solution)
                    (= sol (reverse solution)))
              (recur (rest remaining) (+  matches cnt))
              (recur (rest remaining) matches))))))))

(defn calculate-tour-length [tour]
  (apply + (map uttsp/euclidean-distance tour (rest tour))))

(defn calculate-pao [tour]
  (let [optimal-tour-length (:optimal-tour-length @tsp.state/ratingdata)]
    (when (and optimal-tour-length (> optimal-tour-length 0))
      (/ (- (calculate-tour-length tour) optimal-tour-length) optimal-tour-length))))


