; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.core
  (:gen-class)
  (:require [hps.GUI.main :as main]
            [hps.GUI.structure :as structure]
            [hps.decision-modules :refer :all]
            tsp.GUI.core
            tsp.io
            tsp.pathfinding ;;Muss hier bleiben wegen der experten für declare-dm
            tsp.rating
            tsp.parameter-fitting
            [tsp.state :as state]))


(defn start-tsp-gui []
  (structure/register-hps-mode "TSP" tsp.GUI.core/adjust-gui-for-tsp true)
  (main/start-hps-gui))


(defn start-without-gui [file & other-args]
  (time
   (let [dm (get-dm-from-name :tsp 'tsp.pathfinding)]
     (println "solve TSP from file: " file)
     (tsp.io/open-tsp-file file)
     (while (<= (count @state/solution) (count @state/problemdata))
       (step-decision-module dm)
       (state/apply-decision @(:internal-decision dm)))
     (println (map :id @state/solution))
     (when (not-empty @state/ratingdata)
       (println (format "PAO: %.2f" (tsp.rating/calculate-pao @state/solution)))
       (println (str "Rating: " (tsp.rating/rate-tour @state/solution)))
       (println (str "Human: "
                     (utilities.math/round2 (:medianRating @state/ratingdata))
                     " - "
                     (utilities.math/round2 (:maxRating @state/ratingdata)))))
     (println (str "Tour was produced "
                   (or (tsp.rating/human-tour-exists @state/solution) 0)
                   " times by humans.")))))




;(defn start-without-gui [& args]
;  (let [[producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                 adaptor-sym
;                                                                 evaluator-sym))]
;    (time (doseq [file args]
;            (println (str "Constructing tour without stacktracking " file " 285 times"))
;            (tsp.io/open-tsp-file file)
;            (loop [n 1000
;                   solutions []]
;              (tsp.parameter-fitting/print-progress-bar (float (* 100 (- 1 (/ n 285)))))
;              (state/reset-all @state/problemdata)
;              (if (< n 1)
;                (spit (clojure.string/replace file #"\.tsp$" ".sol")
;                      (apply str (map #(str (clojure.string/join " " %) "\n") solutions)))
;                (do (while (<= (count @state/solution) (count @state/problemdata))
;                      (state/apply-decision (tsp.make-decision/make-decision
;                                                 {:producers producers
;                                                  :adaptors adaptors
;                                                  :evaluators evaluators})))
;
;                    ;; (println sol)
;                    ;; (println (map :id @state/solution) (tsp.rating/rate-tour @state/solution)
;                    ;;          "|" (select-keys @state/ratingdata [:minRating :medianRating :maxRating]))
;
;                    (recur (dec n) (conj solutions (map :id @state/solution))))))))))
;
;
;
;
;(defn validate-parameters [args]
;  (time
;   (let
;       [parameterfile (first args)
;        problems (rest args)
;        [producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                 adaptor-sym
;                                                                 evaluator-sym))]
;     ;; (println "validate")
;     ;; (println (count (tsp.io/read-parameter-file parameterfile))))))
;     (loop [remaining problems
;            good-params (tsp.io/read-parameter-file parameterfile)]
;       (if (empty? remaining)
;         (do
;           (println (str "Kept " (count good-params) " Parameters"))
;           (tsp.io/write-json good-params  "./validation/validation-final.json"))
;         (let [params-kept (fitting/validate good-params (first remaining)
;                                             producers adaptors evaluators 5)]
;           (tsp.io/write-json good-params (str "./validation/validated-params-" (count remaining) ".json"))
;           (recur (rest remaining)
;                  (vec params-kept))))))))
;(defn start-best-fitting [args]
;  (time
;   (let [triesperconst 1
;         [producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                  adaptor-sym
;                                                                  evaluator-sym))
;         iterations (Math/pow 2 (count evaluators))]
;     (println "Start best parameter fitting with " triesperconst " x " iterations " iterations")
;     (loop [file (first args)
;            remaining (rest args)
;            good-params []]
;       (if (= file nil)
;         (do
;           (println (str "Found " (count good-params) " good parameters in total"))
;           ;; (pprint/pprint (map vals  (map :params good-params)))
;           (tsp.io/write-json good-params "bestparameters.json"))
;         (let [params (fitting/find-best-parameter file producers
;                                                   adaptors evaluators
;                                                   triesperconst)]
;           (println (str "Found " (count params) " good parameters for this problem"))
;           (recur (first remaining)
;                  (rest remaining)
;                  (vec (concat good-params params)))))) )))
;
;
;(defn start-parameter-fitting [args]
;  (time
;   (let [triesperconst 10
;         [producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                  adaptor-sym
;                                                                  evaluator-sym))
;         iterations (Math/pow 2 (count evaluators))]
;     (println "Start parameter fitting with " triesperconst " x " iterations " iterations")
;     (loop [file (first args)
;            remaining (rest args)
;            good-params []]
;       (if (= file nil)
;         (do
;           (println (str "Found " (count good-params) " good parameters in total"))
;           ;; (pprint/pprint (map vals  (map :params good-params)))
;           (tsp.io/write-json good-params "parameter-samples.json"))
;         (let [params (fitting/start-fitting file producers
;                                             adaptors evaluators
;                                             iterations triesperconst)]
;           (println (str "Found " (count params) " good parameters for this problem"))
;           (recur (first remaining)
;                  (rest remaining)
;                  (vec (concat good-params params)))))) )))
;
;
;(defn start-with-backtracking [& args]
;  (let [file (first args)
;        [producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                 adaptor-sym
;                                                                 evaluator-sym))]
;    (println (str "Constructing tour with backtracking for " file))
;    (tsp.io/open-tsp-file file)
;    (println (make-decision-with-backtracking {:producers producers
;                                               :evaluators evaluators}))
;    (pprint/pprint @state/solution)))
;
;(defn start-with-stacktracking [& args]
;  (let [[producers adaptors evaluators] (map #(map eval %) (list producer-sym
;                                                                 adaptor-sym
;                                                                 evaluator-sym))]
;    (time (doseq [file args]
;            (println (str "Constructing tour with stacktracking for " file " 285 times"))
;            (tsp.io/open-tsp-file file)
;            (loop [n 1000
;                   solutions []]
;              (tsp.parameter-fitting/print-progress-bar (float (* 100 (- 1 (/ n 285)))))
;              (state/reset-all @state/problemdata)
;              (if (< n 1)
;                (spit (clojure.string/replace file #"\.tsp$" ".sol")
;                      (apply str (map #(str (clojure.string/join " " %) "\n") solutions)))
;                (let [sol (make-decision-with-stacktracking {:producers producers
;                                                             :evaluators evaluators})]
;                  ;; (println sol)
;                  ;; (println (map :id @state/solution) (tsp.rating/rate-tour @state/solution)
;                  ;;          "|" (select-keys @state/ratingdata [:minRating :medianRating :maxRating]))
;                  (recur (dec n) (conj solutions (map :id sol))))))))))



