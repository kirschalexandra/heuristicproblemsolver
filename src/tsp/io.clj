; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.io
  (:require [clojure.data.json :as json]
            [tsp.state :as state]
            [tsp.utilities :as util]
            [clojure.java.io :as io]
            [utilities.misc :as utmisc]))

(def coord-regex #"([0-9]+)\s+([0-9]+)\s+([0-9]+)")

(defn parse-tsp-file
  "Takes the filename of a tsp file and returns a vector
  of TSPNodes specified in the file."
  [filename]
  (with-open [rdr (clojure.java.io/reader filename)]
    (reduce #(let [match (re-seq coord-regex %2)]
               (if (not-empty match)
                 (conj %1 (util/match->node match))
                 %1))
            []
            (doall (line-seq rdr)))))

(defn load-json-file
  "Loads a json dump file containing all information for the rating process."
  [file]
  (let [filename (clojure.string/replace file #"\.tsp$" ".json")]
    (if (.exists (clojure.java.io/as-file filename))
      (state/set-ratingdata (json/read-str (slurp filename) :key-fn keyword)))))

(defn make-tour-from-node-ids [nodelist]
  (when (not-empty @state/problemdata)
    (conj (vec (sort-by :id (partial utmisc/comp-order-like-seq nodelist) @state/problemdata))
          (first (filter #(= (:id %) (first nodelist)) @state/problemdata)))))

(defn set-ratingdata [file]
  "Loads a json dump file containing all information for the rating process and calculates optimal tour length."
  (let [json-filename (clojure.string/replace file #"\.tsp$" ".json")
        sol-file (.toFile (.resolve (.toPath (io/as-file (System/getenv "HPS_TSP_SOLUTIONS_DIR")))
                                    (str (clojure.string/replace (.getName (io/as-file file)) #"\.tsp$" ".sol"))))
        json-data (when (.exists (io/as-file json-filename))
                    (json/read-str (slurp json-filename) :key-fn keyword))]
    (state/set-ratingdata
      (if (.exists sol-file)
        (let [first-solution (first (line-seq (io/reader sol-file)))
              tour (make-tour-from-node-ids (map inc (read-string (str "[" first-solution "]"))))]
          (assoc json-data :optimal-tour-length (tsp.rating/calculate-tour-length tour)))
        (or json-data [])))))


(defn open-tsp-file
  [file]
  (if file
    (let [data (parse-tsp-file file)]
      (when data
        (state/reset-all data)
        ;(println file)
        (set-ratingdata file)))))



(defn write-json
  [data filename]
  (with-open [wrtr (clojure.java.io/writer filename)]
    (.write wrtr (json/write-str data))))


(defn read-parameter-file
  [filename]
  (json/read-str (slurp filename) :key-fn keyword))
