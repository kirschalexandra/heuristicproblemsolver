; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns tsp.state
  (:require [utilities.misc :refer [in?]]))


;;Holds the original problemdata.
;;Should not be changed but when loading a different problem.
(def problemdata (atom []))

;;Holds the nodes not used in the current solution
(def unused-nodes (atom []))

;;Holds the current solution state as a sorted list of nodes
(def solution (atom []))

;;Holds the applied decision vectors.(
(def decisions (atom []))

;;Holds the decision vector yet to come
(def future-decisions (atom []))

;;Holds states with alternative decision branches
(def uncertainties (atom []))

;;Keeps track of pushed uncertain states to prevent endless loops
(def uncertainty-history (atom []))

;;Holds the data to rate the tour
(def ratingdata (atom []))

(defn apply-decision
  "moves the node specified in the decision from the
  unused vector to the solution vector"
  [decision] ; executed-decision, also vom Typ DecisionAlternative
  (let [node (:value (:decision decision))]
    (when node
      (swap! decisions conj decision)
      (swap! solution conj node)
      (swap! unused-nodes #(vec (remove #{%2} %1)) node))))


(defn revert-decision
  "Moves the last point in solution back to unused-points"
  []
  (when (pos? (count @solution))
    (if (< (count @unused-nodes) (count @problemdata))
      (swap! unused-nodes conj (last @solution)))
    (swap! solution pop)
    (swap! future-decisions conj (last @decisions))
    (swap! decisions pop)))


(defn redo-decision
  "Re-applies a decision stored in the future-decisions"
  []
  (when (pos? (count @future-decisions))
    (apply-decision (last @future-decisions))
    (swap! future-decisions pop)))

(defn reset-all
  "Resets all atoms to the null-state"
  [data]
  (reset! problemdata data)
  (reset! unused-nodes data)
  (reset! solution [])
  (reset! decisions [])
  (reset! future-decisions [])
  (reset! uncertainties [])
  (reset! ratingdata [])
  (reset! uncertainty-history []))

(defn throw-away-future
  "Resets the future-decisions list"
  []
  (reset! future-decisions []))


(defn push-uncertain-state
  "Pushes the current stat to the backtracking stack."
  [alternative]
  ;;We need to check first, if the alternative was already on the stack
  ;;with the same solution state. If so, we might get stuck in an endless
  ;;loop of pushing and popping the same states :(
  ;; (println  (:value alternative))
  (when (<  (count @uncertainties) 5)
    (when-not (in? (deref uncertainty-history) {:alternative (:value alternative), :solution (deref solution)})
      (swap! uncertainty-history conj {:alternative (:value alternative)
                                       :solution (deref solution)})
      (swap! uncertainties conj {:decisions (deref decisions)
                                 :alternative alternative
                                 :future (deref future-decisions)
                                 :unused (deref unused-nodes)
                                 :solution (deref solution)}))))


(defn pop-uncertain-state
  "Reverts the state to the last uncertain state"
  []
  (let [state (last @uncertainties)]
    (swap! uncertainties pop)
    (reset! unused-nodes (:unused state))
    (reset! solution (:solution state))
    (reset! future-decisions (:future state))
    (:alternative state)))


(defn set-ratingdata
  [data]
  (reset! ratingdata data))
