(defproject heuristic-problem-solver "1.0.0-SNAPSHOT"
  :description "The Heuristic Problem Solver with applications"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [org.clojure/math.combinatorics "0.1.4"]
                 [org.clojure/tools.logging "0.4.0"]
                 [org.slf4j/slf4j-log4j12 "1.7.25"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/tools.cli "0.3.5"]]
  :aot [hps.GUI.main hps.core tsp.core robot.GUI.control.core robot.core]
  :main core
  :jvm-opts ["-XX:ThreadStackSize=2048"]) ; nur nötig für Parameteroptimierung
