;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns hps-test.gui
  (:use [hps-test.examples.food]
        [hps.types]
        [hps.make-decision])
  (:require [hps.GUI.structure :as structure]
            [hps.GUI.main :as main]
            [utilities.gui :as gut]
            [hps.decision-modules :as dms]
            [hps.GUI.make-decision :as hpsgui])
  (:import (javafx.scene Scene Node)
           (javafx.scene.layout HBox VBox Priority Pane BorderPane)
           (javafx.scene.control Label Button SplitPane)
           (javafx.scene.control MenuBar Menu MenuItem)
           (javafx.application Platform)
           (javafx.scene.paint Color)
           (javafx.geometry Pos Insets)))


; initialize-expert-display aus morph_gui/hps/decision_process/displays.clj geholt -> in HPS umziehen?
(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dms/get-dm-from-name dm-keyword 'hps-test.examples.food))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
    (dorun (map #(apply hpsgui/initialize-experts-in-display %) [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]]))))

(defn read-configuration-fun [dm-pane]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems dm-pane))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))
        read-out-experts-fun (fn [pane]
                               (remove #(= % :All) (mapcat #(when (.isSelected %) (list (keyword (.getText %)))) (.getChildren (.getContent pane)))))]
    (fn [_]
      (map->DecisionSetup {:experts {:producers  (read-out-experts-fun producer-pane)
                                     :evaluators (read-out-experts-fun evaluator-pane)}
                           :parameters (hpsgui/read-parameters-from-grid (.getContent parameters-pane))}))))

(defn adjust-gui-for-test [scene]
  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        dm-pane (structure/make-decision-module-basic-display)
        step-button (Button. "Solve")
        dm-object (assoc (dms/get-dm-from-name :food 'hps-test.examples.food)
                    :configure-fun (read-configuration-fun dm-pane))]
    (.setMaxWidth stateRegion 0)
    (gut/add-to-container decisionModules dm-pane)
    (initialize-expert-display dm-pane :food)
    (gut/add-to-container controlRegion step-button)
    (gut/set-button-action
      step-button
      (dms/reset-decision-module dm-object)

      (dms/step-decision-module dm-object)
      (hps.GUI.make-decision/update-decision-display @(:internal-decision dm-object) dm-pane))))

; always add "Test" mode
(structure/register-hps-mode "Test" adjust-gui-for-test false)

; Start mit (start-hps-gui) und Auswahl Modus "Test"