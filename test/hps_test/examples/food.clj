;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns hps-test.examples.food
  (:use [hps.heuristics]
        [hps.decision-modules])
  (:require [utilities.misc :as utmisc]))

(def-heuristic pizza
               :producer (def-expert-fun [] 'pizza))

(def-heuristic asia
               :producer (def-expert-fun [] '(noodles rice)))

(def-heuristic none
               :producer (def-expert-fun [] '()))

(def-heuristic yummy
               :evaluator (def-expert-fun [alts] (utmisc/map-to-kv (fn [alt] (* (count (name (:value alt))) 0.1)) alts)))

(def-heuristic close
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt]
                                                              (if (seq? (:produced-by alt))
                                                                (cond ;(:name (:produced-by alt))
                                                                  (some #(= % :pizza) (:produced-by alt)) 1.0
                                                                  (some #(= % :asia) (:produced-by alt)) 0.9
                                                                  :else 0.1)
                                                                (case (:produced-by alt)
                                                                  :pizza 1.0
                                                                  :asia 0.9
                                                                  0.1)))
                                                            alts)))

(def-heuristic no-pizza
               :evaluator (def-expert-fun [alts] (utmisc/map-to-kv (fn [alt] (if (= (:value alt) 'pizza) nil 1.0)) alts)))


(declare-dm :food
            :available-experts {:producers  [:pizza :asia :none]
                                :evaluators [:yummy :close :no-pizza]}
            :decision-parameters {:merge-similar-alternatives? false})
;:configure-fun (fn [_] (map->DecisionSetup {:experts {:producers  [:pizza :asia]
;                                                      :evaluators [:close :no-pizza]}}))
;:reconfigure-fun (fn [_] (map->DecisionSetup {:experts {:producers  [:pizza :asia]
;                                                        :evaluators [:yummy :close :no-pizza]}})))


;; adaptors are not working yet, will probably be removed
;(def adaptor-healthy (map->Adaptor
;  {:name :adaptor-healthy
;   :adapt-fun (fn [alternative]
;     (let [{old-command :value old-produced-by :produced-by} alternative]
;       (if (= old-command 'pizza)
;         `(:replace ~(list (map->DecisionAlternative
;                             {:value 'small-pizza
;                              :produced-by ((if (seq? old-produced-by) cons list) adaptor-healthy old-produced-by)})))
;         '(:keep 0.5))))}))
;
;(def adaptor-more (map->Adaptor
;  {:name :adaptor-more
;   :adapt-fun (fn [alternative]
;     (let [{old-command :value old-produced-by :produced-by} alternative]
;       (if (= old-command 'rice)
;         `(:replace ~(list (map->DecisionAlternative
;                            {:value 'rice-with-vegetables
;                             :produced-by ((if (seq? old-produced-by) cons list) adaptor-healthy old-produced-by)})
;                           (map->DecisionAlternative
;                            {:value 'spring-roll
;                             :produced-by ((if (seq? old-produced-by) cons list) adaptor-healthy old-produced-by)})))
;         '(:keep 0.5))))}))
