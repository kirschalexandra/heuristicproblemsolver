;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns hps-test.decision-modules
  (:use clojure.test)
  (:require hps.core))

; Beispiele sind nicht als automatisierte Testfälle gedacht, sondern zum Reinkopieren in die REPL

(def dm-spec (hps.decision-modules/run-basic-decision-module))
(def navgoal {:x -3.0 :y 0.1 :yaw 3.14})
(def goal (struct-map hps.types/goal :type nav-goaltype :x (:x navgoal) :y (:y navgoal) :yaw (:yaw navgoal)))
(hps.goals/set-goal goal dm-spec)


(def experts-for-unachieved-goals
	(reduce (fn [experts goal] (remove #(= (:goal %) goal) experts))
	                (:experts old-decision-setup)
	                (clojure.set/difference old-goals new-goals)))
(def experts-for-active-goals
	(reduce (fn [experts goal] (hps.decision-modules/merge-experts experts (hps.decision-modules/get-experts-for-goal goal)))
	                experts-for-unachieved-goals
	                (clojure.set/difference new-goals old-goals))
